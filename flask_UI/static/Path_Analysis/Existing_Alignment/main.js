
window.scrollTo(0, 0);

function initViz(stringDiv,url) {
var containerDiv = document.getElementById(stringDiv),
// url = "https://public.tableau.com/shared/X4Q92PCMD?:display_count=yes&:origin=viz_share_link";

options = {
            hideTabs: true,
            onFirstInteractive: function () {
                console.log("Run this code when the viz has finished loading.");
            }
        };

var viz = new tableau.Viz(containerDiv, url, options);
} 


// // the navigation 
// function openCity(evt, cityName) {
//     // Declare all variables
//     var i, tabcontent, tablinks;

//     // Get all elements with class="tabcontent" and hide them
//     tabcontent = document.getElementsByClassName("tabcontent");
//     for (i = 0; i < tabcontent.length; i++) {
//     tabcontent[i].style.display = "none";
//     }

//     // Get all elements with class="tablinks" and remove the class "active"
//     tablinks = document.getElementsByClassName("tablinks");
//     for (i = 0; i < tablinks.length; i++) {
//     tablinks[i].className = tablinks[i].className.replace(" active", "");
//     }

//     // Show the current tab, and add an "active" class to the button that opened the tab
//     document.getElementById(cityName).style.display = "block";
//     evt.currentTarget.className += " active";
// }


function openPage(pageName, elmnt) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Remove the background color of all tablinks/buttons
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].style.backgroundColor = "";
    }
  
    // Show the specific tab content
    let selected = document.getElementById(pageName);
    selected.style.display = "block";
    window.scrollTo(0, 0);
    // // Add the specific color to the button used to open the tab content
    // elmnt.style.backgroundColor = color;
    
  }
  
  // Get the element with id="defaultOpen" and click on it
    let defaultOpen = document.getElementById("defaultOpen");
    defaultOpen ? defaultOpen.click() : undefined;


    // window.onload ={
    //     document.getElementById('')
    // }