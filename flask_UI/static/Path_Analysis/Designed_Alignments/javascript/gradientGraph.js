function gradientGraph(divName, data, selectedThresholds, colorGraph, name_type = "name", colour_type = "colour",type = "line") {

    let div = document.querySelector(divName);
    let xAxisName = div.dataset.x_label;
    let yAxisName = div.dataset.y_label;

    //For responsive size
    let rect = d3.select(divName).node().getBoundingClientRect(),
    width = rect.width - 200,
    height = rect.height - 80;


    // Format Data
    let parseDate = d3.timeParse("%d-%b-%y");
    data.forEach(function (d) {
        d.values.forEach(function (d) {
            if ((typeof d.xAxis) == "string") {
                d.xAxis = parseDate(d.xAxis);
            }
            d.yAxis = +d.yAxis;
        });
    });



    // Create arrays for the axes
    let xAxisExtend = [];
    let yAxisExtend = [];
    for (let i = 0; i < data.length; i++) {
        let values = data[i].values;
        for (let j = 0; j < values.length; j++) {
            xAxisExtend.push(values[j].xAxis);
            yAxisExtend.push(values[j].yAxis);
        }
    }


    // Create window with x,y points info
    let tooltip = d3.select(divName)
        .append("div")
        .attr("class", "tooltip")
        .classed("hidden", true);


    // Scale
    let xScale = d3.scaleLinear()
        .domain([0, d3.max(xAxisExtend)])
        .range([0, (width)]);

    
    let yScale;
    if(d3.max(yAxisExtend) !== undefined){///////!Warning
        yScale = d3.scaleLinear()
        .domain([0, d3.max(yAxisExtend)])
        .range([(height), 0]);
    }



    // Add SVG
    let generalSvg = d3.select(divName).append("svg")
        .attr("width",`${width}`)
        .attr("height",`${height}`)
        .append('g')
        .attr("transform", "translate(0,0)");

    let svg = generalSvg.append("g");


    //Axes | Grid | Thresholds
    let valuesNum = calcTickNum({x:width, y:height});
    let axes = createAxes(generalSvg, {x:xScale, y:yScale}, valuesNum, xAxisName, yAxisName, {x:width, y:height});
    
    createGrid(svg, {x:xScale, y:yScale}, valuesNum, {x:width, y:height});
    
    let thresholdData = toThresholds(selectedThresholds, width, yScale);
    createThresholds(thresholdData, svg);;


    // Legend
    let legend_svgwrapper = d3.select(`${divName}-legend`).append("svg")
        .attr("width", `${width}`)
        .attr("height", 200);

    let legend = legend_svgwrapper
        .append('g')
        .attr("class", "legend")
        .attr("transform", "translate(0,0)");

    let distinctColours = [...new Set(data.map(item => item.colour))];
    let nextX = 0;

    legend.selectAll('g').data(distinctColours)
        .enter()
        .append('g')
        .each(function (d, i) {
            var g = d3.select(this);

            g.append("rect")
                .attr("x", nextX)
                .attr("y", 0)
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", name(distinctColours[i], colour_type, colorGraph));

            g.append("text")
                .attr("x", nextX + 15)
                .attr("y", 9)
                .attr("font-size", "8pt")
                .text(name(distinctColours[i], name_type));

            let this_width = g.node().getBoundingClientRect().width;
            nextX += this_width + this_width/13 + 15;
        });

    
    let legendWidth = legend_svgwrapper.select('.legend').node().getBoundingClientRect().width;
    legend_svgwrapper
    .attr("width", legendWidth + 20);



    // Create Lines or areas  ****
    let toGraph;
    if(type === "line"){
        toGraph = d3.line()
        .x(d => xScale(d.xAxis))
        .y(d => yScale(d.yAxis));
    }else if ( type === "area"){
        toGraph = d3.area()
        .x(function (d) { return xScale(d.xAxis); })
        .y0(height)
        .y1(function (d) { return yScale(d.yAxis); });
    }


    let lines = svg.append('g')
        .attr('class', 'lines');

    let lineGroup = lines.selectAll('.line-group')
        .data(data).enter()
        .append('g')
        .attr('class', 'line-group')
        .on("mouseover", function (d, i) {
            svg.append("text")
                .attr("class", "selected-label")
                .text(name(d.name, name_type));
        })
        .on("mouseout", function (d) {
            svg.select(".selected-label").remove();
        });

    
    let paths = lineGroup.append('path')
        .attr('class', `line-graph-${type}`)
        .attr('id', d => `line-${d.name}`)
        .attr('d', d => toGraph(d.values))
        .style('stroke', (d, i) => name(d.colour, colour_type, colorGraph));
    
    if(type === "area"){
        paths
        .style("fill", (d, i) => name(d.colour, colour_type, colorGraph));
    }

        
        
        

    // ------------------------------------------------------------------------------------Labels

    let labels = svg.append("g").
        classed("labels", true);

    let vertical = svg.selectAll(".line-graph-line:not(#line-Gradient_Profile)");
    vertical
        .each((d,i)=>{
            
            let x0 = xScale(data[i].values[0].xAxis);
            let x1 = width;
            if(i+1 < data.length-1) x1 = xScale(data[i+1].values[0].xAxis);

            let xPos = ((x1 - x0)/2) + x0;

            if(x1 - x0 > 5){
                let newLabel = labels.append("text")
                .text(name(data[i].name, name_type))
                .attr("x", 0)
                .attr("y", 0)
                .attr("font-size", "8pt")
                .style("transform",`rotate(-90deg) translate(9px,${xPos+2}px)`);

                if(data[i].name === "L"){
                    newLabel.style("transform",`translate(${xPos-5}px, 9px)`);
                }
            }
        });
    







    /* Add circles in the line */
    let circles = lines.selectAll("circle-group")
        .data(data).enter()
        .append("g")
        .attr('id', d => `circle-${d.name}`)
        .style("fill", (d, i) => name(d.colour, colour_type, colorGraph))
        .selectAll("circle")
        .data(d => d.values).enter()
        .append("g")
        .attr("class", "circle")
        .on("mouseover", function (d) {
            d3.select(this)
                .style("cursor", "pointer");
            tooltip
                .classed("hidden",false)
                .html(`<b>${xAxisName}: </b>${parseFloat(d.xAxis).toFixed(3)}<br/> <b>${yAxisName}: </b>${parseFloat(d.yAxis).toFixed(3)}`);
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style("cursor", "none");
            tooltip
            .classed("hidden",true);
        })
        .append("circle")
        .attr("cx", d => xScale(d.xAxis))
        .attr("cy", d => yScale(d.yAxis))
        .attr("r", circleRadius)
        .style('opacity', circleOpacity)
        .on("mouseover", function (d) {
            d3.select(this)
                .transition()
                .duration(duration)
                .attr("r", circleRadiusHover) 
                .style("cursor", "pointer");
            let new_name = d3.select(this)._groups[0][0].parentElement.parentElement.id.split("circle-")[1];
            new_name = name(new_name, name_type);
            svg.append("text")
                .attr("class", "selected-label")
                .text(new_name);
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .transition()
                .duration(duration)
                .attr("r", circleRadius)
                .style("cursor", "none");
            svg.select(".selected-label").remove();
        });

}