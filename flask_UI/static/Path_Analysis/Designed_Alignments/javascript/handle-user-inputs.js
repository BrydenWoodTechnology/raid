//Existing Slider





//Select for single-path graphs
function toGraphs(selectedPath) {
    //Check Existing Alignment
    let existingLinks = false;
    if (document.getElementById("e-a").checked) {
        existingLinks = true;
    }

    let documentPath = `${designedAlignments_path}/${selectedPath.dataset.id}_${selectedPath.dataset.type}_v${selectedPath.dataset.version}.json`;

    // Speed, Gradient, Land Use Proximity, Earth Work
    retrieveCustomjsonData(documentPath);
    // Curvature
    handleAllData(documentPath, existingLinks, { x: 3 * 0.000621371192, y: 1 }, st,"curvature");
}



//-----------------------------------------------------------Events
d3.select(window).on('resize', resize); 

function resize() {
    toMultipleLineGraph(undefined, "cost", "#cost", "cost");
    toMultipleLineGraph(undefined, "speed", `#speed`, "speed", "line");
    updateGraph("#cut_n_fill", singlelineGraphs.cut_n_fill, undefined, thisColours, "cost-name", "cost-colour", "line", mixGraph);   
    updateGraph(`#curvature`, singlelineGraphs.curvature, st, thisColours, "name", "colour");
    updateGraph("#gradient", singlelineGraphs.gradient, undefined, thisColours, "cost-name", "cost-colour", "line", gradientGraph);
}


//Toggle Graph button
let alignments = document.getElementsByClassName("menu")[0];
alignments.addEventListener("click",e=>{
    if(e.target.classList.contains("menu-label")){
        e.target.classList.toggle("selected");
    }
    if(e.target.localName === "input" && (e.target.type === "radio" || e.target.type === "checkbox")){
        toGraphs(document.querySelector("#designed-paths input:checked"));
    }
});