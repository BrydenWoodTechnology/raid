let circleRadius = 1.5;
let circleRadiusHover = 2;


function linearGraph(divName, data, selectedThresholds, colorGraph, name_type = "name", colour_type = "colour",type = "line") {

    let div = document.querySelector(divName);
    let xAxisName = div.dataset.x_label;
    let yAxisName = div.dataset.y_label;



    let duration = 250;
    let lineOpacity = "0.8";
    let otherLinesOpacityHover = "0.1";
    let circleOpacity = '0.85';
    


    //For responsive size
    let rect = d3.select(divName).node().getBoundingClientRect(),
    width = rect.width - 200,
    height = rect.height - 80;


    /* Format Data */
    let parseDate = d3.timeParse("%d-%b-%y");
    data.forEach(function (d) {
        d.values.forEach(function (d) {
            if ((typeof d.xAxis) == "string") {
                d.xAxis = parseDate(d.xAxis);
            }
            d.yAxis = +d.yAxis;
        });
    });



    /* Create arrays for the axis */
    let xAxisExtend = [];
    let yAxisExtend = [];
    for (let i = 0; i < data.length; i++) {
        let values = data[i].values;
        for (let j = 0; j < values.length; j++) {
            xAxisExtend.push(values[j].xAxis);
            yAxisExtend.push(values[j].yAxis);
        }
    }


    /* Create window with x,y points info */
    let tooltip = d3.select(divName)
        .append("div")
        .attr("class", "tooltip")
        .classed("hidden", true);


    /* Scale */
    let xScale = d3.scaleLinear()
        .domain([0, d3.max(xAxisExtend)])
        .range([0, (width)]);

    
    let yScale;
    if(d3.max(yAxisExtend) !== undefined){///////!Warning
        yScale = d3.scaleLinear()
        .domain([0, d3.max(yAxisExtend)])
        .range([(height), 0]);
    }


    // let color = colorGraph.domain(data);


    /* Add SVG */
    let generalSvg = d3.select(divName).append("svg")
        .attr("width",`${width}`)
        .attr("height",`${height}`)
        // .attr("viewBox",`0 0 ${width} ${height}`)
        // .attr("preserveAspectRatio", "none")
        .append('g')
        .attr("transform", "translate(0,0)");

    let svg = generalSvg.append("g")
        // .attr("clip-path", "url(#clip)");//-----------------------------------------------------------------------------------------


    //Axes | Grid | Thresholds | Brush
    let valuesNum = calcTickNum({x:width, y:height});
    let axes = createAxes(generalSvg, {x:xScale, y:yScale}, valuesNum, xAxisName, yAxisName, {x:width, y:height});
    
    createGrid(svg, {x:xScale, y:yScale}, valuesNum, {x:width, y:height});
    
    let thresholdData = toThresholds(selectedThresholds, width, yScale);
    createThresholds(thresholdData, svg);

    // addBrush(svg, {x:width, y:height}, {x:xScale, y:yScale}, axes.x);

    /* append the legend */
    let legend_svgwrapper = d3.select(`${divName}-legend`).append("svg")
        .attr("width", `${width}`)
        .attr("height", 200);

    let legend = legend_svgwrapper
        .append('g')
        .attr("class", "legend")
        .attr("transform", "translate(0,0)");

    let nextX = 0;

    legend.selectAll('g').data(data)
        .enter()
        .append('g')
        .each(function (d, i) {
            var g = d3.select(this);

            g.append("rect")
                .attr("x", nextX)
                .attr("y", 0)
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", name(d.name, colour_type, colorGraph))
                .style("cursor", "pointer")
                .attr("id", name(d.name, name_type))
                .on("mouseover", function (d, i) {

                    svg.selectAll(`.line-graph-area`)
                        .classed("not-selected", true);
                    svg.selectAll(`.line-graph-line`)
                        .classed("not-selected", true);
        
                    let selectedId = this.id;
                    svg.select(`.line-graph-line#line-${selectedId}`)
                        .classed("selected-line",true)
                        .classed("not-selected", false);
        
                })
                .on("mouseout", function (d) {
        
                    svg.selectAll(`.line-graph-area`)
                        .classed("not-selected", false);
                    svg.selectAll(`.line-graph-line`)
                        .classed("not-selected", false);
        
                    svg.select("selected-line")
                        .classed("selected-line",false);
                    
                });


            g.append("text")
                .attr("x", nextX + 15)
                .attr("y", 9)
                .attr("font-size", "8pt")
                .text(name(d.name, name_type));

            let this_width = g.node().getBoundingClientRect().width;
            nextX += this_width + this_width/13 + 15;
        });

    
    let legendWidth = legend_svgwrapper.select('.legend').node().getBoundingClientRect().width;
    legend_svgwrapper
    .attr("width", legendWidth + 20);


    /* Add line into SVG */
    let toGraph;
    let toLine;
    if(type === "line"){
        toGraph = d3.line()
        .x(d => xScale(d.xAxis))
        .y(d => yScale(d.yAxis));
    }else if ( type === "area"){
        toGraph = d3.area()
        .x(function (d) { return xScale(d.xAxis); })
        .y0(height)
        .y1(function (d) { return yScale(d.yAxis); });

        toLine = d3.line()
        .x(d => xScale(d.xAxis))
        .y(d => yScale(d.yAxis));
    }


    let lines = svg.append('g')
        .attr('class', 'lines');

    let lineGroup = lines.selectAll('.line-group')
        .data(data).enter()
        .append('g')
        .attr('class', 'line-group')
        .on("mouseover", function (d, i) {

            svg.append("text")
                .attr("class", "selected-label")
                .text(name(d.name, name_type));
 
            svg.selectAll(`.line-graph-area`)
                .classed("not-selected", true);
            svg.selectAll(`.line-graph-line`)
                .classed("not-selected", true);

            let selectedId = lines.selectAll('.line-group')._groups[0][i].childNodes[0].id;
            svg.select(`.line-graph-line#${selectedId}`)
                .classed("selected-line",true)
                .classed("not-selected", false);

        })
        .on("mouseout", function (d) {
            svg.select(".selected-label").remove();

            svg.selectAll(`.line-graph-area`)
                .classed("not-selected", false);
            svg.selectAll(`.line-graph-line`)
                .classed("not-selected", false);

            svg.select("selected-line")
                .classed("selected-line",false);
            
        });

    
    let paths = lineGroup.append('path')
        .attr('class', `line-graph-${type}`)
        .attr('id', d => `line-${name(d.name, name_type)}`)
        .attr('d', d => toGraph(d.values))
        .style('stroke', (d, i) => name(d.name, colour_type, colorGraph));
    
    if(type === "area"){
        paths
        .style("fill", (d, i) => name(d.name, colour_type, colorGraph));
    }

    if(toLine){
        let path_lines = lineGroup.append('path')
        .attr('class', `line-graph-line`)
        .attr('id', d => `line-${d.name}`)
        .attr('d', d => toLine(d.values))
        .style('stroke', (d, i) => name(d.name, colour_type, colorGraph));
    }


        // paths.on("mouseover", function (d) {
        //     console.log(this.id, "this");
        //     // svg.selectAll(`.line-graph-${d.type}`)
        //     //     .style('opacity', 0.2);
        //     // svg.selectAll('.circle')
        //     //     .style('opacity', 0.2);
        //     // svg.selectAll(`#${this.id}`).classed("selected-line",true);
        // })
        // .on("mouseout", function (d) {
        //     d3.selectAll(`.line-graph-${d.type}`)
        //         .style('opacity', lineOpacity);
        //     // d3.selectAll('.circle')
        //     //     .style('opacity', circleOpacity);
        //     d3.selectAll(`#${this.id}`).classed("selected-line",false);
        // });   


    /* Add circles in the line */
    let circles = lines.selectAll("circle-group")
        .data(data).enter()
        .append("g")
        .attr('id', d => `circle-${d.name}`)
        .style("fill", (d, i) => name(d.name, colour_type, colorGraph))
        .selectAll("circle")
        .data(d => d.values).enter()
        .append("g")
        .attr("class", "circle")
        .on("mouseover", function (d) {
            d3.select(this)
                .style("cursor", "pointer");
            tooltip
                .classed("hidden",false)
                .html(`<b>${xAxisName}: </b>${parseFloat(d.xAxis).toFixed(3)}<br/> <b>${yAxisName}: </b>${parseFloat(d.yAxis).toFixed(3)}`);
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style("cursor", "none");
            tooltip
            .classed("hidden",true);
        })
        .append("circle")
        .attr("cx", d => xScale(d.xAxis))
        .attr("cy", d => yScale(d.yAxis))
        .attr("r", circleRadius)
        .style('opacity', circleOpacity)
        .on("mouseover", function (d) {
            d3.select(this)
                .transition()
                .duration(duration)
                .attr("r", circleRadiusHover) 
                .style("cursor", "pointer");
            let new_name = d3.select(this)._groups[0][0].parentElement.parentElement.id.split("circle-")[1];
            new_name = name(new_name, name_type);
            svg.append("text")
                .attr("class", "selected-label")
                .text(new_name);
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .transition()
                .duration(duration)
                .attr("r", circleRadius)
                .style("cursor", "none");
            svg.select(".selected-label").remove();
        });

}


//------------------------------------For Zoom

let idleTimeout
function idled() { idleTimeout = null; }

  // A function that update the chart for given boundaries
function updateChart(svg, brush, xAxis, scale) {
    
    extent = d3.event.selection

    // If no selection, back to initial coordinate. Otherwise, update X axis domain
    if (!extent) {
        if (!idleTimeout) return idleTimeout = setTimeout(idled, 350); // This allows to wait a little bit
        scale.x.domain([4, 8])
    } else {
        scale.x.domain([scale.x.invert(extent[0]), scale.x.invert(extent[1])])
        svg.select(".brush").call(brush.move, null) // This remove the grey brush area as soon as the selection has been done
    }

    // Update axis and circle position
    xAxis.transition().duration(1000).call(d3.axisBottom(scale.x));

    svg.selectAll("circle")
        .transition().duration(1000)
        .attr("cx", d => {return scale.x(d.xAxis)})   ///solve the error!!!!!!!!!!!!!!!!!!!!!!!!!
        .attr("cy", d => {return scale.y(d.yAxis)});
}



//Brush Event
function isbrushed(x, y, extent, id){ //"id" is circle.parentElement.parentElement.id
    //Check x,y coordinate of circle | if coordinates are inside extent(brushe-window) coordinates === true
    let apply = extent[0][0] <= x && extent[1][0] >= x && extent[0][1] <= y && extent[1][1] >= y;
    d3.selectAll(`#${id}`).selectAll(`[cx="${x}"]`)
            .classed("selected", apply)
            .attr("r",(apply ? circleRadiusHover : circleRadius));
}

function updateBrushed(event,svg, scale){
    // Get the selection coordinate | looks like: [ [12,11], [132,178] ]
    let extent = (event.selection ? event.selection :[[0,0],[0,0]]);
    let circles = svg.selectAll("circle");
    circles.each((d, i)=>{isbrushed(scale.x(d.xAxis), scale.y(d.yAxis), extent, circles._groups[0][i].parentElement.parentElement.id)});
}


function addBrush(svg, dimentions, scale, xAxis){
    let brush = d3.brush() 
    .extent( [ [0,0], [dimentions.x,dimentions.y] ] )
    .on("end", () => {
        updateBrushed(d3.event,svg, scale); 
        // updateChart(svg, brush, xAxis, scale);
    })

    svg.append("g")
        .attr("class", "brush")
        .call( brush );
}




function createThresholds(thresholdData, svg) {
    let area = d3.area()
        .x(function (d) { return d.start.xAxis })
        .y0(function (d) { return d.start.yAxis })
        .y1(function (d) { return d.end.yAxis });

    svg.selectAll('.threshold-group')
        .data(thresholdData).enter()
        .append('g')
        .attr('class', d => d.name)
        .classed(`threshold-group`,true)
        .append('path')
        .attr('d', d => area(d.values));
}


function calcTickNum(dimentions, dist = {x: 80, y: 80}){
    let x = dimentions.x/dist.x;
    let y = dimentions.y/dist.y;
    return {x:x,y:y};
}


function createGrid(svg, scale, valuesNum, dimentions, direction = [true, true]) {
    if (direction[0]) {
        svg.append("g")
            .attr("class", "grid")
            .call(d3.axisBottom(scale.x)
                .ticks(valuesNum.x)
                .tickSize(dimentions.y)
                .tickFormat("")
            );
    }

    if (direction[1]) {
        svg.append("g")
            .attr("class", "grid")
            .call(d3.axisLeft(scale.y)
                .ticks(valuesNum.y)
                .tickSize(-dimentions.x)
                .tickFormat("")
            );
    }
}
    

function createAxes(svg, scale, valuesNum, xAxisName, yAxisName, dimentions) {
    let x = svg.append("g")
        .attr("class", "x-axis")
        .call(d3.axisBottom(scale.x).ticks(valuesNum.x));

    svg.append("text")
        .attr("class", "x-label")
        .text(xAxisName);


    let y = svg.append("g")
        .attr("class", "y-axis")
        .call(d3.axisLeft(scale.y).ticks(valuesNum.y));

    svg.append("text")
        .attr("class", "y-label")
        .text(yAxisName);

    return {x:x,y:y};
}



//Only for this
function name(name, returnType = "name", colours){
    if(name === "Existing"){
        return (returnType === "name" ? name : 
                returnType === "colour" ? colours[name] : "");
    }else{
        return (returnType === "name" ? name :
                returnType === "colour" ? colours["Path"] :
                returnType === "cost-name" ? name.substring(0, 1).toUpperCase() + name.substring(1,name.length) :
                returnType === "cost-colour" ? colours[name] : "");
    }
}

