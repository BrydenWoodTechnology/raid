
//Existing Slider
function removeExisting(){
    for(type in allGraphs){
        let existingIndex = allGraphs[type].findIndex(obj=>obj.name==="Existing_ver_0");
        if(existingIndex>-1){
            allGraphs[type].splice(existingIndex, 1);
        }
        
    }
}

let existingButtons = document.getElementById("existing-buttons");
let sliders = document.querySelectorAll("#rail-slider input");
function findExistingStations(){
    let start = parseInt(sliders[0].value);
    let end = parseInt(sliders[1].value);
    if (parseInt(sliders[0].value) > parseInt(sliders[1].value)) {
        start = parseInt(sliders[1].value);
        end = parseInt(sliders[0].value);
    }
    let selectedRoots = roots.slice(start, end);
    return selectedRoots;
}


// //Apply Button Function
// function handleApplyBtn(dataContainer){
//     let checked = listContainer.querySelectorAll("input:checked");
//     let notStored = [];

//     // Reinisialize colours
//     colours = {};
//     colours = {existing: exisitngColour};

//     for(let i=0; i<checked.length; i++){

//         // Create Colour Pallete
//         if(i > colourPallete.length){
//             colourPallete.push([[40,40,40],[(Math.random()*50)+200,(Math.random()*50)+200,(Math.random()*50)+200]]);
//         }
//         if(checked[i].id !== "e-a"){
//             let paths = checked[i].parentElement.parentElement.querySelectorAll(".check-row input");
//             let this_container = checked[i].parentElement.parentElement.parentElement.querySelector(".header LABEL").innerHTML;
//             let pathsColours = gradient(paths.length,colourPallete[i]);

//             if(!colours[this_container]) colours[this_container] = {};
//             paths.forEach((path,index)=>{colours[this_container][path.id] = pathsColours[index]});
//         }
        
//         let lineName = toLineID( checked[i].dataset.id, checked[i].dataset.version );
//         let index = (dataContainer ? dataContainer.findIndex(obj => obj.name === lineName) : -1);
//         (index < 0 && lineName!=="Existing_ver_0") ? notStored.push({new:checked[i].dataset.id, version:checked[i].dataset.version}) : "";

//     }
//     let existingLinks = {};
//     if(document.getElementById("e-a").checked){
//         existingLinks = {...createExistingLinks()};
//     }

//     if(notStored.length > 0){
//         let link1 = graphLink(mongo_design_collection, notStored, {x:"Index", y:"curvature"});
//         handleAllData(link1,existingLinks.curvature, st);

//     }else if(notStored.length === 0 && document.getElementById("e-a").checked){
//         handleAllData(undefined,existingLinks.curvature, st, "Curvature");
//     }else{
//         let selectedData1 = [...selectedLineData(allGraphs["Curvature"])];
//         updateGraph(`#curvature`, selectedData1, st);
//     }
// }


//Select for single-path graphs
function handleApplyBtn(selectedPath) {
    //Check Existing Alignment !
    let existingLinks = {};
    existingLinks.curvature = undefined;
    if (document.getElementById("e-a").checked) {
        existingLinks = {};
        existingLinks = { ...createExistingLinks() };
    }
    //COST !
    let linkCost = `${collectionLink(mongo_database, mongo_design_collection)}/aggregate?pipeline=[{%22$match%22:{%22$and%22:[{"${sessionID}":"${lastSession}"},{%22parent_ID%22:"${selectedPath.dataset.id}"},{%22alignment_type%22:"${selectedPath.dataset.type}"},{%22version%22:${selectedPath.dataset.version}},{%22Index%22:{%22$ne%22:null}},{%22land_uses_proximity%22:{%22$ne%22:null}}]}},{%22$project%22:{%22parent_ID%22:%22$parent_ID%22,%22Index%22:%22$Index%22,%22version%22:%22$version%22,%22cost%22:%22$land_uses_proximity%22}}]&pagination=false`;
    getRequestAsync(linkCost, toMultipleLineGraph, "cost", `#cost`, "cost");
    //SPEED !
    let linkSpeed = `${collectionLink(mongo_database, mongo_design_collection)}/aggregate?pipeline=[{%22$match%22:{%22$and%22:[{"${sessionID}":"${lastSession}"},{%22parent_ID%22:"${selectedPath.dataset.id}"},{%22version%22:${selectedPath.dataset.version}},{%22alignment_type%22:"${selectedPath.dataset.type}"},{"Index":{"$ne":null}}]}},{%22$project%22:{%22speed.permissible_speed%22:%22$permissible_speed%22,%22speed.actual_speed%22:%22$actual_speed%22,%22parent_ID%22:%22$parent_ID%22,%22version%22:%22$version%22,%22Index%22:%22$Index%22}}]&pagination=false`;
    getRequestAsync(linkSpeed, toMultipleLineGraph, "speed", `#speed`, "speed", "line");
    //CUT N FILL !
    let bigThreshold = 60;
    let lowThreshold = -40;
    let linkCutNFill = `${collectionLink(mongo_database, mongo_design_collection)}/aggregate?pipeline=[{%22$match%22:{%22$and%22:[{"${sessionID}":"${lastSession}"},{%22parent_ID%22:"${selectedPath.dataset.id}"},{%22version%22:${selectedPath.dataset.version}},{%22alignment_type%22:"${selectedPath.dataset.type}"},{"Index":{"$ne":null}}]}},{%22$project%22:{%22elevation_type%22:{%22$cond%22:{%22if%22:{%22$and%22:[{%22$gte%22:[%22$elevation_difference%22,${lowThreshold}]},{%22$lte%22:[%22$elevation_difference%22,${bigThreshold}]}]},%22then%22:{%22$cond%22:{%22if%22:{%22$gte%22:[%22$elevation_difference%22,0]},%22then%22:%22cut%22,%22else%22:%22fill%22}},%22else%22:{%22$cond%22:{%22if%22:{%22$gte%22:[%22$elevation_difference%22,0]},%22then%22:%22tunnel%22,%22else%22:%22bridge%22}}}},%22ID%22:%22$ID%22,%22version%22:%22$version%22,%22Index%22:%22$Index%22,%22alignment_elevation%22:1,%22ground_elevation%22:1}}]&pagination=false`;
    getRequestAsync(linkCutNFill, toCutNFill);
    // //GRADIENT
    let gradientY = "gradient_smooth";
    let boolLabel = "gradient_labels";
    let stringLabel = "gradient_ratio";
    let linkGradient = `${collectionLink(mongo_database, mongo_design_collection)}/aggregate?pipeline=[{%22$match%22:{%22$and%22:[{"${sessionID}":"${lastSession}"},{%22parent_ID%22:"${selectedPath.dataset.id}"},{%22version%22:${selectedPath.dataset.version}},{%22alignment_type%22:"${selectedPath.dataset.type}"},{"Index":{"$ne":null}}]}},{%22$project%22:{%22ground_elevation%22:%22$${gradientY}%22,%22boolean%22:%22$${boolLabel}%22,%22label%22:%22$${stringLabel}%22,%22parent_ID%22:%22$parent_ID%22,%22version%22:%22$version%22,%22Index%22:%22$Index%22}}]&pagination=false`;
    getRequestAsync(linkGradient, toGradientGraph);

    //CURVATURE !
    let linkCurvature = `${collectionLink(mongo_database, mongo_design_collection)}/aggregate?pipeline=[{%22$match%22:{%22$and%22:[{"${sessionID}":"${lastSession}"},{%22alignment_type%22:"${selectedPath.dataset.type}"},{%22parent_ID%22:"${selectedPath.dataset.id}"},{%22version%22:${selectedPath.dataset.version}},{%22Index%22:{%22$ne%22:null}},{%22curvature%22:{%22$ne%22:null}}]}},{%22$project%22:{%22parent_ID%22:%22$parent_ID%22,%22Index%22:%22$Index%22,%22version%22:%22$version%22,%22curvature%22:%22$curvature%22}}]&pagination=false&hateoas=false`;
    handleAllData(linkCurvature, existingLinks.curvature, { x: 3 * 0.000621371192, y: 1 }, st,"curvature");

}



//-----------Events
// let applyButton = document.getElementById("apply-paths");
// applyButton.addEventListener("click",e=>{
//     handleApplyBtn(document.querySelector("#designed-paths input:checked"));
// });


//Toggle Graph buttons
let alignments = document.getElementsByClassName("menu")[0];
alignments.addEventListener("click",e=>{
    if(e.target.classList.contains("menu-label")){
        e.target.classList.toggle("selected");
    }
    if(e.target.localName === "input" && (e.target.type === "radio" || e.target.type === "checkbox")){
        handleApplyBtn(document.querySelector("#designed-paths input:checked"));
    }
});

// let graphContainer = document.getElementsByClassName("container")[0];
// graphContainer.addEventListener("click",e=>{
//     if(e.target.parentElement.classList.contains("graph-select")){
//         let graphParent = e.target.parentElement.parentElement.querySelector(".graph");
//         handleSelect(e.target, graphParent); 
//     }
// });


//-----------------------------------------------------------Events
d3.select(window).on('resize', resize); 

function resize() {
    toMultipleLineGraph(undefined, "cost", "#cost", "cost");
    toMultipleLineGraph(undefined, "speed", `#speed`, "speed", "line");
    updateGraph("#cut_n_fill", singlelineGraphs.cut_n_fill, undefined, thisColours, "cost-name", "cost-colour", "line", mixGraph);   
    updateGraph(`#curvature`, singlelineGraphs.curvature, st, thisColours, "name", "colour");
    updateGraph("#gradient", singlelineGraphs.gradient, undefined, thisColours, "cost-name", "cost-colour", "line", gradientGraph);
}
