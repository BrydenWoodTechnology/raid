let thresholds = {
    "extreme_small": [
        0.0033333333333333335
    ],
    "small": [
        0.0033333333333333335,
        0.002
    ],
    "medium": [
        0.002,
        0.001
    ],
    "big": [
        0.001,
        0.0002
    ],
    "extreme_big": [
        0.0002
    ],
    "straight": [
        0,
        0.000125
    ]
}
