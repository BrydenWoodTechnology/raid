//Variables
let username_password = "19029-RAID:wgDtXJFhTrVR";

let mongoLink = "http://api-node.bwtcreativetech.io";
let mongo_database = "19029-RAID-NR";
let mongo_existing_collection = "Existing_Alignment";
let mongo_design_collection = "Design_Alignments_test";//"Design_Alignments_test";//"Design_Alignments";
let maxPageNumber = 100;

let dataArray = [];
let allGraphs = {};


//----------------------------------------------------------------------------General Request functions
function getRequestAsync(requestURL, f, param1, param2, param3, param4) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var returnStr = xhr.responseText;
            let source = JSON.parse(returnStr);
            if(f !== undefined){
                f(source, param1, param2, param3, param4);
            }else{
                return source;
            }
        }
    };

    xhr.open("GET", requestURL, true);
    xhr.setRequestHeader("Authorization", "Basic " + btoa(username_password));
    xhr.send(null);
}





async function getRequestPromiseAsync(requestURL) {
 
    let promise = new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function () {
    // below only runs if request is completed successfully. Handle other situations.
    if (this.readyState == 4 && this.status == 200) {
    
    // Typical action to be performed when the document is ready.
    resolve(xhr.responseText);
    // return returnStr;
    }
    };
    
    xhr.open("GET", requestURL, true);
    // //turn credentials to base64 endcoded and set request header
    xhr.setRequestHeader("Authorization", "Basic " + btoa(username_password));
    xhr.send(null);
    
    });
    
    let result = await promise;
    return JSON.parse(result);
   }


//----------------------------------------------Request Link
function collectionLink(database, collection){
    return `${mongoLink}/${database}/${collection}`;
}

function filterPaths(paths,axes){
    let filters = ``;
    for(let i=0; i<paths.length; i++){
        filters += `{"$and":[{"parent_ID":"${paths[i].new}"},{"version":${parseInt(paths[i].version)}},{"${axes.x}":{"$ne":null}},{"${axes.y}":{"$ne":null}},{"${axes.y}":{"$gte":0}}]}`;
        filters += (i < paths.length-1 ? `,` : "");
    }
    filters = `{"$or":[${filters}]}`;
    return `{"$match":${filters}}`;
}

function projectData(axes){
   return `{"$project":{"${axes.x}":"$${axes.x}","${axes.y}":"$${axes.y}","parent_ID":"$parent_ID","version":"$version"}}`;
}

function filterRoots(roots){
 let filters = ``;
 for (i=0; i<roots.length; i++){
     filters += `{"Label":"${roots[i]}"}`;
     filters += (i < roots.length-1 ? `,` : "");
 }
 return `{"$match":{"$or":[${filters}]}}`;
}

function graphLink(collection, paths, axes){
    let genLink = collectionLink(mongo_database, collection);
    let filters = (collection !== mongo_existing_collection ? `${filterPaths(paths,axes)},` : `${filterRoots(paths)},`);
    let project = projectData(axes);
    return `${genLink}/aggregate?pipeline=[${filters}${project}]&pagination=false`;
}



// Cost
let costArray = [
    "Building",
    "CarChargingPoint",
    "DTM",
    "ElectricityTransmissionLine",
    "Foreshore",
    "Glasshouse",
    "Greenspaces",
    "ImportantBuilding",
    "MotorwayJunction",
    "NationalParks",
    "RailwayStation",
    "RailwayTrack",
    "RailwayTunnel",
    "Road",
    "RoadTunnel",
    "Roundabout",
    "Slope",
    "SurfaceWater_Area",
    "SurfaceWater_Line",
    "TidalBoundary",
    "TidalWater",
    "Woodland"
];


function projectLine(costArray){
    let filter = `"parent_ID":"$parent_ID","Index":"$Index","version":"$version"`;
    // for (let i=0; i<costArray.length; i++){
    //     filter += `,"cost.${costArray[i]}":{"$ifNull":["$${costArray[i]}",0]}`
    // }
    filter += `"cost":"$cost"`;
    filter = `{"$project":{${filter}}}`;
    return filter;
}


function costQuery(collection ,paths, cost = costArray){
    let genLink = collectionLink(mongo_database, collection);
    let project = projectLine(cost);
    let match = filterPaths(paths,{x:"Index",y:"land_uses_proximity"});
    return `${genLink}/aggregate?pipeline=[${match},${project}]&pagination=false`;
}

//------------------------------------------------------To stations
let stationList = `${mongoLink}/${mongo_database}/${mongo_existing_collection}/distinct?fields=Label`;
getRequestAsync(stationList, toStations);




//------------------------------------------------------------------------New Path-List
let newVersion = "parent_ID";
let version = "version";
let type = "alignment_type";
let sessionID = "session_ID";

let codeName =  localStorage.getItem("codeName"); 
let lastSession = codeName.substring(0,8)+codeName.substring(codeName.length-8,codeName.length);

let pathListLink = `${mongoLink}/${mongo_database}/${mongo_design_collection}/aggregate?pipeline=[{"$match":{"$and":[{"${sessionID}":"${lastSession}"},{"${newVersion}":{"$exists":true}},{"${version}":{"$exists":true}},{"${type}":{"$exists":true}}]}},{"$group":{"_id":"$${newVersion}","versions":{"$addToSet":{"$concat":["$${type}","_ver_",{"$toString":"$${version}"}]}}}}]&hateoas=false`;
getRequestAsync(pathListLink, toPathList, listContainer);


//---------------------------------------------------------------------------Mongo Input Functions

//Create Nested Path List
function addExisting(data){
    data.splice(0, 0, {_id:"Existing", versions:[]});
    return data;
}

function toPathList(source, tag){
    let dataFormat = addParentVersion(source);
    dataFormat.sort((a,b)=> a._id > b._id);
    updateNestedList(dataFormat, tag);

    handleApplyBtn(document.querySelector("#designed-paths input:checked"));
}


function addParentVersion(source){
    let dataFormat = [];

    for(let i=0; i<source.length; i++){
        let pathIndex = source[i].versions.findIndex(value=>value.split("_ver_")[0]==="new");
        dataFormat.push(source[i]);
        dataFormat[i].path_version = pathIndex > -1 ? source[i].versions[pathIndex].split("_ver_")[1] : i;
    }

    return dataFormat;
}


function addNew(source){
    let newIndex = source.findIndex(obj=>obj.versions[0].split("_ver_")[0]==="new");

    if (newIndex > -1) {
        let newList = [...source];
        newList.splice(newIndex, 1);
        let originalVersions = {...source[newIndex]};
        let dataFormat = [];
        for (let i = 0; i < newList.length; i++) {
            let originalIndex = originalVersions.versions.findIndex(version => version === newList[i]._id.split("_ver_")[1]);
            let originalList = [];
            if (originalIndex !== -1){
                originalList = ["new"];
                originalVersions.versions.splice(originalIndex, 1);
            }

            let newObject = {
                "_id": newList[i]._id,
                "versions": originalList.concat(newList[i].versions)
            };
            dataFormat.push(newObject);
        }
        for(let i=0; i < originalVersions.versions.length; i++){
            let newObject = {
                "_id": `${originalVersions._id}_ver_${originalVersions.versions[i].split("_ver_")[1]}`,
                "versions": ["new"]
            };
            dataFormat.push(newObject);
        }
        return dataFormat;
    }else{
        return source;
    }
}




function rootDiv(index,station){
    let div = createTag("div",["data-id"],[index],undefined,undefined,station);
    document.getElementById("labels").appendChild(div);
}

let roots = [];
function toStations(source){
    roots = source.Label;
    for(let i=0; i<source.Label.length; i++){
        let station = source.Label[i].split("-")[0];
        rootDiv(i,station);
        if(i === source.Label.length-1 ){
            let last_station = source.Label[i].split("-")[1];
            rootDiv(i+1,last_station);
        }
    }
}





function createExistingLinks(){
    removeExisting();
    let roots = findExistingStations();

    let links = {};
    links.curvature = graphLink(mongo_existing_collection, roots, {x:"Index", y:"curvature"});
    return links;
}



function addToGraphList(source){
    let newData = source._items;
    //Attributes
    let axesAtt = {x:"Index"};
    axesAtt.y = "";
    for(item in source._items[0]){
        if(item !== "_links" && item !=="_id" && item !== "Index" && item !== "parent_ID" && item !== "version") {
            axesAtt.y = item;
        }
    }
    //Axes Names
    let axesNames = {x: "Mileage"};
    axesNames.y = axesAtt.y.charAt(0).toUpperCase() + axesAtt.y.slice(1);
    //Constant values
    let dataConst = {x:3*0.000621371192,y:1};

    //Update AllGraph data
    if (allGraphs[axesNames.y] === undefined){
        allGraphs[axesNames.y] = [];
    }
    allGraphs[axesNames.y] = toLineGraphData(allGraphs[axesNames.y], newData, axesAtt.x, axesAtt.y, dataConst);

    return {axesAtt, axesNames};
}



async function handleAllData(designedLink, existingLink, dataConst, selectedThresholds, graphArray){
    singlelineGraphs[graphArray] = [];

    if(existingLink !== undefined){
        let existingSource = await getRequestPromiseAsync(existingLink);
        singlelineGraphs[graphArray] = [...toLineGraphData(singlelineGraphs[graphArray], existingSource._items, "Index", graphArray, dataConst)];
    }

    if(designedLink !== undefined){
        let designedSource = await getRequestPromiseAsync(designedLink);
        singlelineGraphs[graphArray] = [...toLineGraphData(singlelineGraphs[graphArray], designedSource, "Index", graphArray, dataConst)];
    }

    updateGraph(`#${graphArray}`, singlelineGraphs[graphArray], selectedThresholds, thisColours, "name", "colour");
}





let singlelineGraphs = {
    speed:[],
    curvature:[],
    gradient: [],
    cost: [],
    cut_n_fill: []
}


let thisColours = {
    "Existing": "#33a8a8",
    "Path": "#8ac5ff",

    "vertical_lines": "#8ac5ff",

    "permissible_speed":"#5289bf",
    "actual_speed":"#b66dff",

    "cut": "#8ac5ff",
    "fill": "#33a8a8",
    "bridge": "#b66dff",
    "tunnel": "#652c49",
    "ground_elevation": "#05353cd4",

    "Building":"#074650",
    "CarChargingPoint":"#05353cd4",
    "DTM":"#006e6e",
    "FunctionalSite":"#006e6e",
    "ElectricityTransmissionLine":"#4ffd4f",
    "Foreshore":"#33a8a8",
    "Glasshouse":"#3b546d",
    "Greenspaces":"#5289bf",
    "ImportantBuilding":"#8ac5ff",
    "MotorwayJunction":"#c4e1fe",
    "NationalParks":"#6d33a7",
    "RailwayStation":"#b66dff",
    "RailwayTrack":"#652c49",
    "RailwayTunnel":"#82395e",
    "Road":"#82395e",
    "RoadTunnel":"#ff2d97",
    "Roundabout":"#bf5289",
    "Slope":"#fe6db6",
    "SurfaceWater_Area":"#bf88a4",
    "SurfaceWater_Line":"#feb5da",
    "TidalBoundary":"#6d3600",
    "TidalWater":"#ad5600",
    "Woodland":"#fb9a3a"
    };

function toMultipleLineGraph(source, yContainer, tagSelector, listName, graphType = "area"){
    let newData;
    let structeredList = [];
    let axesAtt = {x:"Index"};
    axesAtt.y = "";
    let dataConst = {x:3*0.000621371192,y:1};
    
    if(source !== undefined){
        let newData = source._items;

        for(key in newData[0][yContainer]){
            let sum = newData.reduce((acc, cur) => {	
                return acc + cur[yContainer][key];
                }, 0);
            if(sum > 0 && key !== "DTM"){
                axesAtt.y = key;
                structeredList = toLineGraphData(structeredList, newData, axesAtt.x, axesAtt.y, dataConst, key, yContainer); 
            }
        }

        singlelineGraphs[listName] = [];
        singlelineGraphs[listName] = [...structeredList];
    } 

    updateGraph(tagSelector, singlelineGraphs[listName], undefined, thisColours, "cost-name", "cost-colour", graphType);
}


function toCutNFill(source = cutNfillJson){
    singlelineGraphs.cut_n_fill = [];
    singlelineGraphs.cut_n_fill = toCutNFillData(source._items, {x:3*0.000621371192,y:1});

    updateGraph("#cut_n_fill", singlelineGraphs.cut_n_fill, undefined, thisColours, "cost-name", "cost-colour", "line", mixGraph);
}

function toGradientGraph(source){
    singlelineGraphs.gradient = [];
    singlelineGraphs.gradient = [...toGradientGraphData(source._items, {x:3*0.000621371192,y:1})];

    updateGraph("#gradient", singlelineGraphs.gradient, undefined, thisColours, "cost-name", "cost-colour", "line", gradientGraph);
}

function toCurvatureGraph(source){
    singlelineGraphs.curvature = [];
    singlelineGraphs.curvature = [...toGradientGraphData(source._items, {x:3*0.000621371192,y:1})];
}