//Variables
let lineReference = "Test_Alignment";
let lineData1 = [];
let lineData2 = [];
let lineData3 = [];


let st = {
    "extreme_small": thresholds["extreme_small"],
    "straight": thresholds["straight"]
}


let exisitngColour = "#31a09e";
let colours = {existing: exisitngColour};
var colourPallete = [
    [[52,149,199], [146, 216, 252]],
    [[74,140,127],[133, 255, 232]],
    [[102,75,141], [204, 168, 255]],
    [[176,91,144], [255, 150, 214]],
    [[163,79,79], [255, 163, 163]],
    [[171, 118, 53],[255, 207, 145]],
    [[148, 140, 86],[255, 243, 168]],
    [[121, 143, 93],[207, 255, 150]],
    [[67, 105, 70],[153, 255, 162]],
    [[91, 135, 119],[191, 255, 250]],
];




//Convert data for line graph
function toLineGraphData(dataContainer,newData, x, y, dataConst = {x:1,y:1}, name, yParent){
    for(let i=0; i<newData.length; i++){
        let lineName = toLineID( newData[i].parent_ID, newData[i].version );
        if(name) lineName = name;
        let index = dataContainer.findIndex(obj => obj.name === lineName);
        if(index < 0){
            let newLine = { name: lineName, values: [] };
            dataContainer.push(newLine);
            index = dataContainer.length-1;
        }

        let yAxis = (parseFloat( (yParent ? newData[i][yParent][y] : newData[i][y]) )  * dataConst.y);
        if(yAxis === null || yAxis === undefined || yAxis === NaN){
            yAxis = 0;
        }

        let values = {
            xAxis: (parseFloat(newData[i][x]) * dataConst.x),
            yAxis: yAxis
        };

        dataContainer[index].values.push(values);
    }
    for(container of dataContainer){
        container.values.sort(function(a,b) { return a.xAxis - b.xAxis });

        let minX = container.values[0].xAxis;
        container.values.forEach(obj=>{obj.xAxis -= minX;})
    }
    return dataContainer;
}


function selectedLineData(dataContainer, menuItem=menu, dataTitle = undefined){
    let checked = menuItem.querySelectorAll("input[type=checkbox]:checked");
     if(checked.length === 0){
        return "";
    }else{
         let selectedAll = [];
         for(let i=0; i<checked.length; i++){
            let lineName = toLineID( checked[i].dataset.id, checked[i].dataset.version );
            let index = dataContainer.findIndex(obj => obj.name === lineName);
            let selected = {};
            if(index > -1) {
                selected = {...dataContainer[index]};
                selectedAll.push(selected);
            }
         }
         return selectedAll;
     }
}


function toCutNFillData(data, dataConst = {x:1,y:1}){
    let ground = {
        name:"Ground_elevation",
        values: [],
        type:"line",
        colour:"ground_elevation"
    };

    let alignment = {
        cut:[],
        fill:[],
        bridge:[],
        tunnel:[]
    }

    for(let i=0; i<data.length; i++){
        ground.values.push({
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis0: 0,
            yAxis1: (parseFloat(data[i].ground_elevation) * dataConst.y)
        });

        let alignmentType = data[i].elevation_type === "cut" || data[i].elevation_type === "fill" ? "area" : "line";

        let alignmentCoords = {
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis0: (parseFloat(data[i].ground_elevation) * dataConst.y),
            yAxis1: (parseFloat(data[i].alignment_elevation) * dataConst.y),
            index:  data[i].Index
        }

        let thisType = alignment[data[i].elevation_type];
        let thisLast;
        let lastxAxis;
        if(thisType.length > 0){
            thisLast = thisType[thisType.length-1].values;
            lastxAxis = thisLast[thisLast.length-1].index;
        }

        if( lastxAxis + 1 === data[i].Index ){
            thisLast.push(alignmentCoords);
        }else{
            let newLine = {
                name: data[i].elevation_type + thisType.length,
                values: [alignmentCoords],
                type: alignmentType,
                colour: data[i].elevation_type
            }

            alignment[data[i].elevation_type].push(newLine);
        }
    }
    
    let allAlignments = [...alignment.cut, ...alignment.fill, ...alignment.tunnel, ...alignment.bridge];
    let final = [ground].concat(allAlignments);
    return final;
}


function toGradientGraphData(data, dataConst = {x:1,y:1}){
    let gradientProfile = {
        name: "Gradient_Profile",
        values : [],
        colour : "ground_elevation"
    }

    let dataStructure = [];

    for (let i=0; i<data.length; i++){
        let newv = {
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis: (parseFloat(data[i].ground_elevation) * dataConst.y)
        };

        gradientProfile.values.push(newv);

        if(data[i].boolean){
            let verticalStructure = [
                {
                    xAxis: (parseFloat(data[i].Index) * dataConst.x),
                    yAxis: 0,
                },
                {
                    xAxis: (parseFloat(data[i].Index) * dataConst.x),
                    yAxis: (parseFloat(data[i].ground_elevation) * dataConst.y),
                },
            ];

            let thisLabel = `${data[i].label}`;
            thisLabel = "1:" + thisLabel.split(".")[0].replace("-","");

            let newLabel = {
                name: thisLabel,
                values: [...verticalStructure],
                colour : "vertical_lines"
            }

            dataStructure.push(newLabel);
        }
    }

    dataStructure.push(gradientProfile);
    return dataStructure;
}


function updateGraph(parentTag, data, selectedThresholds, colourList = colours, name_type, colour_type, type, graphFunction = linearGraph){
    document.querySelector(parentTag).innerHTML = "";
    document.querySelector(`${parentTag}-legend`).innerHTML = "";
    graphFunction(parentTag, data, selectedThresholds, colourList, name_type, colour_type, type);
}


function toThresholds(selectedThresholds, width, scaleY){
    let thresholdXY = [];
    for(item in selectedThresholds){
        let name = item;
        let value = [
            {
                start:{
                    xAxis: 0,
                    yAxis: scaleY(selectedThresholds[item][0])
                },
                end:{
                    xAxis: 0,
                    yAxis: selectedThresholds[item][1] ? scaleY(selectedThresholds[item][1]) : 0
                },
            },
            {
                start:{
                    xAxis: width,
                    yAxis: scaleY(selectedThresholds[item][0])
                },
                end:{
                    xAxis: width,
                    yAxis: selectedThresholds[item][1] ? scaleY(selectedThresholds[item][1]) : 0
                },
            }
        ]
        let newThreshold = {name:name, values:value};
        thresholdXY.push(newThreshold);
    }
    return thresholdXY;
}


function toLineID( id, version ){
    if(id && version){
        let thisContainer = document.querySelector(`input[data-id="${id}"]`).parentElement.parentElement.parentElement;
        let path = thisContainer.querySelector(".header LABEL").innerHTML;
        return `${path}_V_${version}`
    }else{
        return "Existing"
    }
}



//------------------------------------------------------------Colour Pallete
//HSL to RGB
function HSLToRGB(h, s, l) {
    s /= 100;
    l /= 100;

    let c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c / 2,
        r = 0,
        g = 0,
        b = 0;
    if (0 <= h && h < 60) {
        r = c; g = x; b = 0;
    } else if (60 <= h && h < 120) {
        r = x; g = c; b = 0;
    } else if (120 <= h && h < 180) {
        r = 0; g = c; b = x;
    } else if (180 <= h && h < 240) {
        r = 0; g = x; b = c;
    } else if (240 <= h && h < 300) {
        r = x; g = 0; b = c;
    } else if (300 <= h && h < 360) {
        r = c; g = 0; b = x;
    }
    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return [r,g,b];
}


function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


function gradient(length, cols) {
    // let steps = (length / (cols.length - 1)) + 1; //The number of colours between two colours of the pallete
    let gradient = [];

    let steps = length;
    if(steps === 0) steps = 1;
    
    for (let i = 0; i < cols.length - 1; i++) {
        let alpha = 0.0;

        for (let j = 0; j < steps; j++) {
            let c = [];
            alpha += (1.0 / steps);
            c[0] = parseInt(cols[i + 1][0] * alpha + (1 - alpha) * cols[i][0]);
            c[1] = parseInt(cols[i + 1][1] * alpha + (1 - alpha) * cols[i][1]);
            c[2] = parseInt(cols[i + 1][2] * alpha + (1 - alpha) * cols[i][2]);

            let colour = rgbToHex(c[0], c[1], c[2]);
            // let colour = {color:`rgb(${c[0]},${c[1]},${c[2]})`};
            gradient.push(colour);
        }
    }
    return gradient;
}





