//Convert text into id - remove unwanted characters
function textToHTMLattribute(value, lowerCase = false) {
    let filter = new RegExp('[^a-zA-Z0-9/_-]', 'g'); //Match all characters except letters, numbers, "-", "_"
    let unwanted = value.match(filter);
    let id = value;
    if (unwanted !== null) {
        for (char of unwanted) {
            id = id.replace(char, "");
        }
    }
    if(lowerCase) id = id.toLowerCase();
    return id;
}


//Set tag's attributes, classes, id
function setTag(element, attributeName, attributeValue, classes, ObjectId, text){
    if(attributeName !== undefined){
        for (let i = 0; i < attributeName.length; i++) {
            element.setAttribute(textToHTMLattribute(attributeName[i],true), attributeValue[i]);
        }
    }
    if (classes !== undefined) {
        for (let i = 0; i < classes.length; i++) {
            element.className += " " + textToHTMLattribute(classes[i]);
        }
    }
    if (ObjectId !== undefined) {
        element.id = textToHTMLattribute(ObjectId);
    }

    if(text !== undefined){
        let textnode = document.createTextNode(text);
        element.appendChild(textnode);
    }  
}


//Create a tag
function createTag(elementType, attributeName, attributeValue, classes, ObjectId, text) {
    let element = document.createElement(elementType);
    setTag(element, attributeName, attributeValue, classes, ObjectId, text);
    return element;
}


//Add an element to list
function addToList(item, itemAttribute = undefined, parentTag, id = undefined, tagType = "option", attributeNames, attributeValues) {
    let name = item;
    if (itemAttribute) name = item[itemAttribute];
    let thisTag;
    thisTag = createTag(tagType, attributeNames, attributeValues, undefined, id, name);
    parentTag.appendChild(thisTag);
}

//Raid List
function pathName(id, version){
    let pathName = id.split("_ver_")[1];
    let pathVersion = (version === "new" ? 0 : version.split("_ver_")[1]);
    return `Path_${pathName} V_${pathVersion}`
}

function createDataset(id, version, name){
    let _id = (version === "new" ? id.split("_ver_")[0] : id);
    let _version = (version === "new" ? id.split("_ver_")[1] : version.split("_ver_")[1]);
    let _type = version.split("_ver_")[0];
    return {names:["data-id", "data-version", "data-type", "data-name"], values:[_id, _version, _type, name]};
}

function createRaidList(source, sourceAttribute = undefined, parentTag, id = undefined, tagType = undefined) {
    for(obj in source){
        for (item in source[obj]["versions"]) {
            let name = pathName(source[obj]["_id"], source[obj]["versions"][item]);
            let attributes = createDataset(source[obj]["_id"], source[obj]["versions"][item], name);
            let thisId;
            if(id) thisId = source[item];
            addToList(name, sourceAttribute, parentTag, thisId, tagType, attributes.names, attributes.values);
        }
    }
}

//Remove all items from a category
function clearList(category) {
    if (category) {
        while (category.firstChild) {
            category.removeChild(category.firstChild);
        }
    }
}



//Update a list of elements
function updateList(f, source, sourceAttribute = undefined, listTag, id = undefined, tagType = undefined){
    clearList(listTag);
    f(source, sourceAttribute, listTag, id, tagType);
}

function updateAllLists(f,tagsSelector, source, sourceAttribute = undefined, id = undefined, tagType = undefined){
    let listTags = document.querySelectorAll(tagsSelector);
    for(let i=0; i<listTags.length; i++){
        updateList(f, source, sourceAttribute, listTags[i], id, tagType);
    }
}