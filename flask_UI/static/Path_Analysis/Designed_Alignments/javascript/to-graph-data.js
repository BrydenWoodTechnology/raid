//Variables
let st = {
    "extreme_small": thresholds["extreme_small"],
    "straight": thresholds["straight"]
}

let singlelineGraphs = {
    speed: [],
    curvature: [],
    gradient: [],
    cost: [],
    cut_n_fill: []
}

let thisColours = {
    "Existing": "#33a8a8",
    "Path": "#8ac5ff",

    "vertical_lines": "#8ac5ff",
    "gradient_smooth": "#fb9a3a",

    "permissible_speed": "#5289bf",
    "actual_speed": "#b66dff",

    "cut": "#ff2d97",
    "fill": "#8adaff",
    "bridge": "#b66dff",
    "tunnel": "#00b896",
    "ground_elevation": "#fb9a3a",

    "Building": "#006b5f",
    "CarChargingPoint": "#05353cd4",
    "DTM": "#006e6e",
    "FunctionalSite": "#006e6e",
    "ElectricityTransmissionLine": "#4ffd4f",
    "Foreshore": "#33a8a8",
    "Glasshouse": "#3b546d",
    "Greenspaces": "#5289bf",
    "ImportantBuilding": "#8ac5ff",
    "MotorwayJunction": "#c4e1fe",
    "NationalParks": "#6d33a7",
    "RailwayStation": "#b66dff",
    "RailwayTrack": "#652c49",
    "RailwayTunnel": "#82395e",
    "Road": "#82395e",
    "RoadTunnel": "#ff2d97",
    "Roundabout": "#bf5289",
    "Slope": "#fe6db6",
    "SurfaceWater_Area": "#bf88a4",
    "SurfaceWater_Line": "#feb5da",
    "TidalBoundary": "#6d3600",
    "TidalWater": "#ad5600",
    "Woodland": "#fb9a3a"
};



//---------------------------------------------------------Corvert data to the right format for the graphs
function toLineID(id, version) {
    if (id && version) {
        let thisContainer = document.querySelector(`input[data-id="${id}"]`).parentElement.parentElement.parentElement;
        let path = thisContainer.querySelector(".header LABEL").innerHTML;
        return `${path}_V_${version}`
    } else {
        return "Existing"
    }
}

function toLineGraphData(dataContainer, newData, x, y, dataConst = { x: 1, y: 1 }, name, yParent) {
    for (let i = 0; i < newData.length; i++) {
        let lineName = toLineID(newData[i].parent_ID, newData[i].version);
        if (name) lineName = name;
        let index = dataContainer.findIndex(obj => obj.name === lineName);
        if (index < 0) {
            let newLine = { name: lineName, values: [] };
            dataContainer.push(newLine);
            index = dataContainer.length - 1;
        }

        let yAxis = (parseFloat((yParent ? newData[i][yParent][y] : newData[i][y])) * dataConst.y);
        if (yAxis === null || yAxis === undefined || yAxis === NaN) {
            yAxis = 0;
        }

        let values = {
            xAxis: (parseFloat(newData[i][x]) * dataConst.x),
            yAxis: yAxis
        };

        dataContainer[index].values.push(values);
    }
    for (container of dataContainer) {
        container.values.sort(function (a, b) { return a.xAxis - b.xAxis });

        let minX = container.values[0].xAxis;
        container.values.forEach(obj => { obj.xAxis -= minX; })
    }
    return dataContainer;
}




function toCutNFillData(data, dataConst = { x: 1, y: 1 }) {
    let ground = {
        name: "Ground_elevation",
        values: [],
        type: "line",
        colour: "ground_elevation"
    };

    let alignment = {
        cut: [],
        fill: [],
        bridge: [],
        tunnel: []
    }

    for (let i = 0; i < data.length; i++) {
        ground.values.push({
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis0: 0,
            yAxis1: (parseFloat(data[i].ground_elevation) * dataConst.y)
        });

        let alignmentType = data[i].elevation_type === "cut" || data[i].elevation_type === "fill" ? "area" : "line";

        let alignmentCoords = {
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis0: (parseFloat(data[i].ground_elevation) * dataConst.y),
            yAxis1: (parseFloat(data[i].alignment_elevation) * dataConst.y),
            index: data[i].Index
        }

        let thisType = alignment[data[i].elevation_type];
        let thisLast;
        let lastxAxis;
        if (thisType.length > 0) {
            thisLast = thisType[thisType.length - 1].values;
            lastxAxis = thisLast[thisLast.length - 1].index;
        }

        if (lastxAxis + 1 === data[i].Index) {
            thisLast.push(alignmentCoords);
        } else {
            let newLine = {
                name: data[i].elevation_type + thisType.length,
                values: [alignmentCoords],
                type: alignmentType,
                colour: data[i].elevation_type
            }

            alignment[data[i].elevation_type].push(newLine);
        }
    }

    let allAlignments = [...alignment.cut, ...alignment.fill, ...alignment.tunnel, ...alignment.bridge];
    let final = [ground].concat(allAlignments);
    return final;
}




function toGradientGraphData(data, dataConst = { x: 1, y: 1 }) {
    let gradientProfile = {
        name: "Gradient_Profile",
        values: [],
        colour: "gradient_smooth"
    }

    let dataStructure = [];

    for (let i = 0; i < data.length; i++) {
        let newv = {
            xAxis: (parseFloat(data[i].Index) * dataConst.x),
            yAxis: (parseFloat(data[i].gradient_smooth) * dataConst.y)
        };

        gradientProfile.values.push(newv);

        if (data[i].boolean) {
            let verticalStructure = [
                {
                    xAxis: (parseFloat(data[i].Index) * dataConst.x),
                    yAxis: 0,
                },
                {
                    xAxis: (parseFloat(data[i].Index) * dataConst.x),
                    yAxis: (parseFloat(data[i].gradient_smooth) * dataConst.y),
                },
            ];

            let thisLabel = `${data[i].label}`;
            thisLabel = "1:" + thisLabel.split(".")[0].replace("-", "");

            let newLabel = {
                name: thisLabel,
                values: [...verticalStructure],
                colour: "vertical_lines"
            }

            dataStructure.push(newLabel);
        }
    }

    dataStructure.push(gradientProfile);
    return dataStructure;
}





