//Uncheck existing alignment
document.getElementById("e-a").checked = false;

//Variables
let allGraphs = {};

let designedAlignments;
let existingAlignment;


var linkList = [];
var designedAlignments_codeName;
function updateMetricPath(){

    designedAlignments_codeName = linkList[0].split(`codeName=`)[1];
    createListOfPaths();

}

function designedAlignments_path(selectedPath){
    let json_name = `${selectedPath.dataset.id}_${selectedPath.dataset.type}_v${selectedPath.dataset.version}.json`;
    let path = `/metrics?fileName=${json_name}&codeName=${designedAlignments_codeName}`;
    return path;
}

// let common_path_steps =  linkList[0].split("/").length - 1;
// let last_common_path_step = linkList[0].split("/")[common_path_steps - 1];
// let designedAlignments_path = linkList[0].split(`${last_common_path_step}`)[0] + last_common_path_step;


//--------------------------------------------------------------Create Side Bar and Graphs once
// Stations for Range Input
$.getJSON('existing_alignment_data.json', data =>{
    let stations = [...new Set(data.map(doc => doc.Label))];
    toStations(stations);
});

//Take the names of the files and create path list
function createListOfPaths(){
    designedAlignments = linkList.map(link => link.split(`?fileName=`)[1].split(`&codeName=`)[0]);

        let files = designedAlignments.map(link => link.replace(".json", "").split("_"));
        files = [...files.filter(item => item.length === 3)];
        files.sort((a, b) => a[0] > b[0]);

        //Structure list in the rignht format for "toPathList" funtion
        let pathList = [];
        for (let i = 0; i < files.length; i++) {
            let version = `${files[i][1]}_ver_${files[i][2].replace("v", "")}`;
            if (i === 0 || files[i][0] !== files[i - 1][0]) {
                let newPath = {
                    _id: files[i][0],
                    versions: []
                }
                pathList.push(newPath);
            }
            pathList[pathList.length - 1].versions.push(version);
        }

        toPathList(pathList, listContainer);
}

//--------------------------------------------------------------Create Side Bar from JSON-data

//Create list of all paths and path-options
function addParentVersion(source){
    let dataFormat = [];

    for(let i=0; i<source.length; i++){
        let pathIndex = source[i].versions.findIndex(value=>value.split("_ver_")[0]==="new");
        dataFormat.push(source[i]);
        dataFormat[i].path_version = pathIndex > -1 ? source[i].versions[pathIndex].split("_ver_")[1] : i;
    }

    return dataFormat;
}

function toPathList(source, tag){
    let dataFormat = addParentVersion(source);
    dataFormat.sort((a,b)=> a._id > b._id);
    updateNestedList(dataFormat, tag);

    toGraphs(document.querySelector("#designed-paths input:checked"));
}


//Append existing alinment's station names into the tange input
function rootDiv(index,station){
    let div = createTag("div",["data-id"],[index],undefined,undefined,station);
    document.getElementById("labels").appendChild(div);
}


let roots = [];
function toStations(source){
    roots = source;
    for(let i=0; i<roots.length; i++){
        if(i === 0){
            let first_station = roots[i].split("-")[1];
            rootDiv(i+1,first_station);
        }
        let station = roots[i].split("-")[0];
        rootDiv(i,station);
    }
}




//------------------------------------------------------------------------Take JSON data for graphs

// Update Selected Stations of the Existing Alignment
let sliders = document.querySelectorAll("#rail-slider input");
// Find the stations that are selected
function findExistingStations(){
    let start = parseInt(sliders[0].value);
    let end = parseInt(sliders[1].value);
    if (parseInt(sliders[0].value) > parseInt(sliders[1].value)) {
        start = parseInt(sliders[1].value);
        end = parseInt(sliders[0].value);
    }
    let selectedRoots = roots.slice(start, end);
    return selectedRoots;
}

function removeExisting(){
    for(type in allGraphs){
        let existingIndex = allGraphs[type].findIndex(obj=>obj.name==="Existing_ver_0");
        if(existingIndex>-1){
            allGraphs[type].splice(existingIndex, 1);
        }
        
    }
}

async function keepSelectedStations(){
    removeExisting();
    let roots = findExistingStations();

    let selected = [];
    await $.getJSON('existing_alignment_data.json', data =>{
        selected = data.filter(obj => {
            let index = roots.findIndex(item => obj.Label === item);
            return (index > -1) ? true : false;
        });
    });

    return selected;
}

// Retrieve json data for simple line graph
async function retrievejsonGraphData(documentPath, yaxisKey){
    let newjson = [];
    await $.getJSON(documentPath, data => {
        newjson = data.map(obj => {
            return {
                [`${yaxisKey}`]: obj[yaxisKey],
                parent_ID: obj["parent_ID"],
                version: obj["version"],
                Index: obj["Index"]
            }
        });
        newjson.sort((a,b) => a.Index > b.Index);
    });

    return newjson;
}

// Create Line Graph with lines for both Existing Alignment and a selected Designed Alignment
async function handleAllData(documentPath, existingLink, dataConst, selectedThresholds, graphArray){
    singlelineGraphs[graphArray] = [];

    if(existingLink){
        let existingSource = await keepSelectedStations();
        singlelineGraphs[graphArray] = [...toLineGraphData(singlelineGraphs[graphArray], existingSource, "Index", graphArray, dataConst)];
    }

    let designedSource = await retrievejsonGraphData(documentPath, graphArray);
    singlelineGraphs[graphArray] = [...toLineGraphData(singlelineGraphs[graphArray], designedSource, "Index", graphArray, dataConst)];

    updateGraph(`#${graphArray}`, singlelineGraphs[graphArray], selectedThresholds, thisColours, "name", "colour");
}



// Retrieve Data for Speed, Gradient, Land Use Proximity and Earth Work 
function retrieveCustomjsonData(documentPath){
    $.getJSON(documentPath, data => {
        let newjson = data.map(obj => {
            return {
                // Land Use Proximity
                cost: obj["land_uses_proximity"],
                // Gradient
                gradient_smooth: obj["gradient_smooth"],
                boolean: obj["gradient_labels"],
                label: obj["gradient_ratio"],
                // Speed
                speed: {
                    permissible_speed: obj["permissible_speed"],
                    actual_speed: obj["actual_speed"],
                },
                // Cut N Fill
                ground_elevation: obj["ground_elevation"],
                alignment_elevation: obj["alignment_elevation"],
                elevation_type:  elevation_type(obj["elevation_difference"]),
                // General
                parent_ID: obj["parent_ID"],
                version: obj["version"],
                Index: obj["Index"]
            }
        });

        function elevation_type(diff){
            if(diff > -40 && diff < 60){
                return (diff > 0 ? "cut" : "fill");
            }else{
                return (diff > 0 ? "tunnel" : "bridge");
            }
        }
        newjson.sort((a,b) => a.Index > b.Index);
        
        toMultipleLineGraph(newjson,"speed", `#speed`, "speed", "line");
        toGradientGraph(newjson);
        toMultipleLineGraph(newjson,"cost", `#cost`, "cost");
        toCutNFill(newjson);
    });

}


//-------------------------------------------------------------- Create Graphs from JSON-data

function updateGraph(parentTag, data, selectedThresholds, colourList = colours, name_type, colour_type, type, graphFunction = linearGraph) {
    document.querySelector(parentTag).innerHTML = "";
    document.querySelector(`${parentTag}-legend`).innerHTML = "";
    graphFunction(parentTag, data, selectedThresholds, colourList, name_type, colour_type, type);
}

// Prepare line/area Graph with multiple lines
function toMultipleLineGraph(source, yContainer, tagSelector, listName, graphType = "area"){
    let structeredList = [];
    let axesAtt = {x:"Index"};
    axesAtt.y = "";
    let dataConst = {x:3*0.000621371192,y:1};
    
    if(source !== undefined){
        let newData = source;

        for(key in newData[0][yContainer]){
            let sum = newData.reduce((acc, cur) => {	
                return acc + cur[yContainer][key];
                }, 0);
            if(sum > 0 && key !== "DTM"){
                axesAtt.y = key;
                structeredList = toLineGraphData(structeredList, newData, axesAtt.x, axesAtt.y, dataConst, key, yContainer); 
            }
        }

        singlelineGraphs[listName] = [];
        singlelineGraphs[listName] = [...structeredList];
    } 
    updateGraph(tagSelector, singlelineGraphs[listName], undefined, thisColours, "cost-name", "cost-colour", graphType);
}

// Prepare Cut and Fill
function toCutNFill(source = cutNfillJson){
    singlelineGraphs.cut_n_fill = [];
    singlelineGraphs.cut_n_fill = toCutNFillData(source, {x:3*0.000621371192,y:1});

    updateGraph("#cut_n_fill", singlelineGraphs.cut_n_fill, undefined, thisColours, "cost-name", "cost-colour", "line", mixGraph);
}

//Prepare Gradient Graph
function toGradientGraph(source){
    singlelineGraphs.gradient = [];
    singlelineGraphs.gradient = [...toGradientGraphData(source, {x:3*0.000621371192,y:1})];

    updateGraph("#gradient", singlelineGraphs.gradient, undefined, thisColours, "cost-name", "cost-colour", "line", gradientGraph);
}




//---------------------------------------------------------------------------------------------------------Handle user Inputs
//Select for single-path graphs
function toGraphs(selectedPath) {
    //Check Existing Alignment
    let existingLinks = false;
    if (document.getElementById("e-a").checked) {
        existingLinks = true;
    }

    let documentPath = designedAlignments_path(selectedPath);

    // Speed, Gradient, Land Use Proximity, Earth Work
    retrieveCustomjsonData(documentPath);
    // Curvature
    handleAllData(documentPath, existingLinks, { x: 3 * 0.000621371192, y: 1 }, st,"curvature");
}



//-----------------------------------------------------------Events
d3.select(window).on('resize', resize); 

function resize() {
    toMultipleLineGraph(undefined, "cost", "#cost", "cost");
    toMultipleLineGraph(undefined, "speed", `#speed`, "speed", "line");
    updateGraph("#cut_n_fill", singlelineGraphs.cut_n_fill, undefined, thisColours, "cost-name", "cost-colour", "line", mixGraph);   
    updateGraph(`#curvature`, singlelineGraphs.curvature, st, thisColours, "name", "colour");
    updateGraph("#gradient", singlelineGraphs.gradient, undefined, thisColours, "cost-name", "cost-colour", "line", gradientGraph);
}


//Toggle Graph button
let alignments = document.getElementsByClassName("menu")[0];
alignments.addEventListener("click",e=>{
    if(e.target.classList.contains("menu-label")){
        e.target.classList.toggle("selected");
    }
    if(e.target.localName === "input" && (e.target.type === "radio" || e.target.type === "checkbox")){
        toGraphs(document.querySelector("#designed-paths input:checked"));
    }
});
