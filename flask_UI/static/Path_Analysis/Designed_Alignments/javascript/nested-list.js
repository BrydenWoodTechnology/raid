//HTML Elements
let listContainer = document.getElementById("list-container");
let menu = document.getElementsByClassName("menu")[0];


//Events
listContainer.addEventListener("click", e => { e.target.localName === "button" ? handleSublist(e.target) : "" });


//Functions
//Handle open - close
function handleSublist(targetBtn) {
    let buttons = targetBtn.parentElement.querySelectorAll("button");
    for (button of buttons) {
        button.classList.toggle("hidden");""
    }
    targetBtn.parentElement.parentElement.querySelector(".sub-container").classList.toggle("hidden");
}


//Create nested list
function createCheckRow(id, version, name=version, checked=false, input=true, radio_name="path"){
    let row = createTag("div", undefined, undefined, ["check-row"]);
    if(input){
        let _id = (version === "new" ? id.split("_ver_")[0] : id);
        let _version = (version === "new" ? id.split("_ver_")[1] : version.split("_ver_")[1]);
        let _type = version.split("_ver_")[0];

        let input = createTag("input", ["type", "name", "data-id", "data-version", "data-type", "data-name"], ["radio", radio_name, _id, _version, _type ,name], undefined, name);
        input.checked = checked;
        row.appendChild(input);
    }

    let label = createTag("LABEL", ["for"], [textToHTMLattribute(name)], undefined, undefined, name);
    row.appendChild(label);
    return row;
}


function createNestedButton(){
    let container = createTag("div", undefined, undefined, ["open-close"]);
    let plus = createTag("button", undefined, undefined, ["hidden"], "plus", ">");
    let minus = createTag("button", undefined, undefined, undefined, "minus", ">");
    container.appendChild(plus);
    container.appendChild(minus);
    return container;
}


function createNestedList(items, tag) {
    for (let i = 0; i < items.length; i++) {
        let categoryContainer = createTag("div", undefined, undefined, ["category-container"]);
        let mainRow = createCheckRow(items[i]._id, "original", `Path_${items[i].path_version}`, false, false);
        mainRow.classList.add("header");
        categoryContainer.appendChild(mainRow);
        if (items[i].versions.length > 0) {
            //Sort versions | First the new and the rest according to their number
            items[i].versions.sort((a,b)=>
                a.split("_ver_")[0]==="new" ? -1 :
                b.split("_ver_")[0]==="new" ? 1 : parseInt(a.split("_ver_")[1]) > parseInt(b.split("_ver_")[1])
            );

            let open_close = createNestedButton();
            
            categoryContainer.appendChild(open_close);
            let subContainer = createTag("div", undefined, undefined, ["sub-container"]);
            for (let j = 0; j < items[i].versions.length; j++) {
                if(items[i].versions[j] !== "original"){
                    let name = `option_${items[i].versions[j].split("_ver_")[0]==="refined" ? items[i].versions[j].split("_ver_")[1] : 0}`;

                    let checked = false;
                    if( i === items.length-1 && j === items[i].versions.length-1 ){
                        checked = true;
                    }

                    let row = createCheckRow(items[i]._id, items[i].versions[j], name, checked);
                    subContainer.appendChild(row);
                }
            }
            categoryContainer.appendChild(subContainer);
        }
        tag.appendChild(categoryContainer);

        //Scroll to the bottom
        let scrollParent = tag.parentElement.parentElement.parentElement;
        scrollParent.scrollTop = scrollParent.scrollHeight;
    }
}


function updateNestedList(items, tag){
    clearList(tag);
    createNestedList(items, tag);
}