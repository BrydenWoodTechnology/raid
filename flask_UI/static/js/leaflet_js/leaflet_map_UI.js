    function post_request_payload(functionName, data_payload) {

        // add task status elements

        var post_response;

        $.ajax({

            type: 'POST',
            data: data_payload,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: '/nontasklistener?functionName=' + functionName,
            success: function(data, status, request) {


             
                moveCenterNew(data['data']);


            },

            error: function() {

                alert('function didnt go through');
            }
        });



        return post_response;


    }





    var bbox_from_shape;
    function getBBOX_coords(bounds){

        let minx,miny,maxx,maxy;
        minx = bounds['_southWest']["lng"]
        miny = bounds['_southWest']["lat"]
        maxx = bounds['_northEast']["lng"]
        maxy = bounds['_northEast']["lat"]

        let bbox = [minx,miny,maxx,maxy]
        let payload = {"bbox":bbox}
        bbox_from_shape = JSON.stringify(payload); 
        console.log(bbox_from_shape);

        // post_request_payload("create_bbox",payload_encoded) 


    }



    function moveCenterNew(data) {

        let lat = data['lat'];
        let lng = data['lng'];

        let UG_coords = { "lat": lat, "lng": lng };
        marker.setLatLng(UG_coords);
        map.setView({ UG_coords });

    }

    function moveCenterTo() {


        let centerPos = map.getCenter();

        // console.log(centerPos)

        var payload = { "lat": centerPos['lat'], "lng": centerPos['lng'] }
        var payload_encoded = JSON.stringify(payload);



        post_request_payload("get_map_center", payload_encoded);

    }


    function addNewLayer(root_path,name, tile_layer_bounds) {
        var lyr = L.tileLayer(root_path +"/"+ name + '/{z}/{x}/{y}.png', { tms: true, opacity: 0.7, attribution: "", maxNativeZoom: 16, maxZoom: 22, minZoom: 12, bounds: tile_layer_bounds }).addTo(map);
        layerControl.addOverlay(lyr, name);
    }

    function addRasterLayers(root_path,raster_layers,tile_layer_bounds) {

        for (let i = 0; i < raster_layers.length; i++) {

            addNewLayer(root_path,raster_layers[i], tile_layer_bounds);

        }


    }


    function startClean() {

        $(".leaflet-control-layers-selector").each(function() { if (this.type == "checkbox") { this.checked = false } })


        $(".leaflet-pane leaflet-tile-pane").html("");


        


    }



    // function makeRandomColor(){
    //     var c = '';
    //     while (c.length < 7) {
    //     c += (Math.random()).toString(16).substr(-6).substr(-1)
    //     }
    
    //     return '#'+c;
    
    // }

    var existing_alignment;
    var index_color = 0;
    function addAlignment(data,name) {
        existing_alignment = L.layerGroup().addTo(map);



        var color;
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        color= "rgb("+r+" ,"+g+","+ b+")"; 

        var colors =["#d16ba5", "#c777b9", "#ba83ca", "#aa8fd8",
         "#9a9ae1", "#8aa7ec", "#79b3f4", "#69bff8", "#52cffe",
          "#41dfff", "#46eefa", "#5ffbf1"];


        // let colors = ["#074650", "#05353cd4","#006e6e", "#4ffd4f","#33a8a8", "#3b546d","#5289bf", "#8ac5ff", "#c4e1fe", "#6d33a7", "#b66dff", "#652c49","#82395e", "#ff2d97","#bf5289", "#fe6db6", "#bf88a4", "#feb5da", "#6d3600", "#ad5600", "#fb9a3a", "#fbb167", "#f9c38d"];
        // var colors= ['#d1936b', "#d79f6e", "#ddab72", "#e1b876", "#e4c57c", "#e8cd7c", "#ebd57c", "#eddd7d",
        //      "#f1e376", "#f5e86f", "#f8ee67", "#fbf45f"];

        index_color+=1;
            
        if(index_color==colors.length){

            index_color=0;
        }
        var myStyle = {
            "color": colors[index_color],
            "weight": 5,
            "opacity": 0.65
        };

        var a = new L.GeoJSON(data.features, {onEachFeature:addMyData, style: myStyle }).addTo(map);
        
        layerControl.addOverlay(existing_alignment,name);

        // layerControl.addOverlay(a, name);

    }


    function addMyData( feature, layer ){

        console.log("its_ok");
        existing_alignment.addLayer( layer )
        // some other code can go here, like adding a popup with layer.bindPopup("Hello")
      }



    function addGeoJson(url, type,name) {

        $.ajax({
            dataType: "json",
            url: url,
            success: function(data) {


                if (type == "line") {



                    
                    addAlignment(data,name);
                } else {

                    let geojsonMarkerOptions = {
                        radius: 8,
                        fillColor: "#ff7800",
                        color: "#000",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 0.8
                    };

                    new L.GeoJSON(data.features, {

                        pointToLayer: function(feature, latlng) {
                            return L.circleMarker(latlng, geojsonMarkerOptions);
                        },

                        onEachFeature: onEachFeature
                    }).addTo(map);

                }



                // $(data.features).each(function(key, data) {
                //     district_boundary.addData(data);
                // });
            }
        }).error(function() { console.log("error") });

    }



    /* **** Leaflet **** */

    // Base layers

    //  .. OpenStreetMap
    var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    // .. OSM Toner
    // var toner = L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png', { attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.' });

    // .. White background
    var white = L.tileLayer("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAAA1BMVEX///+nxBvIAAAAH0lEQVQYGe3BAQ0AAADCIPunfg43YAAAAAAAAAAA5wIhAAAB9aK9BAAAAABJRU5ErkJggg==");
    var black = L.tileLayer("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAA1BMVEUAAACnej3aAAAAPUlEQVR4nO3BAQ0AAADCoPdPbQ8HFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8GadCAABYe850QAAAABJRU5ErkJggg==");


 





    // // Overlay layers (TMS)
    var lyr = L.tileLayer('tile_images/{z}/{x}/{y}.png', { tms: true, opacity: 0.7, attribution: "", maxNativeZoom: 16, maxZoom: 22 });


    // // Map
    var map = L.map('map', {
        center: [53.637717616072834, -1.842613220214844],
        zoom: 12,
        minZoom: 1,
        maxZoom: 22
    });



    // -------------------------------------
    // overlay image png
    // -------------------------------------
    var version_ind = 0;
    function add_overlayImage(url,bbox){
        let overlay_img;

        // if (overlay_img){
        //     layerControl.removeLayer(overlay_img);
        // }

    var imageUrl = url;
    var imageBounds = [[bbox[0][1],bbox[0][0]],[bbox[1][1],bbox[1][0]]];

    // console.log(imageBounds);

        // L.imageOverlay(imageUrl, imageBounds).addTo(map);
        


        overlay_img = new L.ImageOverlay(imageUrl, imageBounds, {
            opacity: 0.5,
            interactive: true,
            attribution: '&copy; A.B.B Corp.'
        });

        map.addLayer(overlay_img);
        
        layerControl.addOverlay(overlay_img, "fitness_layer_v"+version_ind);
        version_ind+=1;
    }

    

    // -------------------------------------
    // -------------------------------------


    // -------------------------------------
    // overlay GeoTiff
    // -------------------------------------

    var windSpeed;

    function addGeoTiff(url,minmax) {

        console.log("here ");
        console.log(minmax);
        if (windSpeed){
            layerControl.removeLayer(windSpeed);
        }

        windSpeed = L.leafletGeotiff(
            url = url,
            options = {
                band: 0,
                displayMin: minmax[0],
                displayMax: minmax[1],
                name: 'Wind speed',
                colorScale: 'rainbow',
                clampLow: false,
                clampHigh: true,
                // arrowSize: 20,
                opacity: 0.1,
            }
        ).addTo(map);


    layerControl.addOverlay(windSpeed, "fitness_layer");

    }

    // -------------------------------------
    // -------------------------------------





    drawnItems = L.featureGroup().addTo(map);


    var layerControl = L.control.layers().addTo(map);
    layerControl.addBaseLayer(osm, 'OSM');
    layerControl.addBaseLayer(white, 'white background');
    layerControl.addBaseLayer(black, 'dark background');




    layerControl.addOverlay(lyr, "custom");

    layerControl.addOverlay(drawnItems, "drawn");
    layerControl.options = { position: 'topright', collapsed: false };


    var control_draw = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
            poly: {
                allowIntersection: false
            }
        },
        draw: {
            polygon: {
                allowIntersection: false,
                showArea: true
            }
        },
        // position:"topright"
    });

    // control_draw.position ="topright"
    map.addControl(control_draw);


 
    var tile_layer_bounds;


    map.on('zoomend', function() {
        moveCenterTo();
        $("#overlayGrid").prop("class", "zoom_" + map._zoom);
    });

    map.on('dragend', function() {
        moveCenterTo();

    });


// -------------------------------------------------------------------
// example from : https://leaflet.github.io/Leaflet.draw/docs/examples/popup.html
// -------------------------------------------------------------------


    // Truncate value based on number of decimals
    var _round = function(num, len) {
        return Math.round(num * (Math.pow(10, len))) / (Math.pow(10, len));
    };
    // Helper method to format LatLng object (x.xxxxxx, y.yyyyyy)
    var strLatLng = function(latlng) {
        return "(" + _round(latlng.lat, 6) + ", " + _round(latlng.lng, 6) + ")";
    };

    // Generate popup content based on layer type
    // - Returns HTML string, or null if unknown object
    var getPopupContent = function(layer) {
        // Marker - add lat/long
        if (layer instanceof L.Marker || layer instanceof L.CircleMarker) {
            return strLatLng(layer.getLatLng());
            // Circle - lat/long, radius
        } else if (layer instanceof L.Circle) {
            var center = layer.getLatLng(),
                radius = layer.getRadius();
            return "Center: " + strLatLng(center) + "<br />" +
                "Radius: " + _round(radius, 2) + " m";
            // Rectangle/Polygon - area
        } else if (layer instanceof L.Polygon) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
                area = L.GeometryUtil.geodesicArea(latlngs);


                getBBOX_coords(layer._bounds);
            return "Area: " + L.GeometryUtil.readableArea(area, true);
            // Polyline - distance
        } else if (layer instanceof L.Polyline) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
                distance = 0;
            if (latlngs.length < 2) {
                return "Distance: N/A";
            } else {
                for (var i = 0; i < latlngs.length - 1; i++) {
                    distance += latlngs[i].distanceTo(latlngs[i + 1]);
                }
                return "Distance: " + _round(distance, 2) + " m";
            }
        }
        return null;
    };

    // Object created - bind popup to layer, add to feature group
    map.on(L.Draw.Event.CREATED, function(event) {
        var layer = event.layer;
        var content = getPopupContent(layer);
        if (content !== null) {
            layer.bindPopup(content);
        }
        drawnItems.addLayer(layer);
    });

    // Object(s) edited - update popups
    map.on(L.Draw.Event.EDITED, function(event) {
        var layers = event.layers,
            content = null;
        layers.eachLayer(function(layer) {
            content = getPopupContent(layer);
            if (content !== null) {
                layer.setPopupContent(content);
            }
        });
    });


    map.on('click', function(e) {
        if (!marker) {
            // marker = L.marker([e.latlng.lat,e.latlng.lng]).addTo(map);
        } else {
            // marker.setLatLng([e.latlng.lat,e.latlng.lng]);
        }
        // read tif data on cursor 
        // console.log(windSpeed.getValueAtLatLng(e.latlng.lat, e.latlng.lng));
    });


function update_tiles(root_path){


    let url_tile = root_path+'/tile_bounds.json'
    $.getJSON(url_tile,

        function(json_file) {



            let cornerA = L.latLng(json_file.bounds.maxy, json_file.bounds.minx);
            let cornerB = L.latLng(json_file.bounds.miny, json_file.bounds.maxx);
            tile_layer_bounds = L.latLngBounds(cornerA, cornerB);

            let raster_layers = json_file.raster_layers;
            addRasterLayers(root_path,raster_layers,tile_layer_bounds);
            console.log("clearing-files");

            startClean();
        }


    )


}

    var markerSwitcher = false;


    // https://github.com/CliffCloud/Leaflet.EasyButton

    var greenIcon = new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    var redIcon = new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });




    var markerStart, markerEnd;



    function assign_population_on(result_population){


        markerStart.bindPopup("Population in 1km: " + result_population["markerStart"][0] +"<br>"+
        "Population in 2km: "+ result_population["markerStart"][1]).openPopup();

        markerEnd.bindPopup("Population in 1km: " + result_population["markerEnd"][0] +"<br>"+
        "Population in 2km: "+ result_population["markerEnd"][1]).openPopup();


    }


    

    function retrieve_start_end(){

        
        var origin = markerStart._latlng;
        var destination = markerEnd._latlng;
        
        let output = {
            "origin":[origin['lng'], origin['lat']],
            "destination":[destination['lng'],destination['lat']],
            "weights":""
          }

        return output
    }


    function retrieve_marker_pos(){

        
        var marker_pos= marker._latlng;
        let output = {
            "marker":{"lng":marker_pos['lng'], "lat":marker_pos['lat']}
          }

        return output
    }


    var clickedTime = 0;


    // var easyButton_marker = new L.easyButton({position:"topright"})
    L.easyButton('fa-globe',function(btn, map) {
        if (clickedTime < 1) {

            let center = map.getCenter();


            markerStart = L.marker([center['lat'], center['lng']], { icon: greenIcon, draggable: true }).addTo(map);
            markerEnd = L.marker([center['lat'], center['lng']], { icon: redIcon, draggable: true }).addTo(map);



            clickedTime += 1;
        } else {

            let center = map.getCenter();

            markerStart.setLatLng({ "lat": center["lat"], "lng": center["lng"] });
            markerEnd.setLatLng({ "lat": center["lat"], "lng": center["lng"] });

        }


    }).addTo(map);

    var district_boundary = new L.geoJson();
    district_boundary.addTo(map);

    function onEachFeature(feature, layer) {

        if (feature.properties) {
            layer.bindPopup(feature.properties.NAME);
        }
    }

   

    let url1 = "files/snapped_stations.json";

    addGeoJson(url1, "marker","marker");

    addAlignment(alignment_json,"existing_alignment");

    var marker=L.marker([0,0],{draggable:true}).addTo(map);


    