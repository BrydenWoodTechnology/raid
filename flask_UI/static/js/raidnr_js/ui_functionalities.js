
//Open always in "Map" tab
 let inputs = document.querySelectorAll(".tabs input[type=radio]:checked");
 for(input of inputs){
   input.checked = false;
 }
 let map = document.querySelector(".tabs #tab-1");
 map.checked = true;

 let smoothenPreSelect = document.querySelector("#smoothening .input-group input[name=smoothening-type]:checked");
 smoothenPreSelect ? smoothenPreSelect.checked = false : undefined;

 let curvature = document.querySelector("#smoothening .input-group input#curvature");
curvature.checked = true;

//Change displayed name of Current Paths
let string = `{ 
  "newPath": "9e50d7dd-f0e7-46f1-a61c-084bd9478f3d", 
  "version": 1
}`; //_ver_1
updateDisplayedPathName(string);
function updateDisplayedPathName(string){
  let json = JSON.parse(string);
  let newPath = document.querySelector("#path-names #new-path");
  let newPathName = json.newPath.split("_ver_")[1];
  newPath.innerHTML = (newPathName ? `Path_${newPathName}` : `Path_${json.version}`);
  newPath.dataset.ID = json.newPath;
  let version = document.querySelector("#path-names #smoothen-version");
  version.innerHTML = (newPathName ? `V_${json.version}` : `V_0`);
  version.dataset.version = `${json.version}`;
}




$("#submitW3W").click(get_w3w);
function addImage(fileName){

   srcUrl="images/Sessions/"+fileName+".png";
   var result_div = $("#here_image");

   result_div.attr("src", srcUrl);
 }

var data = [4, 2, 2, 6, 13, 7, 30, 30, 5, 15, 5, 30]




let heightRatio = 1;
let graphDataStructure = [
 {name: "category0", value: 4},
 {name: "category1",value: 2},
 {name: "category2",value: 2},
 {name: "category3",value: 6},
 {name: "category4",value: 13},
 {name: "category5",value: 7},
 {name: "category6",value: 30},
 {name: "category7",value: 30},
 {name: "category8",value: 5},
 {name: "category9",value: 15},
 {name: "category10",value: 5},
 {name: "category11",value: 30}
];

var graphData = [{name: "category0", value: 4},
{name: "category1",value: 2}];


function handleGraphData(data){ 
////for python +++ change array name in handle_css 154
  graphData=[];
  // graphData = JSON.parse(data);
  graphData = data;

  updateColumnGraph(data);
}


function updateColumnGraph(data) {

  document.querySelector("#left_bar #graph").innerHTML = "";
  document.querySelector("#left_bar #labels").innerHTML = "";

  let colors = ["#4e79a7", "#a0cbe8", "#f28e2b", "#ffbe7d", "#59a14f", "#8cd17d", "#499894", "#86bcb6", "#d37295", "#fabfd2","#b6992d", "#f1ce63"];

  //For analoge to screen size
  let rect = d3.select("#left_bar svg").node().getBoundingClientRect();
  let width = "15px";//rect.width;
  heightRatio = rect.height/100;

  let valuesArray = data.map((a)=>a.value);
  const arrSum = valuesArray.reduce((a, b) => a + b);

  let initpos_y = 0;
  for (var i = 0; i < data.length; i++) {

      let h = Math.round(data[i].value * 100 / arrSum);

      let colorBox = colors[i];

      d3.select("#left_bar svg g#graph").append("rect")
          .attr("fill",colorBox)
          .attr("x", 0)
          .attr("y", initpos_y)
          .attr("width", width)
          .attr("height", h * heightRatio)
          .attr("target", "_top")
          .attr("xlink:href", function(e){return "www.google.com"})
          .attr("class",data[i].name)
          .on("click",function(event){addImage()})
          .on("mouseenter",function() {$(this).css("opacity", "1"); console.log("here")})
          .on("mouseout",function(){$(this).css("opacity", "0.8")});
      


      d3.select("#left_bar svg g#labels").append("text")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width","50px")
        .style("transform",`translate(5px,${initpos_y}px) rotate(0deg)`)
        .attr("data-height", initpos_y)
        .text(data[i].name);

        initpos_y += h * heightRatio;

  }
}


//For responsive height
window.onresize = function() {
  let preRatio = heightRatio;
  let rect = d3.select("#left_bar svg").node().getBoundingClientRect();
  heightRatio = rect.height/100;

  let box = d3.selectAll("#left_bar svg rect");
  let text = d3.selectAll("#left_bar svg text")
  if(box.length>0){
    box
    .attr("height", (d,i)=>{
      let preHeight = box[0][i].height.animVal.value;
      let newHeight = (preHeight/preRatio)*heightRatio;
      // text[0][i].style("transform",`translate(5px,${newHeight}px) rotate(0deg)`);
      return newHeight;
    })
    .attr("y", (d,i)=>{
      let preHeight = box[0][i].y.animVal.value;
      // console.log((preHeight/preRatio)*heightRatio, preHeight);
      return (preHeight/preRatio)*heightRatio;
    });

    text
      .style("transform", (d,i)=>{
        let preHeight = parseFloat(text[0][i].dataset.height); 
        let newHeight = (preHeight/preRatio)*heightRatio;
        text[0][i].dataset.height = newHeight;
        return `translate(5px,${newHeight}px) rotate(0deg)`;
      })
  }
};


//Convert text into id - remove unwanted characters
function textToHTMLattribute(value, lowerCase = false) {
  let filter = new RegExp('[^a-zA-Z0-9/_-]', 'g'); //Match all characters except letters, numbers, "-", "_"
  let unwanted = value.match(filter);
  let id = value;
  if (unwanted !== null) {
      for (char of unwanted) {
          id = id.replace(char, "");
      }
  }
  if(lowerCase) id = id.toLowerCase();
  return id;
}

function createNestedButton(){
  let button = 
  `<div class="open-close">
      <button id="plus" class="">></button>
      <button id="minus" class="hidden">></button>
  </div>`;
  return button;
}

function makeCategoryContainer(header, button){
  let id = textToHTMLattribute(header);
  let container = 
  `<div id="${id}" class="category">
      <div class="header">
          ${createNestedButton()}
          <button>${header}</button>
      </div>
      <div class="sub-container ${id} hidden">
          ${button}
      </div>
  </div>`;

  $('.mapControllers').append(container);
}

function makethebuttons() {
  $.getJSON('files/buttons_info.json',
    function (functionalities) {
      let index = 0;

      for (let i = 0; i < functionalities.data.length; i++) {
        let l = functionalities.data[i];
        let id = (!l['class'].includes("popup-button") ? "start-bg-job" : l["pointTo"]);
        let data_id = undefined;
        if(!l['class'].includes("popup-button")){
          index++;
          data_id = index;
        }
        var buttonDiv = `<button ${(data_id ? `data-id="${data_id}"` : ``)} data-point_to="${l['pointTo']}" id="${id}" class="${l['class']}" onclick="${l['function_name'] }">${l['class'].includes("popup-button") ? "" : '<div class="bullet hidden">&#8226;</div>'}${l['title']}</button> <div id="progress${l['function_name']}"></div>`; //<br><br>

        let containerId = textToHTMLattribute(l["category"]);
        let container = document.getElementById(containerId);
        container === null ? makeCategoryContainer(l["category"], buttonDiv) : $(`.mapControllers #${containerId}.category .sub-container`).append(buttonDiv);
      }
    }

  )
}




// function makethebuttons() {
//   $.getJSON('files/buttons_info.json',

//     function (functionalities) {
//       let index = 0;

//       for (let i = 0; i < functionalities.data.length; i++) {

//         let l = functionalities.data[i];

//         //Disable the button if it is not "popup-button"
//         // let disabled = (!l['pointTo'].includes("popup-button") ? "disabled" : "");
//         let id = (!l['class'].includes("popup-button") ? "start-bg-job" : l["category"]);

//         let data_id = undefined;
//         if(!l['class'].includes("popup-button")){
//           index++;
//           data_id = index;
//         }
        

//         var buttonDiv = `<button ${(data_id ? `data-id="${data_id}"` : ``)} data-point_to="${l['pointTo']}" id="${id}" class="${l['class']}" onclick="${l['function_name'] }">${l['title']}</button> <div id="progress${l['function_name']}"></div>`; //<br><br>

//         // console.log(buttonDiv);

//         $('.mapControllers').append(buttonDiv);
//       }
//     }

//   )
// }



function showVal(e){

//  console.log(e.value);


 let id = e.id;
 let val = e.value;

$(".inputNumber#"+id)[0].value=val;
$(".inputRange#"+id)[0].value=val;
}


function makeCursors (){
  $.getJSON('files/grid_weights.json',
 
         function(functionalities) {            
 
               let i=0; 
               // console.log(Object.keys(functionalities.grid_weights).length);
               for (item in functionalities.grid_weights){
 
                   let weight_category = item;
                   // console.log(weight_category);
                   let context = functionalities.grid_weights[item];
                   let position = (i < Object.keys(functionalities.grid_weights).length/2 ? "bottom" : "top");
                   i++;
 
                   let disabled = ((weight_category==="DTM" || weight_category==="DSM") ? "disabled" : "");
                   let value = 0;
                   switch(weight_category){
                     case "Building": value = 5; break;
                     case "SurfaceWater_Area": value = 5; break;
                     case "Road": value = 1; break;
                   }
 
                  let cursorDiv= '<div class="weight_ input-wrapper" id="'+weight_category+'" style="display: inline;">'+
                                         '<input class="inputRange" id='+weight_category+' type="range" min="0" max="100"  style="display: inline;"  oninput="showVal(this)" onchange="showVal(this)" value="'+ value +'"'+ disabled +'/>'+
                                         '<input class="inputNumber" id='+weight_category+' min="0" max="100" type="number"   oninput="showVal(this)" onchange="showVal(this)" value="'+ value +'"'+ disabled +'/>'+
                                         ' <label id="num2_'+weight_category+'" type="number" style="position:relative;" class="name-contaner">'+ 
                                               weight_category + '</label>'+
                                         (context !== "" ? `<span class="context-container ${position}"><span class="context">${context}</span></span>` : "")
                                         '</div>';
 
                   $('.weight_container').append(cursorDiv);
                   }
             }
     )
 }


topo_layers = ['Building',
'CarChargingPoint',
'DTM',
'ElectricityTransmissionLine',
'Foreshore',
'FunctionalSite',
'Glasshouse',
'Greenspaces',
'ImportantBuilding',
'MotorwayJunction',
'NationalParks',
'RailwayStation',
'RailwayTrack',
'RailwayTunnel',
'Road',
'RoadTunnel',
'Roundabout',
'Slope',
'SurfaceWater_Area',
'SurfaceWater_Line',
'TidalBoundary',
'TidalWater',
'Woodland']



function retrieveWeights() {
  var arrayVals = []
  for (var i = 0; i < topo_layers.length; i++) {
    let val = document.querySelector(`.inputRange#${topo_layers[i]}`).value;
    arrayVals.push(val);
  }
  // console.log(arrayVals);
  return arrayVals;
}


function get_w3w_r() {
 let val = $("#W3words")[0].value
 if (val == "") {
   alert('Fill the w3w input field');
 } else {
   var payload = { "w3w_list": val }
   var payload_encoded = JSON.stringify(payload);
  
   post_request_payload("get_w3w", payload_encoded);
 }
}


function get_w3w(){
get_w3w_r()
}


//----------------------Retrieve Data
//Smouthening
function retrieveSmoothening(e){
  let typeElement = document.querySelector(".popup#smoothening input:checked");
  let type = typeElement.id;
  let iterations = document.querySelector(".popup#smoothening input[type=number]#Iterations").value;
  let deviation = document.querySelector(".popup#smoothening input[type=number]#Distance-Deviation").value;
  deviation = Math.pow((deviation/3), 2);
  
  let looping = document.getElementById("multiple-options");
  let allIterations = iterations;
  let allDeviation = deviation;
  if(looping.checked){
    let iterationsMin = document.querySelector("input[type=number]#Min-Iteration").value;
    let iterationsMax = document.querySelector("input[type=number]#Max-Iteration").value;
    let deviationMin = document.querySelector("input[type=number]#Min-Distance").value;
    let deviationMax = document.querySelector("input[type=number]#Max-Distance").value;
    allIterations = [iterationsMin, iterationsMax];
    allDeviation = [deviationMin, deviationMax];
  }


  let output = {
    "type":type,
    "iterations":allIterations,
    "s":allDeviation,
    "isloop": looping.checked
  }
  console.log(output);
  post_request_payload(e.target.dataset.point_to, JSON.stringify(output));
}


function retrievePath(e){


   var output_markers = document.getElementById("leaflet_container").contentWindow.retrieve_start_end();
   

   if(output_markers!=undefined){

     output_markers['weights']=retrieveWeights()
     output_markers["existing_alignment"] =  document.querySelector("input#existing-alignment").checked;
     output_markers["circuity"]= document.querySelector("input#circuity").checked;

     post_request_payload(e.target.dataset.point_to, JSON.stringify(output_markers));



   }else{

      let output = {
        origin:[parseInt(document.getElementById("OriginLatLong").value.split(",")[0]), parseInt(document.getElementById("OriginLatLong").value.split(",")[1])],
        destination:[parseInt(document.getElementById("DestinationLatLong").value.split(",")[0]), parseInt(document.getElementById("DestinationLatLong").value.split(",")[1])],
        weights: retrieveWeights(e),
        attractive_force: parseInt(document.querySelector("input[type=number]#attractive-force").value),
        existing_alignment: document.querySelector("input#existing-alignment").checked,
        circuity: document.querySelector("input#circuity").checked
    }
     post_request_payload(e.target.dataset.point_to, JSON.stringify(output));



   }
   

}


//What 3 words
function retrieveW3W(){
 let inputs = document.querySelector("input#W3words");
 let output = inputs.value.split(",");
 return JSON.stringify(output);
}

//Latitude Longtitude
function retrieveLagLong(e){
 let inputs = document.querySelector("input#LatLong").value.split(",");
 let output = {
   "x":inputs[0],
   "y":inputs[1]
 }
 post_request_payload(e.target.dataset.point_to, JSON.stringify(output));
}

//UserData
function retrieveUserData(){
  let output = {
    "u": document.querySelector("#Username").value,
    "p": document.querySelector("#Password").value,
    "s": document.querySelector("#SessionID").value
  }
  return JSON.stringify(output);
}



//Retrieve script
function retrieveScript(e){
  let output = document.querySelector("#python-script textarea").value;
}



//Check Mongo and fire python function
function showPopup(source){
 let id = document.querySelector(".notification-popup #id");
 id.innerHTML = `ID : <strong>${source[0].ID}</strong>`;
 let version = document.querySelector(".notification-popup #version");
 version.innerHTML = `version : <strong>${source[0].version}</strong>`;
 document.querySelector(".disable-screen").classList.toggle("hidden");
}
function closePopup(e){
 e.target.parentElement.parentElement.classList.toggle("hidden");
}

function updateState(source,functionName){
//  console.log(source);
//  console.log(functionName)
 source.length === 0 ? post_request_payload(functionName,"") : showPopup(source); //to be abdated with a relative python function
}

function checkMongo(e){

 let functionName= e.target.dataset.point_to;
 let checkLink = `http://api-node.bwtcreativetech.io/19029-RAID-NR/Test_Alignment/?where={"$and":[{"ID":"${document.querySelector("#path-names #new-path").dataset.ID}"},{"version":${document.querySelector("#path-names #smoothen-version").dataset.version}}]}&fields=ID,version&hateoas=false`;
//  getRequestAsync(checkLink, updateState,functionName);
 post_request_payload(e.target.dataset.point_to,"");
}


// function checkMongo(e){
//   let checkLink = `http://api-node.bwtcreativetech.io/19029-RAID-NR/Test_Alignment/?where={"$and":[{"ID":"${document.querySelector("#path-names #new-path").dataset.ID}"},{"version":${document.querySelector("#path-names #smoothen-version").dataset.version}}]}&fields=ID,version&hateoas=false`;
//   let point_to = e.target.dataset.point_to;
//   getRequestAsync(checkLink, updateState, point_to);
// }
// //--


function retrieveBbox_ui(){

 var minx= $("#LongitudeNW")[0].value;
 var miny= $("#LatitudeSE")[0].value;
 var maxx= $("#LongitudeSE")[0].value;
 var maxy= $("#LatitudeNW")[0].value;

 let bbox_payload = {"bbox":[minx,miny,maxx,maxy]}

 return JSON.stringify(bbox_payload)
 
}


function creaeteBbox_(e){

 var bbox_;
 let bbox_from_shape =document.getElementById("leaflet_container").contentWindow.bbox_from_shape;
 if(bbox_from_shape== undefined){

   bbox_=retrieveBbox_ui();
 }else{

   bbox_=bbox_from_shape;
 }


 post_request_payload(e.target.dataset.point_to, bbox_);


}


function query_cost_on_marker_(){


  console.log("got a query");

  let marker_pos = document.getElementById("leaflet_container").contentWindow.retrieve_marker_pos();
  
  console.log("querying on "+ marker_pos['marker']['lat'])
  
  let payload = JSON.stringify(marker_pos)

 post_request_payload("query_cost_point", payload);

}



function closeSignIn(close){
  let popup = document.getElementById("credentials");
  let error_message = popup.querySelector(".error-message");
  error_message.classList.remove("hidden");
  if(close){
    popup.classList.toggle("hidden");
    error_message.classList.toggle("hidden");
  }
}

function query_population(event){
  
  var output_markers = document.getElementById("leaflet_container").contentWindow.retrieve_start_end();

  console.log(output_markers);
  post_request_payload(event.target.dataset.point_to, JSON.stringify(output_markers));


}

$("#submitCred").on("click",

 function storePass(e){


   var user_credentials = retrieveUserData();

   console.log("got credentials" + user_credentials);


   $.ajax({

     type: 'POST',
     data: user_credentials,
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     url: '/postcred',
     success: function(data, status, request) {
       
       userCode = request.getResponseHeader('codeName');
       localStorage.setItem("codeName", userCode);
       
       $("#submitCred")[0].innerHTML="Credentials Saved";
       $("#submitCred").css("background-color", "rgb(17, 99, 65)");
       closeSignIn(true);
       
       console.log(request.getResponseHeader('existing'));
       if(request.getResponseHeader('existing')=="True"){

        alert('Welcome Back! You can load your previous session from Load Existing Session tab.');

       }

   },
     error: function() {

       alert('credentials not match');
     }
 });

});


function add_download_link(url,name){


let download_link = '<div class="input-wrapper"><a href="'+ url +'" download="'+name+'">'+name+'</a></div>';


$('.export_files').append(download_link);

}