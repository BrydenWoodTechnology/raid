
//Update Buttons colour according to the current step
function updateProcessStep(button){
    let previous = document.getElementsByClassName("current-step")[0];
    if(previous){
      previous.classList.remove("current-step");
      // previous.disabled = true;
      previous.classList.toggle("disabled");
    }
    
    let preNext = document.getElementsByClassName("next-step");
    for(let i=preNext.length-1; i>=0; i--){
      preNext[i].classList.toggle("disabled");
      preNext[i].classList.remove("next-step");
    }
    button.classList.toggle("current-step");
    button.classList.remove("next-step");
    button.classList.remove("disabled");
    // button.disabled = false;

    let currentIndex = button.dataset.id;
    let next = document.querySelector(`button[data-id="${parseInt(currentIndex)+1}"]`);
    console.log(next);
    if(next !== null){
      // next.disabled = false;
      next.classList.remove("disabled");
      next.classList.toggle("next-step");
    }

    let gen_path = document.getElementsByClassName("gen_path")[0];
    if(currentIndex > gen_path.dataset.id){
      gen_path.disabled = false;
      gen_path.classList.toggle("next-step");
    }
  }

   //Anable Generate Path
   function generatePath(target){
    let current = document.getElementsByClassName("current-step")[0];
    let gen_path = document.getElementsByClassName("gen_path")[0];
    if(current && target.id !== "select-box-area" && current.dataset.id >= gen_path.dataset.id) {
      // gen_path.disabled = false;
      gen_path.classList.remove("current-step");
      gen_path.classList.toggle("next-step");
    }
  }
  

  function removeSelected(targetsClass, tabParent, parentClass) {
    let preSelected = document.querySelector(`${targetsClass}.selected`);
    if (preSelected !== null) preSelected.classList.remove("selected");
  
    if (tabParent) {
      if (preSelected !== null) {
        let preTab = tabParent.querySelector(`#${preSelected.id}`);
        preTab.classList.toggle(parentClass);
      }
    }
  }
  
  // Handle Popups for Right-Bar buttons
  function handleTabs(target, targetsClass, tabParent, parentClass){
    if (!target.classList.contains("selected")) removeSelected(targetsClass, tabParent, parentClass);
    target.classList.toggle("selected");
    tabParent.querySelector(`#${target.id}`).classList.toggle(parentClass);
  }
  


  //Press button Event
  let rightBar = document.querySelector("#right_bar .buttons-column");
  let popups = document.getElementById("popups");
  
  rightBar.addEventListener("click",e=>{
    if(e.target.localName === "button" && !e.target.classList.contains("popup-button")){
      updateProcessStep(e.target);
      removeSelected(".popup-button", popups, "in");
    }
    else if(e.target.localName === "button" && e.target.classList.contains("popup-button")){
      handleTabs(e.target, ".popup-button", popups, "in");
      generatePath(e.target);
    }
  });


  //Press Map-Buttons Event
  let searchButtons = document.querySelector(".map-inputs .search-buttons");
  let searchContents = document.querySelector(".topBar-contents");

  searchButtons.addEventListener("click",e=>{
    if(e.target.localName === "button"){
      handleTabs(e.target, ".map-inputs .search-buttons button", searchContents, "hidden");
    }
  });



//Drug and Drop
var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

let draggable = document.getElementsByClassName("draggable")[0];
dragElement(draggable);

function dragElement(tag) {
  let dragBtn = tag.querySelector(".drag-btn");
  dragBtn ? dragBtn.onmousedown = dragMouseDown : tag.onmousedown = dragMouseDown;


  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    tag.style.top = (tag.offsetTop - pos2) + "px";
    tag.style.left = (tag.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }

}

// Open/Close Python
let pythonButton = document.querySelector(".footer button");

pythonButton.addEventListener("click",e=>{
  document.querySelector("#python-script").classList.remove("hidden");
});

document.querySelector(".drag-btn button").addEventListener("click", e=>{
  document.querySelector("#python-script").classList.toggle("hidden");
});




// //W3W & LatLong
// let leaflet = document.querySelector(".leaflet-control-attribution");
// leaflet.addEventListener("click",e=>{
//   // document.getElementsByClassNam("map-inputs")[0].classList.toggle("hidden");
//   console.log(e);
// });


//Sign in window
document.getElementById("sign-in").addEventListener("click",e=>{
  console.log(e);
  document.getElementById("credentials").classList.toggle("hidden");
});