
  
  let smootheningTab = document.querySelector(".popup#smoothening");
let single = smootheningTab.querySelector("#single-option");
let singleTab = smootheningTab.querySelector("#single-option-parameters");
let multipletTab = smootheningTab.querySelector("#multiple-option-parameters");
single.checked = true;

smootheningTab.addEventListener("click",e=>{
  if(e.target.id === "single-option"){
    singleTab.classList.remove("hidden");
    if(!multipletTab.classList.contains("hidden")) multipletTab.classList.toggle("hidden");
  }else if(e.target.id === "multiple-options"){
    multipletTab.classList.remove("hidden");
    if(!singleTab.classList.contains("hidden")) singleTab.classList.toggle("hidden");
  }
});


  let logo = document.querySelectorAll("#raid-logo a");
  logo[1].setAttribute("href",`${window.location.href.replace(window.location.pathname,"/")}`);
  logo[0].setAttribute("href",`${window.location.href.replace(window.location.pathname,"/")}`);


function handleOpenClose(tag) {
  if (tag.querySelector(".open-close")) {
    let arrowButtons = tag.querySelectorAll(".open-close button");
    arrowButtons[0].classList.toggle("hidden");
    arrowButtons[1].classList.toggle("hidden");
  }
}

  function removeSelected(targetsClass, tabParent, parentClass, selectedClass = "selected") {
    let preSelected = document.querySelector(`${targetsClass}.${selectedClass}`);
    if (preSelected !== null) {
      preSelected.classList.remove(`${selectedClass}`);
      handleOpenClose(preSelected)
    }
  
    if (tabParent) {
      if (preSelected !== null) {
        
        let preTab = tabParent.querySelector(`.${preSelected.id}`);
        preTab.classList.toggle(parentClass);
      }
    }
  }
  
  // Handle Popups
  function handleTabs(target, targetsClass, tabParent, parentClass){
    if (!target.classList.contains("selected")) removeSelected(targetsClass, tabParent, parentClass);
    target.classList.toggle("selected");
    tabParent.querySelector(`.${target.id}`).classList.toggle(parentClass);
    handleOpenClose(target);
  }


  //Press button Event
  let buttonsCol = document.querySelector(".bar .buttons-column");
  let popups = document.getElementById("popups");
  
  buttonsCol.addEventListener("click",e=>{
    if(e.target.localName === "button" && e.target.classList.contains("popup-button")){
      handleTabs(e.target, ".popup-button", popups, "in");

      document.getElementById("designed-alignment-tab").classList.remove("selected");
      document.getElementsByClassName("designed-alignment-tab")[0].classList.remove("in");
      openCloseIFrame(document.getElementById("designed-alignment-frame"), true);
    }
    else if(e.target.localName === "button" && e.target.parentElement.classList.contains("header")){
      handleTabs(e.target.parentElement.parentElement, ".category", buttonsCol, "hidden");
      removeSelected(".popup-button", popups, "in");

      if(e.target.parentElement.querySelector(".hidden").id==="plus"){
        document.getElementById("designed-alignment-tab").classList.remove("selected");
        document.getElementsByClassName("designed-alignment-tab")[0].classList.remove("in");
        openCloseIFrame(document.getElementById("designed-alignment-frame"), true);
      }
    }
    else if(e.target.localName === "button" && e.target.parentElement.parentElement.classList.contains("header")){
      handleTabs(e.target.parentElement.parentElement.parentElement, ".category", buttonsCol, "hidden");
    }
    else if(e.target.localName === "button"){
      removeSelected(".popup-button", popups, "in");
      removeSelected("button", undefined, undefined, "current");
      e.target.classList.toggle("current");
      e.target.classList.remove("loaded");
    }
    
  });


  //Press side bar toggle
  let bar = document.getElementsByClassName("bar")[0];
  let toggle = document.getElementsByClassName("side-bar-toggle")[0];
  let logIn = document.getElementById("log-in");

  toggle.addEventListener("click",e=>{
    bar.classList.toggle("hidden");
    toggle.classList.toggle("in");
    toggle.innerHTML === "&lt;&lt;" ? toggle.innerHTML = "&gt;&gt;" : toggle.innerHTML = "&lt;&lt;";
    removeSelected(".popup-button", popups, "in");

    document.getElementsByClassName("designed-alignment-tab")[0].classList.toggle("full-screen");
    if(document.getElementById("designed-alignment-frame") && !toggle.classList.contains("down")){
      toggle.classList.toggle("down");
      logIn.classList.toggle("hidden");
    }else if(document.getElementById("designed-alignment-frame") != undefined){
      toggle.classList.remove("down");
      logIn.classList.remove("hidden");
    }
  });


  //Log In
  logIn.addEventListener("click", e=>{
    document.getElementById("credentials").classList.toggle("hidden");
  });

  
  //Press Map-Buttons Event
  let searchButtons = document.querySelector(".map-inputs .search-buttons");
  let searchContents = document.querySelector(".topBar-contents");
  
  searchButtons.addEventListener("click",e=>{
    if(e.target.localName === "button"){
      handleTabs(e.target, ".map-inputs .search-buttons button", searchContents, "hidden");
    }
  });


  
  function openCloseIFrame(tag, close, parentTag, id, src){
    if(tag || close){
      tag ? tag.parentElement.removeChild(tag) : "";
    }else{


   
          let tag = document.createElement("iframe");
          tag.id = id;
          tag.setAttribute("src",src);
          parentTag.appendChild(tag);

          
       

        setTimeout(function() { 
          updateTheMetrics();
        }, 2000);





    }
  }


  function updateTheMetrics(){
    // let metrics_array_to_pushed= ["/metrics?fileName=5f58c2cb-d1f4-4e78-8af5-a7a4d3a84e6f_refined_v1.json&codeName=b6bed389221a03f4cbe726eb2281401c95a8492f3e22bb09d6f6d6f4f6f67e8c"];
    document.getElementById("designed-alignment-frame").contentWindow.linkList=metrics_array_to_pushed;
    document.getElementById("designed-alignment-frame").contentWindow.updateMetricPath();
  }

  //Press tab buttons Event
  let tabButtons = document.getElementsByClassName("tab-buttons")[0];
  let tabParent = document.getElementsByClassName("main_page")[0];

  tabButtons.addEventListener("click",e=>{
    if(e.target.id === "designed-alignment-tab"){
      handleTabs(e.target, ".popup-button", popups, "in");
      removeSelected(".popup-button", popups, "in");

      openCloseIFrame(document.getElementById("designed-alignment-frame"), undefined, document.getElementsByClassName(e.target.id)[0], "designed-alignment-frame", "Path_Analysis/Designed_Alignments/index.html");
    }
    else if(e.target.localName  === "button" && !e.target.classList.contains("selected") && e.target.id !== "existing-alignment-tab"){
      handleTabs(e.target, ".tab-buttons button", tabParent, "hidden");
      
      removeSelected(".popup-button", popups, "in");
      document.getElementById("designed-alignment-tab").classList.remove("selected");
      document.getElementsByClassName("designed-alignment-tab")[0].classList.remove("in");
      openCloseIFrame(document.getElementById("designed-alignment-frame"), true);
    }
  });

//------------------------Drug
var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

let draggable = document.getElementsByClassName("draggable");
dragElement(draggable[0]);

function dragElement(tag) {
  let dragBtn = tag.querySelector(".drag-btn");
  dragBtn ? dragBtn.onmousedown = dragMouseDown : tag.onmousedown = dragMouseDown;


  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    tag.style.top = (tag.offsetTop - pos2) + "px";
    tag.style.left = (tag.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }

}
//------------------------


//---------------------------------------------------------------------------------------------------------------------
// Open/Close Graph
let graph = document.querySelector("#left_bar");
let graphButton = document.querySelector("#tab-1 #land-use");

graphButton.addEventListener("click",e=>{
  graph.classList.toggle("hidden");
  if(!graph.classList.contains("hidden")){
    updateColumnGraph(graphData);
  }
});
//---------------------------------------------------------------------------------------------------------------------

