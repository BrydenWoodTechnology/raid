
var  messages= [
    {
      "name": "Fence Boundary",
      "pointTo": "ImportFB",
      "order": 1,
      "category": "INPUTS",
      "steps": [
        "ImportFB_msg_1",
        "ImportFB_msg_2",
        "ImportFB_msg_3",
        "ImportFB_msg_4"
      ],
      "messages": [
        "Reading Fence Boundary coordinates from the Database",
        "Creating spatial feature from data",
        "Saving file to local directory",
        "Preparing visual output & saving figure"
      ]
    },
    {
      "name": "Environmental Data",
      "order": 2,
      "pointTo": "ImportENV",
      "category": "INPUTS",
      "steps": [
        "ImportENV_msg_1",
        "ImportENV_msg_2",
        "ImportENV_msg_3",
        "ImportENV_msg_4",
        "ImportENV_msg_5"
      ],
      "messages": [
        "Reading Environmental Data from local directory",
        "Dissolving features on REM category",
        "Saving output file to local directory",
        "Changing�feature�names according to Standards",
        "Preparing visual output & saving figure"
      ]
    },
    {
      "name": "Frame Points",
      "pointTo": "ImportFRM",
      "category": "INPUTS",
      "order": 3,
      "steps": [
        "ImportFRM_msg_1",
        "ImportFRM_msg_2",
        "ImportFRM_msg_3"
      ],
      "messages": [
        "Reading Frame Point data from the Database",
        "Constructing the�Dataframe",
        "Preparing visual output & saving figure"
      ]
    },
    {
      "name": "Inputs",
      "pointTo": "ImportALL",
      "category": "INPUTS",
      "order": 4,
      "steps": [
        "ImportALL_msg_1"
      ],
      "messages": [
        "Preparing visual output & saving figure"
      ]
    },
    {
      "name": "Global Parameters",
      "pointTo": "ImportGP",
      "category": "INPUTS",
      "order": 5,
      "steps": [
        "ImportGP_msg_1",
        "ImportGP_msg_2",
        "ImportGP_msg_3",
        "ImportGP_msg_4",
        "ImportGP_msg_5"
      ],
      "messages": [
        "Reading from the Database � Environmental Parameters",
        "Constructing the�Dataframe�� Environmental Parameters",
        "Reading from the Database � Standard Layer Names",
        "Constructing�the�Dataframe�� Standard Layer Names",
        "Merging�Dataframes�"
      ]
    },
    {
      "name": "Central Reserve Line",
      "pointTo": "PreprocessCR",
      "category": "PRE-PROCESSING",
      "order": 6,
      "steps": [
        "PreprocessCR_msg_1",
        "PreprocessCR_msg_2"
      ],
      "messages": [
        "Constructing line from points",
        "Saving file"
      ]
    },
    {
      "name": "Frame Boxes",
      "pointTo": "PreprocessFRB",
      "category": "PRE-PROCESSING",
      "order": 8,
      "steps": [
        "PreprocessFRB_msg_1",
        "PreprocessFRB_msg_2",
        "PreprocessFRB_msg_3",
        "PreprocessFRB_msg_4",
        "PreprocessFRB_msg_5",
        "PreprocessFRB_msg_6",
        "PreprocessFRB_msg_7",
        "PreprocessFRB_msg_8",
        "PreprocessFRB_msg_9",
        "PreprocessFRB_msg_10",
        "PreprocessFRB_msg_11"
      ],
      "messages": [
        "Populating mid-points",
        "Creating lines�perpendicular�to the central reserve line",
        "Constructing polygons around the Frame Points",
        "Clipping Frame Boxes to left buffer",
        "Clipping Frame Boxes to right buffer",
        "Offsetting�Frame�Points 1 metre to the left & 1 metre to the right",
        "Spatial join between Frame Boxes and�Frame Points - Carriageway A",
        "Spatial join between Frame Boxes and Frame Points - Carriageway�B",
        "Passing Frame Point data to Frame Boxes",
        "Clipping the Frame Boxes to the Fence Boundary�",
        "Preparing visual output & saving figure"
      ]
    },
    {
      "name": "Gantry Model",
      "pointTo": "AnalysisG",
      "category": "ANALYSIS",
      "order": 9,
      "steps": [
        "AnalysisG_msg_1",
        "AnalysisG_msg_2",
        "AnalysisG_msg_3",
        "AnalysisG_msg_4",
        "AnalysisG_msg_5",
        "AnalysisG_msg_6",
        "AnalysisG_msg_7",
        "AnalysisG_msg_8",
        "AnalysisG_msg_9"
      ],
      "messages": [
        "Gantry Model",
        "Filtering Global Parameters",
        "Concatenating the Frame Boxes of both Carriageways",
        "Assigning risk values to Frame Points using buffer distance and soft estate values�",
        "Calculating the total risk per Frame Point",
        "Counting the number of different risk�types per�Frame Point",
        "Creating a Geo-Dataframe�and saving�data as Shapefile to local directory",
        "Saving data as CSV to local directory",
        "Preparing visual output & saving figure",
        "Writing output values to Database"
      ]
    },
    {
      "name": "EA Model",
      "pointTo": "AnalysisEA",
      "category": "ANALYSIS",
      "order": 10,
      "steps": [
        "AnalysisEA_msg_1",
        "AnalysisEA_msg_2",
        "AnalysisEA_msg_3",
        "AnalysisEA_msg_4",
        "AnalysisEA_msg_5",
        "AnalysisEA_msg_6",
        "AnalysisEA_msg_7",
        "AnalysisEA_msg_8",
        "AnalysisEA_msg_9",
        "AnalysisEA_msg_10",
        "AnalysisEA_msg_11",
        "AnalysisEA_msg_12"
      ],
      "messages": [
        "Filtering Global Parameters",
        "Divide Environmental�Data to�right & left side of the road",
        "Preparing visual output & saving figure",
        "Assigning risk values to Frame Points using buffer distance and soft estate�values�-�Carriageway A",
        "Assigning risk values to Frame Points using buffer distance and soft estate�values�-�Carriageway B",
        "Concatenating risk output values",
        "Calculating the total risk per Frame Point",
        "Counting the number of different risk types per Frame Point",
        "Creating a Geo-Dataframe�and saving data as Shapefile to local directory",
        "Saving data as CSV to local directory",
        "Preparing visual output & saving figure",
        "Writing output values to Database"
      ]
    },
    {
      "name": "BoreHoles",
      "pointTo": "Boreholes",
      "order": 11,
      "category": "Boreholes",
      "steps": [
        
      ],
      "messages": [
        "Computing nearest Boreholes"
      
      ]
    },    
    {
      "name": "Download Boreholes PDF",
      "pointTo": "DownloadBoreholes",
      "order": 12,
      "category": "Boreholes",
      "steps": [
        
      ],
      "messages": [
        "Downloading borehole pdf"
      
      ]
    }
  ]
