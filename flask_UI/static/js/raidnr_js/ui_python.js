

// javascript files for the raidNR UI


function findPath(e){

    let new_weights = retrieveWeights();


    var origin= $("#OriginLatLong")[0].value;
    var destination= $("#DestinationLatLong")[0].value;

    var payload = {"weights":new_weights,"origin":origin,"destination":destination}
    var payload_encoded = JSON.stringify(payload);

   
   let functionName = e.target.dataset.point_to;

   console.log(payload_encoded);
    $.ajax({

       type: 'POST',
       data: payload_encoded,
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       url: '/nontasklistener?functionName=gen_path',
       success: function(data, status, request) {
           
           plot_file_name = data['plot'];
           if(plot_file_name!=""){

               console.log(plot_file_name);
               addImage(plot_file_name);

           }

           $('button.' + functionName).css("background-color", "rgb(17, 99, 65) ");


       },

       error: function() {

           alert('function didnt go through' );
       }
   });



}



//update the progress bar in the UI
function update_progress(status_url,functionName) {
   // send GET request to status URL
   $.getJSON(status_url, function(data) {
      
       if ('result' in data){

            if(data["messageID"]=="show_tiles"){


            }

           $('button.' + functionName).css("background-color", "rgb(17, 99, 65)");

           console.log(data['result']);
       } else {
           // rerun in 2 seconds
           setTimeout(function(){ update_progress(status_url,functionName)}, 2000);
       }
   });
}


 


function start_python_task(e) {

   console.log("ok got it");
   // add task status elements
   div = $('<div class="progress">' +
       '<div></div>' +
       '<div></div>' +
       '<div>...</div>' +
       '<div></div>' +
       '</div><hr>');

   var result_div = $("#here_image");

   let functionName = e.target.dataset.point_to;


   $('#progress' + functionName).append(div);

   // // create a progress bar
   // var nanobar = new Nanobar({

   //     bg: '#44f',
   //     target: div[0].childNodes[0]

   // });

   var codeName = localStorage.getItem("codeName");
   // console.log(codeName);
   // send ajax POST request to start background job
   $.ajax({

       type: 'POST',
       url: '/listener?functionName=' + functionName+"&codeName="+codeName,
       success: function(data, status, request) {
           status_url = request.getResponseHeader('Location');
           console.log(status_url);
           update_progress(status_url,functionName);
       },

       error: function() {

           alert('Unexpected error');
       }
   });
   
   
}


function update_tiles_directly(e){

    let functionName = e.target.dataset.point_to;
    var codeName = localStorage.getItem("codeName");

    let tile_path = "test_tile/"+codeName.substring(0,8)+codeName.substring(codeName.length-8,codeName.length)



    document.getElementById("leaflet_container").contentWindow.update_tiles(tile_path);
    $('button[data-point_to=' + functionName+']')[0].classList.toggle("loaded");
    $('button[data-point_to=' + functionName+']').css("background-color", "#e3e3e3");

}

var metrics_array_to_pushed=[];

function start_python(e) {


    var codeName = localStorage.getItem("codeName");

   console.log("ok got a simple request");
   // add task status elements

   let functionName = e.target.dataset.point_to;

   $.ajax({

       type: 'POST',
       url: '/nontasklistener?functionName=' + functionName+"&codeName="+codeName,
       success: function(data, status, request) {

        $('button[data-point_to=' + functionName+']')[0].classList.toggle("loaded");

           $('button[data-point_to=' + functionName+']').css("background-color", "#e3e3e3");
           response = data;

           if(response['data']!=""){


            if(response["data"]["pointTo"]=="leaflet_plot_mul_alignments"){
                
                // let links_for_metrics=[];

                for(var i=0; i<response["data"]["json_path"].length; i++){

                    console.log(response['data']["json_path"][i]);
                    let geoJson_url = response["data"]["json_path"][i];
                    let download_ulr = "/outputfiles?fileName="+geoJson_url+"&codeName="+codeName;
                    document.getElementById("leaflet_container").contentWindow.addGeoJson(download_ulr,"line",geoJson_url);
                    add_download_link(download_ulr,geoJson_url);
                    
                    let metric_fileName = response["data"]["metrics_path"][i];
                    let metric_url = "/metrics?fileName="+metric_fileName+"&codeName="+codeName;


                    metrics_array_to_pushed.push(metric_url);


                }
                // console.log(links_for_metrics)

            }

            if(response["data"]["pointTo"]=="leaflet_plot_tiff"){
                let tiff_path = "/static/"+response["data"]["tiff_path"]
                let min_max = response["data"]["min_max"]
                let bbox = response["data"]["bbox"]
                // document.getElementById("leaflet_container").contentWindow.addGeoTiff(tiff_path,min_max)
                document.getElementById("leaflet_container").contentWindow.add_overlayImage(tiff_path,bbox)
                add_download_link(tiff_path,"fitness_layer");


            }


            if(response["data"]["pointTo"]=="leaflet_plot_tiles"){
                let tiff_path = response["data"]["tile_path"]
                document.getElementById("leaflet_container").contentWindow.update_tiles(tiff_path);

            }

            if(response["data"]["pointTo"]=="metrics"){

                let metric_fileName = response["data"]["metrics_data"]["alm_fileName"];
                let metric_url = "/metrics?fileName="+metric_fileName+"&codeName="+codeName;
                metrics_array_to_pushed.push(metric_url);


            }

           }


           if(response['plot']!=""){


            $("#here_image").attr("src", response['plot']);
                add_download_link(response['plot'],"plot");

          }
           
           console.log("success");
       },

       error: function() {

           alert('Unexpected error');
       }
   });
   
  

   
}



function post_request_payload(functionName,data_payload) {

   // add task status elements
   console.log(data_payload);
   var codeName = localStorage.getItem("codeName");

   var response;

   $.ajax({

       type: 'POST',
       data: data_payload,
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       url: '/nontasklistener?functionName='+functionName+"&codeName="+codeName,
       success: function(data, status, request) {
        
           $('button[data-point_to=' + functionName+']')[0].classList.toggle("loaded");
          
           $('button[data-point_to=' + functionName+']').css("background-color", "#e3e3e3");

           response = data;
           
           console.log(response);

           if(response['data']!=""){


            if(response["data"]["pointTo"]=="leaflet_plot_mul_alignments"){
                    
                for(var i=0; i<response["data"]["json_path"].length; i++){

                    // console.log(response['data']["json_path"][i]);
                    let geoJson_url = response["data"]["json_path"][i];
                    let download_ulr = "/outputfiles?fileName="+geoJson_url+"&codeName="+codeName;
                    document.getElementById("leaflet_container").contentWindow.addGeoJson(download_ulr,"line",geoJson_url);
                    add_download_link(download_ulr,geoJson_url)
                    
                    let metric_fileName = response["data"]["metrics_path"][i];
                    let metric_url = "/metrics?fileName="+metric_fileName+"&codeName="+codeName;
                    metrics_array_to_pushed.push(metric_url);


                }

               
            }

               

                if(response["data"]["pointTo"]=="leaflet_plot_alignment"){
                    let geoJson_url = response["data"]["json_path"]
                    let download_ulr = "/outputfiles?fileName="+geoJson_url+"&codeName="+codeName
                    document.getElementById("leaflet_container").contentWindow.addGeoJson(download_ulr,"line",geoJson_url)
                    add_download_link(download_ulr,geoJson_url);

                }

                if(response["data"]["pointTo"]=="leaflet_move_map"){
                    
                    // let coord_data = {"lat":response["data"]["coords"][1],"lng":response["data"]["coords"][0]};
                    document.getElementById("leaflet_container").contentWindow.map.setView({"lat":response["data"]["coords"][1],"lng":response["data"]["coords"][0]})
                    document.getElementById("leaflet_container").contentWindow.marker.setLatLng({"lat":response["data"]["coords"][1],"lng":response["data"]["coords"][0]});

                }     


                if(response["data"]["pointTo"]=="updateCost"){
                    let data_cost = response["data"]["weightDict"];
                    handleGraphData(data_cost);

                }

                
                if(response["data"]["pointTo"]=="population_query"){
                    
                    let population_query = response["data"]["population"];

                    document.getElementById("leaflet_container").contentWindow.assign_population_on(population_query)

                }

            

            if(response["data"]["pointTo"]=="metrics"){

                let metric_fileName = response["data"]["metrics_data"]["alm_fileName"];
                let metric_url = "/metrics?fileName="+metric_fileName+"&codeName="+codeName;

                metrics_array_to_pushed.push(metric_url);


            }



           }

           if(response['plot']!=""){


             $("#here_image").attr("src", response['plot']);
             add_download_link(response['plot'],"plot");

           }
       },

       error: function() {

           alert('function didnt go through' );
       }
   });

   
  
  return response

   
}



