	
			String.prototype.format = function () {

				var str = this;

				for ( var i = 0; i < arguments.length; i ++ ) {

					str = str.replace( '{' + i + '}', arguments[ i ] );

				}
				return str;

			};

			var container, stats;
			var camera, scene, renderer;
			var splineHelperObjects = [];
			var splinePointsLength = 100;
			var positions = [];
			var point = new THREE.Vector3();
			var cursor;
			var geometry = new THREE.BoxBufferGeometry( 1, 1, 1 );
			var plane;
			var onClickPosition = new THREE.Vector2();
			var mouse = new THREE.Vector2();
			var raycaster = new THREE.Raycaster();
			var controls;



			var transformControl;
			var ARC_SEGMENTS = 200;

			var splines = {};

			var params = {
				uniform: true,
				tension: 0.5,
				centripetal: true,
				chordal: true,
				addPoint: addPoint,
				removePoint: removePoint,
				exportSpline: exportSpline
			};

			var textureH,textureW;
			textureH=500;
			textureW=500;


			var buildings_material= new THREE.MeshBasicMaterial() ;

			var rasterTextures = [];

			rasterTextures.push(buildings_material)

			init();

			let file = 'outfile.jpg';
			changeMap(file);

			function changeMap(file){


				var loader = new THREE.TextureLoader();


					// load a resource
				loader.load(
					// resource URL
					'static/files/'+file ,

					// onLoad callback
					function ( texture ) {
						// in this example we create the material when the texture is loaded
						buildings_material = new THREE.MeshBasicMaterial( {
							map: texture,
							transparent	:true,
							opacity	 : 0.9
						 } );

						textureW=texture.image.width;
						textureH=texture.image.height;
						
						
						plane.material= buildings_material;
						plane.material.needsUpdate = true;


					},

					// onProgress callback currently not supported
					undefined,

					// onError callback
					function ( err ) {
						console.error( 'An error happened.' );
					}
				);




			}

			



			function readFilePath(positions){
                
                var loader = new THREE.FileLoader();

				loader.load(

					'static/files/data.json' , function(jsonFile){initializeGrid(jsonFile,positions)}
					,
						// onProgress callback currently not supported
				undefined,

				// onError callback
				function ( err ) {
					console.error( 'An error happened.' );
				}
					)
			}
		
			function initializeGrid(fileJson,positions){

						console.log(positions.length)
										/*******
						* Curves
						 *********/

						var obj = JSON.parse(fileJson)

						splinePointsLength=obj.x.length;


							for ( var i = 0; i < splinePointsLength; i +=20) {

								xindex = obj.x[i];
								yindex = obj.y[i];


								addSplineObject(false ,xindex,yindex);
							

							}

							// console.log(splinePointsLength	);
							// console.log(splineHelperObjects.length	);
							// console.log(xindex);

							// console.log(splineHelperObjects[0].position);
							// positions = [];

							for ( var i = 0; i < splinePointsLength; i ++ ) {
								try{
									positions.push( splineHelperObjects[ i ].position );

								}
								catch(err){
									continue
								}

							}


					console	.log("positions length" + positions.length	);
					var geometry = new THREE.BufferGeometry();
					geometry.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array( ARC_SEGMENTS * 3 ), 3 ) );

					var curve = new THREE.CatmullRomCurve3( positions );
					curve.curveType = 'catmullrom';
					curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
						color: 0xff0000,
						opacity: 0.35,
					} ) );
					curve.mesh.castShadow = false;
					splines.uniform = curve;

					curve = new THREE.CatmullRomCurve3( positions );
					curve.curveType = 'centripetal';
					curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
						color: 0x00ff00,
						opacity: 0.35
					} ) );
					curve.mesh.castShadow = false;
					splines.centripetal = curve;

					curve = new THREE.CatmullRomCurve3( positions );
					curve.curveType = 'chordal';
					curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
						color: 0x0000ff,
						opacity: 0.35
					} ) );

					curve.mesh.castShadow = false;
					splines.chordal = curve;

					for ( var k in splines ) {

						var spline = splines[ k ];
						scene.add( spline.mesh );

					}

					animate();
					resizeCanvasToDisplaySize();
					}

			// instantiate a loader
	
			function init() {


				container = document.getElementById("three_container");
				console.log(container.style.width);

				scene = new THREE.Scene();
				scene.background = new THREE.Color( 0xf5f5f5 );

				camera = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 20000 );
				camera.position.set( textureW, 200, textureH );

				scene.add( camera );

				scene.add( new THREE.AmbientLight( 0xf0f0f0 ) );
				var light = new THREE.SpotLight(0xffcc66, 1.5 );
				light.position.set( 0, 1500, 200 );
				light.castShadow = true;
				light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 70, 1, 200, 2000 ) );
				light.shadow.bias = - 0.000222;
				light.shadow.mapSize.width = 1024;
				light.shadow.mapSize.height = 1024;
				scene.add( light );

				var planeGeometry = new THREE.PlaneGeometry( textureW, textureH );
				planeGeometry.rotateX( - Math.PI / 2 );
				planeGeometry.rotateY( - Math.PI / 2 );


				planeGeometry.materials = rasterTextures;
				planeGeometry.dynamic = true;


				// var planeMaterial = new THREE.ShadowMaterial( { opacity: 0.2 } );

			    plane = new THREE.Mesh( planeGeometry, new THREE.MeshBasicMaterial());

				planeGeometry.rotateY(  Math.PI / 2 );

				plane.position.x+=textureW/2;
				plane.position.z+=textureH/2;
				plane.position.y = 0;
				// plane.receiveShadow = true;
				scene.add( plane );



				//add Cursor

				addCursor();

				var helper = new THREE.GridHelper( textureW-1, textureH+2);

				helper.position.x+=textureW/2;
				helper.position.z+=textureH/2;
				helper.position.y=0.1;


				helper.material.opacity = 0.25;
				helper.material.transparent = true;
				scene.add( helper );

				//var axes = new THREE.AxesHelper( 1000 );
				//axes.position.set( - 500, - 500, - 500 );
				//scene.add( axes );

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				// renderer.setSize( window.innerWidth, window.innerHeight );
				renderer.setSize(1600, 500);

				// renderer.shadowMap.enabled = true;
				container.appendChild( renderer.domElement );

				stats = new Stats();
				container.appendChild( stats.dom );

				window.addEventListener( 'resize', resizeCanvasToDisplaySize, false );

				var gui = new dat.GUI();

				gui.add( params, 'uniform' );
				gui.add( params, 'tension', 0, 1 ).step( 0.01 ).onChange( function ( value ) {

					splines.uniform.tension = value;
					updateSplineOutline();

				} );
				
				gui.add( params, 'centripetal' );
				gui.add( params, 'chordal' );
				gui.add( params, 'addPoint' );
				gui.add( params, 'removePoint' );
				gui.add( params, 'exportSpline' );
				gui.open();

				// Controls

				// let controls_container = $(".sideBar#right_bar")[0];

				controls = new THREE.OrbitControls( camera,renderer.domElement);
				controls.target.set( textureW/2, -200, textureH/2 );

				controls.damping = 0.2;
				controls.addEventListener( 'change', render );

				controls.addEventListener( 'start', function () {

					cancelHideTransorm();

				} );

				controls.addEventListener( 'end', function () {

					delayHideTransform();

				} );


				transformControl = new THREE.TransformControls( camera, renderer.domElement );
				transformControl.addEventListener( 'change', render );
				transformControl.addEventListener( 'dragging-changed', function ( event ) {

					controls.enabled = ! event.value;

				} );
				
				scene.add( transformControl );

				// Hiding transform situation is a little in a mess :()
				transformControl.addEventListener( 'change', function () {

					cancelHideTransorm();

				} );

				transformControl.addEventListener( 'mouseDown', function () {

					cancelHideTransorm();

				} );

				transformControl.addEventListener( 'mouseUp', function () {

					delayHideTransform();

				} );

				transformControl.addEventListener( 'objectChange', function () {

					updateSplineOutline();

				} );


				var cursorControl = new THREE.DragControls( [cursor], camera, renderer.domElement ); //
				cursorControl.enabled = false;
				cursorControl.addEventListener( 'hoveron', function ( event ) {

					transformControl.attach( event.object );
					console.log(event.object.position);
					cancelHideTransorm();

				} );

				cursorControl.addEventListener( 'hoveroff', function () {

					cursor.position.y = 0.1;
					delayHideTransform();

				} );



				var dragcontrols = new THREE.DragControls( splineHelperObjects, camera, renderer.domElement ); //
				dragcontrols.enabled = false;
				dragcontrols.addEventListener( 'hoveron', function ( event ) {

					transformControl.attach( event.object );
					// console.log(event.object.position);
					cancelHideTransorm();

				} );

				dragcontrols.addEventListener( 'hoveroff', function () {

					delayHideTransform();

				} );

				var hiding;

				function delayHideTransform() {

					cancelHideTransorm();
					hideTransform();

				}

				function hideTransform() {

					hiding = setTimeout( function () {

						transformControl.detach( transformControl.object );

					}, 2500 );

				}

				function cancelHideTransorm() {

					if ( hiding ) clearTimeout( hiding );

				}

				// container.addEventListener( 'mousemove', onMouseMove, false );
			    container.addEventListener('dblclick', onMouseMove, false);


				
				readFilePath(positions);

				
				// load( [ new THREE.Vector3( 289.76843686945404, 452.51481137238443, 56.10018915737797 ),
				// 	new THREE.Vector3( - 53.56300074753207, 171.49711742836848, - 14.495472686253045 ),
				// 	new THREE.Vector3( - 91.40118730204415, 176.4306956436485, - 6.958271935582161 ),
				// 	new THREE.Vector3( - 383.785318791128, 491.1365363371675, 47.869296953772746 ) ] );

			}

			var switchCamera = true;
			var prevCameraLoc,prevRotation,prevCotnrols;

			function changeCamera(){

				console.log("helloCamera");
				if(switchCamera){

					prevCameraLoc = camera.position;
					prevRotation = camera.rotation;
					prevCotnrols = controls.target;

					camera.setRotationFromEuler(new THREE.Euler(-1.5708,0,0));
					controls.enableRotate = false;


					camera.lookAt(cursor.position);
					camera.position.set( cursor.position.x, 200, cursor.position.z );
					
					camera.updateProjectionMatrix();
					
					controls.target.set( cursor.position.x, -200, cursor.position.z );
					controls.needsUpdate = true;
					switchCamera = false;



				}else{


					camera.position.set(prevCameraLoc.x,prevCameraLoc.y,prevCameraLoc.z);
					camera.setRotationFromEuler(new THREE.Euler(prevRotation.x,prevRotation.y,prevRotation.z));

					camera.updateProjectionMatrix();
					// camera.lookAt(prevCameraLoc.x,0,prevCameraLoc.z);
					camera.needsUpdate = true;


					controls.enableRotate = true;
					controls.target.set(200,prevCotnrols.y,200);
					controls.target.needsUpdate	 = true;

					switchCamera = true;


				}


				

			}






			function ondblclick(event) {
				// console.log(event);
			    x = (event.clientX /window.innerWidth) * 2 - 1;
			    y = -(event.clientY / window.innerHeight) * 2 + 1;

			    // console.log(x,y);
			    dir = new THREE.Vector3(x, y, -1)
			    dir.unproject(camera)

			    ray = new THREE.Raycaster(camera.position, dir.sub(camera.position).normalize())
			    var intersects = ray.intersectObject(plane);
			    if ( intersects.length > 0 )
			    {
			    	// console.log(intersects[0].point.x);
			    	moveCursor(intersects[0].point);
			    }
			}



				
			function addCursor(){
				
				var material = new THREE.MeshLambertMaterial( { color:  0xffcc66} );
				cursor = new THREE.Mesh( geometry, material );

				cursor.position.x=0;
				cursor.position.y=0;
				cursor.position.z=0;
				// splineHelperObjects.push(cursor);
				scene.add( cursor );




			}

			function moveCursor(position){


				cursor.position.copy(position);

				cursor.position.needsUpdate = true;

			}



			function addSplineObject( position,xindex,yindex ) {

				var material = new THREE.MeshLambertMaterial( { color:  0xffcc66} );
				var object = new THREE.Mesh( geometry, material );

				if ( position ) {

					object.position.copy( position );
					console.log("here");

				} else {

					// object.position.x = Math.random() * 1000 - 500;
					// object.position.y = Math.random() * 600;
					// object.position.z = Math.random() * 800 - 400;

					object.position.x = xindex;
					object.position.y = Math.random() * 100;
					object.position.z = yindex;

			
				}

				// console	.log(object.position);


				// object.castShadow = true;
				// object.receiveShadow = true;
				scene.add( object );
				splineHelperObjects.push( object );


				return object;

			}



			function addPoint() {

				splinePointsLength ++;

				positions.push( addSplineObject(position="",xindex=0,yindex=0).position );
				updateSplineOutline();

			}

			function removePoint() {

				if ( splinePointsLength <= 4 ) {

					return;

				}
				splinePointsLength --;
				positions.pop();
				scene.remove( splineHelperObjects.pop() );

				updateSplineOutline();

			}

			function updateSplineOutline() {

				for ( var k in splines ) {

					var spline = splines[ k ];

					var splineMesh = spline.mesh;
					var position = splineMesh.geometry.attributes.position;

					for ( var i = 0; i < ARC_SEGMENTS; i ++ ) {

						var t = i / ( ARC_SEGMENTS - 1 );
						spline.getPoint( t, point );
						position.setXYZ( i, point.x, point.y, point.z );

					}

					position.needsUpdate = true;

				}

			}

			function exportSpline() {

				var strplace = [];

				for ( var i = 0; i < splinePointsLength; i ++ ) {

					var p = splineHelperObjects[ i ].position;
					strplace.push( 'new THREE.Vector3({0}, {1}, {2})'.format( p.x, p.y, p.z ) );

				}

				console.log( strplace.join( ',\n' ) );
				var code = '[' + ( strplace.join( ',\n\t' ) ) + ']';
				prompt( 'copy and paste code', code );

			}

			function load( new_positions ) {

				while ( new_positions.length > positions.length ) {

					addPoint();

				}

				while ( new_positions.length < positions.length ) {

					removePoint();

				}

				for ( var i = 0; i < positions.length; i ++ ) {

					positions[ i ].copy( new_positions[ i ] );

				}

				updateSplineOutline();

			}

			function animate() {
  				
  				resizeCanvasToDisplaySize();

				requestAnimationFrame( animate );
				render();
				stats.update();

			}

			function render() {



				splines.uniform.mesh.visible = params.uniform;
				splines.centripetal.mesh.visible = params.centripetal;
				splines.chordal.mesh.visible = params.chordal;
				renderer.render( scene, camera );

			}


			function resizeCanvasToDisplaySize() {

				  const check = document.getElementById("three_container");
				  const width =check.clientWidth;
				  const height = check.clientHeight;


				  // look up the size the canvas is being displayed
		

				  // adjust displayBuffer size to match
				  if (check.width !== width || check.height !== height) {
				    // you must pass false here or three.js sadly fights the browser
				    renderer.setSize(width, height);
				    camera.aspect = width / height;
				    camera.updateProjectionMatrix();

				    // update any render target sizes here
			  }
			}



			function onMouseMove( evt ) {
				evt.preventDefault();
				var array = getMousePosition( container, evt.clientX, evt.clientY );
				onClickPosition.fromArray( array );
				var intersects = getIntersects( onClickPosition, scene.children );

				// console.log(intersects);

				if ( intersects.length > 0  ) {
					var uv = intersects[ 0 ].uv;
					// intersects[ 0 ].object.material.map.transformUv( uv );
					let pos = intersects[0].point;
					pos.y=0;
					moveCursor(pos);

					// setCrossPosition( uv.x, uv.y );
				}
			}

			var getMousePosition = function ( dom, x, y ) {
				var rect = dom.getBoundingClientRect();
				return [ ( x - rect.left ) / rect.width, ( y - rect.top ) / rect.height ];
			};

			var getIntersects = function ( point, objects ) {
				mouse.set( ( point.x * 2 ) - 1, - ( point.y * 2 ) + 1 );
				raycaster.setFromCamera( mouse, camera );
				return raycaster.intersectObjects( objects );
			};

		

		function addImage(){

			srcUrl="static/files/rail.jpg"
			var result_div = $("#here_image");

			result_div.attr("src", srcUrl);
		}