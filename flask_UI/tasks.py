# -----------------------------------------------------------------
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# -----------------------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Ioannis Toumpalidis
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Connecting Flask nodes with specific raid functions
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------



import os
import random
import time
from flask import Flask, request, render_template, session, flash, redirect, \
    url_for, jsonify,send_from_directory,Response

from werkzeug.utils import secure_filename

from celery import Celery


import json
import numpy as np
import numpy as np
import matplotlib.pyplot as plt

import inspect
import sys

import os.path
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
import base_methods as bm



UPLOAD_FOLDER = "./uploads"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask("app")
app.config['SECRET_KEY'] = 'top-secret!'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# # Celery configuration
# app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
# app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

# # Initialize Celery
# celery = Celery("app", broker='redis://localhost:6379/0')
# celery.conf.update(app.config)


client_img_path = "images/Sessions/plots/"



def returnFileCode(codeName):

	fileCode = codeName[:8]+codeName[-8:]

	return fileCode

def init_session(codeName):

    sessionCode = returnFileCode(codeName)

    bm.init_session(sessionCode)

    return {"result": "process_done","print":"","plot":"","data":""}



def create_bbox(data_payload,codeName):

    input_bbox = data_payload["bbox"]
    bm.create_bbox(input_bbox)


    return {"result": "process_done","print":"","plot":"","data":""}


def initialize_all(data_payload,codeName):

    grid(data_payload,codeName)
    init_existing_alignment(data_payload,codeName)
    manager_init(data_payload,codeName)

    return {"result": "process_done","print":"","plot":"","data":""}




def preprocess_context_data(data_payload,codeName):
    

    bm.preprocess_context_data()

    return {"result": "process_done","print":"","plot":"","data":""}



def tile_data(data_payload,codeName):
    

    codePath = returnFileCode(codeName)
    spath_tile="test_tile/"+codeName


    bm.tile_data()

    return {"result": "process_done","print":"","plot":"","data":{"pointTo":"leaflet_plot_tiles","tile_path":spath_tile}}



def grid(data_payload,codeName):

    bm.grid()

    return {"result": "process_done","print":"","plot":"","data":"",}

def read_bbox(data_payload,codeName):

    bm.read_bbox()

    return {"result": "process_done","print":"","plot":"","data":"",}



def init_existing_alignment(data_payload,codeName):
    bm.init_existing_alignment()
    
    return {"result": "process_done","print":"","plot":"","data":""}



def manager_init(data_payload,codeName):
    bm.manager_init()

    return {"result": "process_done","print":"","plot":"","data":""}

def manager_design(data_payload,codeName):
    
    origin_str = data_payload['origin']
    origin = (float(origin_str[0]),float(origin_str[1]))

    print("origin point " , origin)

    destination_str = data_payload['destination']
    destination = (float(destination_str[0]),float(destination_str[1]))
    print("destination point " , destination[0])

    w = [int(i) for i in data_payload['weights']]
    
    if data_payload['existing_alignment']== True:
        naa_ind= 'alignment'
    else:
        naa_ind= None
    plt_result=bm.manager_design(origin, destination, w, naa=naa_ind)
    geo_result=bm.manager_get_alignment_to_coords(fileName="alm",crs="wgs",geoDataFrame=True,save="json",refined=False)

    
    fileCode = returnFileCode(codeName)
    plt_path = client_img_path +fileCode+"/"+plt_result['plot']+".png"
    json_path =geo_result['json_path']


    return {"result": "process_done","print":"","plot":plt_path,"data":{"json_path":json_path,"pointTo":"leaflet_plot_alignment"}}





def f(value, trend='exp'):
    """ 
    """
    if np.isnan(value):
        value=0.
    assert(value>=0.0 and value<=1.0),"Curvature must be between 0. and 1., got {}".format(value)
    bend = ["extreme_small","small","medium","big","extreme_big","straight"]
    curvature_thresholds = {"extreme_small": [1.0, 0.0033333333333333335], 
                            "small": [0.0033333333333333335, 0.002], 
                            "medium": [0.002, 0.001], 
                            "big": [0.001, 0.0002], 
                            "extreme_big": [0.0002, 0.000125], 
                            "straight": [0.000125, 0.0] 
                            }
    weight=0.1
    for c,iv in curvature_thresholds.items():
        if value < iv[0] and value > iv[1]:
            weight += bend.index(c) #
            break
    # if trend=='exp':
    #     return np.exp(weight)
    return weight




def refine_loop(data_payload,codeName):

    loop_number = int(data_payload["iterations"][0])
    
    refine = "curvature"

    
    iterations =int(data_payload["iterations"][1])
    s = [int(data_payload["s"][0]),int(data_payload["s"][1])]

    fileCode = returnFileCode(codeName)

    json_paths = []
    metrics_paths=[]
    metrics_parameters=[]

    s_loop =1

    for i in range(1,loop_number):
        
        
        print('\n{:-^80s}'.format(str(i) + " of %s "%(loop_number)))
        print(s,iterations)
        print("current on ", s_loop)


        plt_result=bm.manager_refine(refine=refine, iterations=iterations, s=s_loop)
        geo_result=bm.manager_get_alignment_to_coords(fileName="alm",crs="wgs",geoDataFrame=True,save="json",refined=True)

        # iterations+=5
        s_loop+=100

        plt_path = client_img_path + fileCode +"/"+ plt_result['plot']+".png"
        json_path =geo_result['json_path']

        json_paths.append(json_path)

        
        print('\n{:-^80s}'.format('Analyzing path..'))

        manager_analyse_refined_alm("",codeName)

        print('\n{:-^80s}'.format('Pushing data to DB..'))

        output_names = manager_output_refined_alm("",codeName)
        metrics_parameters.append(output_names['data']['metrics_data']['params_fileName'])
        metrics_paths.append(output_names['data']['metrics_data']['alm_fileName'])


        print('\n{:.^80s}'.format(''))
        print('\n{:.^80s}'.format(''))


        
    
    return {"result": "process_done","print":"","plot":plt_path,"data":{"json_path":json_paths,"pointTo":"leaflet_plot_mul_alignments","plot_title":"land_type","metrics_path":metrics_paths,"metrics_parameters":metrics_parameters}}


def manager_refine_fillet(data_payload,codeName):

    data_payload["keep_existing"]=True
    data_payload['isloop']=False
    result=manager_refine(data_payload,codeName)

    return result

def manager_refine(data_payload,codeName):
    

    
    isloop = data_payload['isloop']

    
    if isloop:
        result=refine_loop(data_payload,codeName)
        return result

    else:

        refine = data_payload["type"]
        iterations = int(data_payload["iterations"])
        s = int(data_payload["s"])

        try:
            keep_existing = data_payload['keep_existing']
        except:
            keep_existing = False

        plt_result=bm.manager_refine(refine, iterations, s,keep_existing=keep_existing)
        geo_result=bm.manager_get_alignment_to_coords(fileName="alm",crs="wgs",geoDataFrame=True,save="json",refined=True)

        fileCode = returnFileCode(codeName)

        plt_path = client_img_path +fileCode+"/"+plt_result['plot']+".png"
        json_path =geo_result['json_path']

                
        return {"result": "process_done","print":"","plot":plt_path,"data":{"json_path":json_path,"pointTo":"leaflet_plot_alignment","plot_title":"path_plot"}}
   

def manager_analyse_new_alm(data_payload,codeName):

    bm.manager_analyse_new_alm()

    fileCode = returnFileCode(codeName)
    plt_result=bm.query_cost_alignment(refined=False)
    plt_path = client_img_path +fileCode+"/"+plt_result['plot']



    return {"result": "process_done","print":"","plot":plt_path,"data":{"plot_title":"land_type_plot"}}


def manager_analyse_refined_alm(data_payload,codeName):

    bm.manager_analyse_refined_alm()
    
    fileCode = returnFileCode(codeName)
    plt_result=bm.query_cost_alignment(refined=True)
    plt_path = client_img_path +fileCode+"/"+plt_result['plot']

    return {"result": "process_done","print":"","plot":plt_path,"data":{"plot_title":"land_type_plot"}}



def manager_output_new_alm(data_payload,codeName):
    
    output_fileNames=bm.manager_output_new_alm(True)

    return {"result": "process_done","print":"","plot":"","data":{"pointTo":"metrics","metrics_data":output_fileNames}}


def manager_output_refined_alm(data_payload,codeName):
    
    output_fileNames=bm.manager_output_refined_alm(True)

    return {"result": "process_done","print":"","plot":"","data":{"pointTo":"metrics","metrics_data":output_fileNames}}



def update_geo_tiff(data_payload,codeName):
    
    fileCode = returnFileCode(codeName)



    
    import scipy.misc

    
    
    
    rnd_nu = str(np.random.randint(0,1000))
    plt_path = os.path.join(bm.sPATH_IMG,rnd_nu+"_image.png") 
    plt_path_ = client_img_path +fileCode+"/"+rnd_nu+"_image.png"
    
    norm = plt.Normalize()
    colors = plt.cm.inferno(norm(bm.global_manager.grid.fitness))
    
    scipy.misc.toimage(colors).save(plt_path)

    max_val=np.max(bm.global_grid.fitness)
    min_val=np.min(bm.global_grid.fitness)



    pointa = bm.gt.bng_to_wgs(bm.global_manager.grid.bbox[:2])
    pointb = bm.gt.bng_to_wgs(bm.global_manager.grid.bbox[2:])
    
    

    bbox_ = [pointa,pointb]
    

    return {"result": "process_done","print":"","plot":"","data":{"tiff_path":plt_path_,"pointTo":"leaflet_plot_tiff","min_max":[min_val,max_val],"bbox":bbox_}}




def query_cost_point(data_payload,codeName):

    lat = float(data_payload['marker']['lat'])
    lng = float(data_payload['marker']['lng'])    
    
    coords_wgs=(lng,lat)
    
    result=bm.query_cost_point(coords_wgs)
    # print(result)

    
    cost_dict=[]
    for i in result.keys():
        if(i=="DTM"):
            continue
        else:
            if(result[i][0]>0):
                cost_dict.append({"name":i,"value":result[i][0]})
            
    


    return {"result": "process_done","print":"","plot":"","data":{"weightDict":cost_dict,"pointTo":"updateCost"}}


def get_map_center(data_payload,codeName):

    lat = data_payload['lat']
    lng = data_payload['lng']

    coords = (lat,lng)
    result=bm.get_map_center(coords)
    print(result)
    

    return {"result": "process_done","data":{"lat":result[0],"lng":result[1],"pointTo":"recenter"},"plot":""}

def get_w3w(data_payload,codeName):


    w3w_list=data_payload['w3w_list']
    result = bm.get_w3w(w3w_list)


    return {"result": "process_done","data":{"pointTo":"leaflet_move_map","coords":result},"plot":""}



def findPopulation_in_range(data_payload,codeName):
    
    origin_str = data_payload['origin']
    origin = (float(origin_str[0]),float(origin_str[1]))

    print("origin point " , origin)

    destination_str = data_payload['destination']
    destination = (float(destination_str[0]),float(destination_str[1]))
    print("destination point " , destination[0])
  
    origin_coords_bng = bm.gt.wgs_to_bng(origin)
    destination_coords_bng = bm.gt.wgs_to_bng(destination)


    result_population = bm.findPopulation_in_range([origin_coords_bng,destination_coords_bng])


    return {"result": "process_done","data":{"pointTo":"population_query","population":result_population},"plot":""}



