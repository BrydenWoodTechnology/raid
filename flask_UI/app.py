# -----------------------------------------------------------------
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# -----------------------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Ioannis Toumpalidis
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Flask API for the RAID UI
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------


import os
import random
import time
from flask import Flask, request, render_template, session, flash, redirect, \
    url_for, jsonify,send_from_directory,Response

from werkzeug.utils import secure_filename

# from celery import Celery


import json

from multiprocessing import current_process
try:
    current_process()._config
except AttributeError:
    current_process()._config = {'semprefix': '/mp'}

import tasks as tsk 
import hashlib

import mimetypes
mimetypes.add_type('text/css', '.css')
# import pythonUI as rempyui


UPLOAD_FOLDER = "OutputFiles"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# Celery configuration
# app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
# app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'




@app.route('/metrics',methods=['GET','POST'])
def returnMetricsPath():


    main = os.getcwd()

    fileName = request.args.get('fileName') #if key doesn't exist, returns None
    print(fileName)

    codeName = request.args.get('codeName') #if key doesn't exist, returns None
    codeName= tsk.returnFileCode(codeName)
    print(codeName)

    uploads = os.path.join(main, app.config['UPLOAD_FOLDER'],"json")
    uploadDir = os.path.join(uploads,codeName,"metrics")

    print(uploadDir)
    return send_from_directory(directory=uploadDir, filename=fileName,mimetype='json',as_attachment=True)

 
@app.route('/outputfiles', methods=['GET','POST'])
def download():

    main = os.getcwd()

    fileName = request.args.get('fileName') #if key doesn't exist, returns None
    print(fileName)

    codeName = request.args.get('codeName') #if key doesn't exist, returns None
    codeName= tsk.returnFileCode(codeName)
    print(codeName)

    uploads = os.path.join(main, app.config['UPLOAD_FOLDER'],"json")
    uploadDir = os.path.join(uploads,codeName)

    print(uploadDir)
    return send_from_directory(directory=uploadDir, filename=fileName,mimetype='json',as_attachment=True)




@app.route('/nontasklistener',methods=['POST'])
def nontasklistener():

    functionName = request.args.get('functionName') #if key doesn't exist, returns None
    codeName = request.args.get('codeName') #if key doesn't exist, returns None
    
    try:
        data = request.data
        data_payload  = json.loads(data)
    except:
        data_payload=""

    task_result=getattr(tsk, functionName)(data_payload,codeName)

    

    # task=getattr(tsk, functionName).apply_async(kwargs={"codeName":codeName})
    # print(root_source)


    data = {"plot":task_result["plot"],"data":task_result["data"]}

    js = json.dumps(data)

    resp = Response(js, status=200, mimetype='application/json')

    return resp



    

@app.route('/postcred',methods=['POST'])
def savecred():


    data_payload = request.data
    data  = json.loads(data_payload)

    # data = json.loads(request.args.get('d')) #if key doesn't exist, returns None
    main = os.getcwd()

    url='http://api-node.bwtcreativetech.io/18050-REM/'
    param={}
    
    
    # TODO

    # response = rempyui.getCallMongoDB(data['u'],data['p'],url,param)
    # response.status_code
    status_code = 200

    if(status_code==200):
        m = hashlib.sha256()
        m.update(bytes(str(data["u"]),"utf-8"))
        m.update(bytes(str(data["p"]),"utf-8"))
        m.update(bytes(str(data["s"]),"utf-8"))
        m.digest()
        
        
        

        infoPath=os.path.join(main,"info")

        credFile=os.path.join(infoPath,"passCred.txt")
        
        

        try:
            with open(credFile, "rt") as fp:
                prevCred = json.load(fp)
        except IOError:
            print("Could not read file, starting from scratch")
            prevCred = {}


        loaded =False
        if (m.hexdigest() in prevCred.keys()):
            loaded = True
        
        else:
            
            # Add some data

            prevCred[m.hexdigest()] = data

       

        with open(credFile, "wt") as fp:
            json.dump(prevCred, fp)
        
       
        if data["s"]=="test_demo":

            codeName="b6bed389221a03f4cbe726eb2281401c95a8492f3e22bb09d6f6d6f4f6f67e8c"

            tsk.init_session(codeName)
            
            return jsonify({}),200,{"status":"success","codeName":codeName,"existing":loaded}

        else :

            tsk.init_session(m.hexdigest())

                
            return jsonify({}),200,{"status":"success","codeName":m.hexdigest(),"existing":loaded}
            
            

    else:

        return jsonify({}),404,{"status":"wrong credentials"}







@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)   



@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')



@app.route('/static/images/<filename>')
def send_image(filename):
    print("hello")
    return send_from_directory("static/images/", filename)



if __name__ == "__main__":

    app.secret_key = os.urandom(12)

    main = os.getcwd()
    ipHost = os.path.join(main,"info","ipconfigFile.txt")


    try:
        with open(ipHost, "r") as fp:
            prevCred = json.load(fp)
    except IOError:
        print("ERROR, navigate to info/ipconfigFile.txt and adress a valid IPv4 adress")
        

    print(prevCred['selected_ip'])
    app.run(debug=True,host=prevCred['selected_ip'], port=prevCred['port'])
    # app.run(debug=True,host='localhost',port=4000)

    # app.run(debug=True,host='10.50.1.205',port=4000)
