
# -----------------------------------------------------------------
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# -----------------------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Claudio Campanile, Ioannis Toumpalidis
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Function wrapper designed for the RAID UI
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------


from multiprocessing import current_process
try:
    current_process()._config
except AttributeError:
    current_process()._config = {'semprefix': '/mp'}


import os
import sys
import json
import random
import requests
import html
import re
import inspect
# scientific / data
import numpy as np
import pandas as pd
# geo
from shapely.geometry import LineString,Polygon,Point
import geopandas as gpd
from scipy import misc
import gdal
from osgeo import gdal_array
from gdalconst import *


# matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# import rempy root
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.core.alignment import Alignment
from raidnr.util.ext_methods import purge_poly, resample_poly, reconstruct_poly, fetch_shp, load_speed_limits, filter_files, snap_bbox
from raidnr.config import *
from raidnr.bwgrid import grid_transformations as gt
from raidnr.util import SpatialToolbox as st
from raidnr.bwgrid.grid import Grid
from raidnr.manager.design_manager import DesignManager
from raidnr.manager.remapper import RemapperEncoder, Remapper

# # ONLY FOR DEBUGGING
# if True:
#     ps=[PATH_RAILWAY, PATH_SPEED, PATH_OS, PATH_RASTER, PATH_BBOX, PATH_BBOX_OFF, PATH_CLIP_ALIGNMENT, PATH_CLIP_CONTEXT, PATH_CLIP_SPEED]
#     for i,p in enumerate(ps):
#         ps[i]=p.replace(r'\..','')
#     PATH_RAILWAY, PATH_SPEED, PATH_OS, PATH_RASTER, PATH_BBOX, PATH_BBOX_OFF, PATH_CLIP_ALIGNMENT, PATH_CLIP_CONTEXT, PATH_CLIP_SPEED = ps


# globals vars
global_grid             = None
topo_layers_fn          = None
global_bbox             = None
global_alignment        = None
global_manager          = None
# global_keys             = Keys()

# tmp data standard paths
sPATH_BBOX              = None
sPATH_BBOX_OFF          = None
sPATH_CLIP_CONTEXT      = None
sPATH_CLIP_CONTEXT_BUFF = None
sPATH_CLIP_SPEED        = None
sPATH_CLIP_ALIGNMENT    = None
sPATH_IMG               = None
sPATH_RASTER            = None
sPATH_JSON              = None

global_start =None
global_end =None


# module paths
cur_dir = inspect.getfile(inspect.currentframe())

modulePath=os.path.join(cur_dir,os.pardir)


ss_name_ = None

def init_session(ss_name):
    """ join base folders to session name

    Parameters
    ----------
    ss_name : str
        name of the session
    """
    if not isinstance(ss_name, str):
        ss_name = str(ss_name)

    # Specific paths for each session

    # shp
    # ----bbox
    global sPATH_BBOX
    sPATH_BBOX              = os.path.join(PATH_BBOX,             ss_name)
    global sPATH_BBOX_OFF
    sPATH_BBOX_OFF          = os.path.join(PATH_BBOX_OFF,         ss_name)    
    
    # ----content
    global sPATH_CLIP_CONTEXT
    sPATH_CLIP_CONTEXT      = os.path.join(PATH_CLIP_CONTEXT,     ss_name)
    global sPATH_CLIP_CONTEXT_BUFF
    sPATH_CLIP_CONTEXT_BUFF = os.path.join(PATH_CLIP_CONTEXT_BUFF,ss_name)
    global sPATH_CLIP_SPEED
    sPATH_CLIP_SPEED        = os.path.join(PATH_CLIP_SPEED,       ss_name)
    # ----alignment
    global sPATH_CLIP_ALIGNMENT
    sPATH_CLIP_ALIGNMENT    = os.path.join(PATH_CLIP_ALIGNMENT,   ss_name)
    
    # Images Directories
    # ----plots
    global sPATH_IMG
    sPATH_IMG               = os.path.join(PATH_IMG,              ss_name)
    # ---- tif
    global sPATH_RASTER
    sPATH_RASTER            = os.path.join(PATH_RASTER,           ss_name)

    #-----JSON

    global sPATH_JSON
    sPATH_JSON            = os.path.join(outputPathJSON,           ss_name)


    global sPATH_TILE
    sPATH_TILE            = os.path.join(PATH_TILE,           ss_name)

    sPATH_JSON_METRICS = os.path.join(outputPathJSON,ss_name,"metrics")
    # create session paths
    sessionPaths=[sPATH_BBOX,sPATH_BBOX_OFF,sPATH_CLIP_CONTEXT,
        sPATH_CLIP_CONTEXT_BUFF,sPATH_CLIP_SPEED,sPATH_CLIP_ALIGNMENT,sPATH_IMG,sPATH_RASTER,sPATH_JSON,sPATH_JSON_METRICS,sPATH_TILE]
    for sspath in sessionPaths:
        # path = os.path.join(sspath,ss_name)
        if(os.path.exists(sspath)==False):
            os.makedirs(sspath)
    


    global ss_name_
    ss_name_ = ss_name


# build tmp paths
rail_fn = filter_files(PATH_RAILWAY, ext='shp', no_of_groups=1)[0]
print(PATH_CLIP_SPEED)
# speed_fn = filter_files(PATH_CLIP_SPEED, no_of_groups=1)[0]
speed_fn = None
rail_clip_fn = os.path.join(PATH_CLIP_ALIGNMENT, 'clipped_rail.shp')
speed_clip_fn = os.path.join(PATH_CLIP_SPEED, 'clipped_speeds.shp')
bbox_fn = os.path.join(PATH_BBOX,'bbox.shp')
img_fn = filter_files(PATH_IMG, ext='png')
img_plots_fn = PATH_IMG

# METHODS

def findPopulation_in_range(longLat=[], rangeM = [1000,5000]):
    """
    Find population in a given range. 
    At the moment our available data are for the areas in proximity to the main track 
    that connects Manchester to York. 
    
    Parameters
    ----------
    
    longLat : array
        Tupple, array-like coordinates of the marker. Will be the center of the buffer.
        Coords needs to be in BNG. 

    
    rangeM : array-like
        Radius of the circle with center the longLat to query the available dataset,
        for an estimation of the population. (in meters)
    
    """
    

    population_fn = os.path.join(inputPathShp,"ONS","lsoa_population.shp")
    lsoa_shp=gpd.read_file(population_fn)
    

    return_result=[]

    for point_ in longLat:

        populations = []
        
        pt_circle = Point(point_[0], point_[1])

        for buffer_i in rangeM:
        
            circle = pt_circle.buffer(buffer_i)

            try:
                intersected_areas = lsoa_shp[lsoa_shp['geometry'].intersects(circle)]

                sum_population=sum(intersected_areas['population'])
            
                populations.append(sum_population)
            
            except:
                
                populations.append("n/a")

        return_result.append(populations.copy())

    
    result = {"markerStart":return_result[0],"markerEnd":return_result[1]}

    return result



def create_bbox(input_bbox):
    """ Create the bbox of the region of interest.

    Parameters
    ----------
        bbox : array-like
            Bbox array of [minx,miny,maxx,maxy] in wgs 
    """

    global global_bbox 
    global sPATH_BBOX

    minx,maxy=gt.wgs_to_bng((input_bbox[0],input_bbox[3]))
    maxx,miny=gt.wgs_to_bng((input_bbox[2],input_bbox[1]))

    bbox=[minx,miny,maxx,maxy]

    # user defined
    # bbox = [407912.83459730295, 414116.4445978563, 414391.0912720474, 417003.13814145426]

    test_bbox=Polygon([[bbox[0],bbox[1]],[bbox[2],bbox[1]],[bbox[2],bbox[3]],[bbox[0],bbox[3]]])

    # save to gpd
    bbox_gpd = gpd.GeoDataFrame(geometry=[test_bbox])
    # reproject
    bbox_gpd.crs = {'init' :'epsg:27700'}


    # TODO maybe we can read directly
    # align with the grid
    NW,SE=gt.get_UG_grid_bounds(bbox_gpd,corners=True)

    # adjust the new bounds
    minx,maxy=NW[0][0],NW[1][0]
    maxx,miny=SE[0][2],SE[1][2]

    # same procedure, maybe wrap to a function
    bbox=[minx,miny,maxx,maxy]
    test_bbox=Polygon([[bbox[0],bbox[1]],[bbox[2],bbox[1]],[bbox[2],bbox[3]],[bbox[0],bbox[3]]])
    bbox_gpd = gpd.GeoDataFrame(geometry=[test_bbox])
    bbox_gpd.crs = {'init' :'epsg:27700'}

    # export to shp
    fileName=os.path.join(sPATH_BBOX,"bbox.shp")
    bbox_gpd.to_file(fileName)

    print(fileName)
    bbox_shape=gpd.read_file(fileName)
    global_bbox=gt.get_Bounds_FromShape(bbox_shape)






def preprocess_context_data():
    """ Clip, buffer and rasterize the context within a pre-defined bbox
    """
    # TODO: add a check if the data exists already into the folder and matches the bbox    
    global global_bbox 
    global sPATH_BBOX
    global sPATH_RASTER


    # get bbox
    bbox = global_bbox.copy()
    bbox_fn = os.path.join(sPATH_BBOX,'bbox.shp')



    # ----shapefiles----
    print('\n{:-^80s}'.format('Preprocessing context data'))
    bf=pd.read_csv(FNAME_BUFFER)
    print('Clipping open data with bbox...')
    st.clipShp(PATH_OS, sPATH_CLIP_CONTEXT, bbox_fn) # first 
    print('Offsetting clipped data...')
    st.bufferShp(bf, clippedPath=sPATH_CLIP_CONTEXT, outputPath=sPATH_CLIP_CONTEXT_BUFF) 
    
    # output clean

    # # rasterization
    print('Rasterizing context data (shapefiles):')
    topo_layers_fn  = filter_files(sPATH_CLIP_CONTEXT_BUFF, ext='shp') # context shp
    for fn in topo_layers_fn:
        out_fn = str(os.path.splitext(os.path.split(fn)[1])[0])+'.tif'
        print('\t- {}'.format(out_fn))
        name=os.path.join(sPATH_RASTER, out_fn)
        gt.rasterize_shape(fn, bbox, tifName=name)

# ---- dtm
    print('Clipping context data (rasters):')
    raster_layers_fn = filter_files(PATH_DTM, ext='tif') # context shp
    for fn in raster_layers_fn:
        out_fn = "Processed_"+str(os.path.splitext(os.path.split(fn)[1])[0])+'.tif'
        print('\tClipping {}'.format(out_fn))
        name=os.path.join(sPATH_RASTER, out_fn)
        st.clip_raster(bbox_fn, fn, name)
        
        # raster_array = misc.imread(name)
        # filled_raster = st.fill_gaps(raster_array)
        
        # raster = gdal.Open(name, gdal.GA_Update) 
        
        # raster.GetRasterBand(1).WriteArray(filled_raster)
        # raster = None
        
        # st.clip_raster(bbox_fn, fn, name)

        # print("none")



def tile_data():

    global sPATH_RASTER
    global sPATH_TILE

    gt.tile_layers(sPATH_TILE,sPATH_RASTER)

    # lazy return


def read_bbox():
    """ Assign bbox to global_bbox variable
    """
    try:
        global global_bbox 
        globalbbox_gpd= gpd.read_file(os.path.join(sPATH_BBOX,"bbox.shp"))
        global_bbox=gt.get_Bounds_FromShape(globalbbox_gpd)
        return 1
    except Exception as e:
        print(e)
        return 0


def grid():
    """ Generate grid object from RAIDNR standard folder which contains raster layers
    """
    try:
        global global_bbox
        global global_grid
        global sPATH_RASTER

        
        print("Reading files...")
        grid_array = gt.get_grid_from_directory(sPATH_RASTER)
        
        print("Initializing Grid...")
        global_grid = Grid(grid_array, global_bbox, GRANULARITY)

        return 1
    except Exception as e:
        print('ERROR:',e)
        return 0


def get_grid():
    """ return global_grid variable
    """
    global global_grid
    return global_grid


def init_existing_alignment(resample_dist = 300.):
    """ Initialise an existing alignment

    Parameters
    ----------
    resampling : float
        [optional] default=300 metres, pitch every which a polyline is re-sampled
    """
    try:
        # make sure that the global grid exist
        global global_grid
        global sPATH_RAILWAY
        global sPATH_SPEED
        global sPATH_CLIP_ALIGNMENT
        global sPATH_CLIP_SPEED
        global sPATH_BBOX

        rail_fn = filter_files(PATH_RAILWAY, ext='shp', no_of_groups=1)[0]
        speed_fn = filter_files(PATH_SPEED, no_of_groups=1)[0]
        rail_clip_fn = os.path.join(sPATH_CLIP_ALIGNMENT, 'clipped_rail.shp')
        speed_clip_fn = os.path.join(sPATH_CLIP_SPEED, 'clipped_speeds.shp')
        bbox_fn = os.path.join(sPATH_BBOX,'bbox.shp')

        # Process existing alignment data (include speed data)
        print('\n{:-^80s}'.format('Processing alignment data'))
        st.clipOGR(bbox_fn, rail_fn, rail_clip_fn)          # clip alignment
        st.clipOGR(bbox_fn, speed_fn, speed_clip_fn)        # clip speed data
        segments, speeds = load_speed_limits(speed_clip_fn) # load speed data
        
        # object instantiation
        print('Init alignment object:')
        alignment = Alignment.from_shp_poly(fn=rail_clip_fn, id=2100, resample_dist=resample_dist, granularity=GRANULARITY, speed_geometry=segments, speed_values=speeds)
        print(alignment)
        
        # output
        global global_alignment 
        global_alignment = alignment
        
        return 1
    except Exception as e:
        print("ERROR: {}".format(e))
        return 0


def query_existing_alignment(push_to_mongo=False, save_plots_to=None, save_json_to=None):
    """ run to query an existing alignment

    Parameters
    ----------
    bbox : array-like
        bounding box
    
    resampling : float
        pitch every which a polyline is sampled.
    
    metrics : array-like of strings
        metrics to run

    push_to_mongo: bool
        True to push the alignment to MongoDB

    save_to : str
        filename for a json document
    """
    global global_alignment 
    if not global_alignment:
        init_existing_alignment()
    
    # standard parameters
    metrics = ["curvature","gradient","distance_deviation_analysis"]

    # output folder to save existing alignment as json file
    if not save_plots_to:
        save_plots_to = PATH_IMG
    if not save_json_to:
        save_json_to = os.path.join(outputPathJSON, 'alignment.json')
    elif save_json_to.split('.')[-1] != '.json':
        save_json_to = os.path.join(save_json_to, 'alignment.json')

    # run metrics and plot
    global_alignment.run_metrics(dtm_grid=global_grid, metrics=metrics)
    global_alignment.plot_metrics(fn=save_plots_to)

    # serialize, save to local and Mongo 
    if push_to_mongo or save_json_to:
        # serialise alignment
        print('\n{:-^80s}'.format('Serializing Alignment into a json-encoded string'))
        js = global_alignment.to_json_str()
        j = json.loads(js)
    if save_json_to:
        print('\n{:-^80s}'.format('Saving data to {}'.format(save_json_to)))
        with open(save_json_to,'w') as f:
            json.dump(j,f,indent=1)



def gen_path(start, end, weights, max_dev=0.):
    """ Generate new least cost path representing a new alignment

    Parameters
    ----------
    start : tuple
        origin point coordinates
    
    end : tuple
        destination point coordinates

    weights : dict
        {'layer_name':weight,...} as str:float
    
    max_dev : float
        [optional] default=0. representing the maximum cumulative squared error distance from the generated least cost path points to the smoothed new alignment design

    """
    # collect weights
    if isinstance(weights,dict):
        weight_dict = weights
    else:
        weight_dict={}
        for i in range(len(TOPO_LAYERS)):
            weight_dict[TOPO_LAYERS[i]]=weights[i]

    # make sure the context grid is in place
    global global_grid
    if not global_grid:
        read_bbox()
        grid()
        init_existing_alignment()
    
    # make sure the existing alignment is in place
    if not global_alignment:
        query_existing_alignment()
    
    # compute alignment
    print("Least cost path being calculated. Context features are weighted as:", weight_dict)
    global_grid.compute_new_alignment_(
        origin=start, 
        destination=end, 
        weights=weight_dict, 
        alignment=global_alignment, 
        naa='alignment', 
        s=max_dev, 
        plot=False
        )
    
    # keep it this way
    plt_result = plotGridLayer()
    return {"plot":plt_result['plot']}


def plotGridLayer(pts_alignment=[],pts_simplified=[],layer=None,fileName=""):
    """ 
    """
    global sPATH_IMG
    global global_manager
    global global_start
    global global_end


    # if(layer!=None):
    #     filename= str(layer)+"_%s"%(str(np.random.randint(0,10000)))
    #     filePath = os.path.join(sPATH_IMG,filename)
    # else:
    
    filename_new = "plot%s"%(fileName)
    filePath = os.path.join(sPATH_IMG,filename_new)

    global_manager.grid.plot(fileName=filePath,origin=global_start,destination=global_end,
        pts_alignment=pts_alignment,pts_simplified=pts_simplified)


    return {"plot":filename_new}


def analyse_new_path(k_size=21, save_to=None):
    """ 
    """
    if not global_grid:
        print("ERROR: Grid must be generated generated first")
        return 0
    # try:
    global_grid.evaluate_new_alignment_()
    global_grid.alignment_cost_surroundings(TOPO_LAYERS, k_size)
    global_grid.new_alignment.plot_metrics(fn=img_plots_fn)
    ras_na = global_grid.rasterize_alignment(global_grid._alignment, metrics=METRICS)
    # serialization
    if save_to:
        # serialise alignment
        print('\n{:-^80s}'.format('Serializing Alignment into a json-encoded string'))
        js = global_grid.new_alignment.to_json_str()
        j = json.loads(js)
        print('\n{:-^80s}'.format('Saving data to {}'.format(save_to)))
        with open(save_to,'w') as f:
            json.dump(j,f,indent=1)
    return 1
    # except Exception as e:
    #     print('ERROR:', e)
    #     return 0



def query_cost_alignment(refined=False,kernel_size=21):

    global global_manager

    if(refined):

        raster_coords=global_manager.alignment_refined.to_raster_coords(global_manager.grid)

        cost_dict=global_manager.grid.alignment_cost_surroundings(raster_coords,kernel_size)

        global_manager._a_rfnd_cost_srr=cost_dict

        plt_name="cost_refined_alm.png"
        fileName=os.path.join(sPATH_IMG,plt_name)

        global_manager.grid.plot_cost_surroundings(fileName=fileName,kernel_size=kernel_size)

    else:

        raster_coords = global_manager.alignment.to_raster_coords(global_manager.grid)

        cost_dict=global_manager.grid.alignment_cost_surroundings(raster_coords,kernel_size)

        global_manager._a_cost_srr=cost_dict

        plt_name="cost_alm.png"
        fileName=os.path.join(sPATH_IMG,plt_name)
        global_manager.grid.plot_cost_surroundings(fileName=fileName,kernel_size=kernel_size)

    
    return {"plot":plt_name}

def query_cost_point(coords_wgs, layer=None, k=21):
    """ 
    Returns
    -------
    OrderedDict
    """

    global global_grid
    global global_bbox

    if layer is None:
        layer=TOPO_LAYERS[:]

    coords = gt.wgs_to_bng(coords_wgs)

    j,i= gt.coords_geo_to_raster(global_bbox, coords)    
    
    cost_result=global_grid.point_cost_surroundings(k,i,j)


    return cost_result


def update_geo_tiff():
    
    global global_manager
    global sPATH_IMG

    outputFileName_bng=os.path.join(sPATH_IMG,"cost_layer_bng.tiff")
    
    rnd_number = np.random.randint(0,10000)
    outputFileName_wgs=os.path.join(sPATH_IMG,str(rnd_number)+"_cost_layer_wgs.tiff")



    gt.array_to_tiff(outputFileName_bng,global_manager.grid.fitness,global_manager.grid.bbox)
    gt.reproject_tiff(outputFileName_bng,outputFileName_wgs)

    return {"path":str(rnd_number)+"_cost_layer_wgs.tiff"}


def manager_init():
    """ 
    """
    try:
        global global_manager
        global global_grid
        global sPATH_IMG
        global ss_name_
        global global_alignment

        global_manager = DesignManager(grid=global_grid, session_name=ss_name_,existing_alm=global_alignment, image_path=sPATH_IMG)
        
        return 1
    except Exception as e:
        print("ERROR: {}".format(e))
        return 0


def manager_design(origin, destination, weights, naa=None, gmax=1.35):
    """ use the manager to design
    
    Parameters
    ----------
    origin : int
        origin on the raster
    
    destination : int
        destination on the raster
    
    weights : dict
        dictionary with weights for each topo layer

    naa : str
        'circuity' or 'alignment' to bind the path to a specific area
    
    gmax :float
        [optional] default=1.35 maximum circuity as applicable
    """
    global global_manager
    global global_start
    global global_end
    global global_alignment
    print(type(global_alignment))
    if isinstance(weights,dict):
        weight_dict = weights
    else:
        weight_dict={}
        for i in range(len(TOPO_LAYERS)):
            weight_dict[TOPO_LAYERS[i]]=weights[i]
    
    global_start = gt.wgs_to_bng(origin)
    global_end = gt.wgs_to_bng(destination)

    print("origin in bng", global_start)
    print("destination in bng", global_end)

    if not global_manager:
        manager_init()

    print(type(global_alignment))
    alignment_raster_coods = global_alignment.to_raster_coords(global_grid,to_int=True)

    global_manager.design_new_alm(global_start, global_end, weight_dict, naa=naa, gmax=gmax, alignment=alignment_raster_coods)

    # plt_path = os.path.join(sPATH_IMG,global_manager.alignment._id)
    plt_result=plotGridLayer(pts_alignment=global_manager._a_idx,fileName="_path_v"+str(global_manager._a_version))

    return {"plot":plt_result['plot']}


def manager_refine(refine, iterations, s, keep_existing=False):
    """ Refine the newly calculated least cost path

    Parameters
    ----------
    refine : dict
        parameters dictionary
    
    iterations : int
        number of iterations for the refinement process
    
    s : float
        smoothing factor. 0=not smooth
    """
    # refine parameters
    if refine == 'curvature':
        r = Remapper(crv_thresholds)
        refine_params = {'metrics':{'mweights':{'curvature':1.0}, 'mfuncs':{'curvature':r},'min_value':0.1, 'max_value':10.0}} # from config.py
    elif refine == 'distance':
        refine_params = {'distance':{}}
    else:
        refine_params = None
    
    global global_manager



    # try:
    if not keep_existing:
        print("s from manager",s)
        global_manager.refine_new_alm(refine=refine_params,iterations=iterations, s=s)
    else:
        global_manager.refine_new_alm_with_fillet(refine=refine_params,iterations=iterations, s=s)
    
    
    # plt_path = os.path.join(sPATH_IMG,str(global_manager._a_rfnd._id))

    plt_result= plotGridLayer(pts_alignment=global_manager._a_idx,pts_simplified=global_manager._a_rfnd_idx,fileName="_refined_v"+str(global_manager._a_rfnd_version))

    # except Exception as e:
    #     print("ERROR: alignment failed refinment due to Exception: {}".format(e))
    
    return {"plot":plt_result['plot']}


def manager_analyse_new_alm(metrics=None,  k_size=11):
    """ 
    """
    if metrics is None:
        metrics = METRICS[:]
    # if topo_layers is None:
    #     topo_layers = TOPO_LAYERS[:]
    
    global global_manager
    global_manager.analyse_new_alm(metrics=metrics, k_size=k_size)


def manager_analyse_refined_alm(metrics=None, k_size=11):
    """ 
    """
    if metrics is None:
        metrics = METRICS[:]

    # if topo_layers is None:
    #     topo_layers = TOPO_LAYERS[:]
    
    
    global global_manager
    global_manager.analyse_refined_alm(metrics=metrics, k_size=k_size)


def manager_output_new_alm(save_local_copy=False):
    """ Output alignment and associated parameters as:
        - Alignment: one document per point
        - Parameters: one document per alignment
    """
    global global_manager
    global ss_name_ 

    # alignment
    js = global_manager.serialize_new_alm()
    if save_local_copy:

        alm_fileName= global_manager._a_ID +"_alm_v"+str(global_manager._a_version)+'.json'
        file_path = os.path.join(sPATH_JSON,"metrics",alm_fileName)

        with open(file_path, 'w') as jf:
            json.dump(json.loads(js), jf)
    
    # params
    jw = json.dumps(global_manager.alignment_weights)
    if save_local_copy:

        params_fileName= global_manager._a_ID +"_alm_v"+str(global_manager._a_version) +'_params.json'
        file_path = os.path.join(sPATH_JSON,"metrics",params_fileName)

        with open(file_path, 'w') as jf:
            json.dump(json.loads(jw), jf)



    return {"params_fileName":params_fileName,"alm_fileName":alm_fileName}



def manager_output_refined_alm(save_local_copy=False):
    """ Output alignment and associated parameters as:
        - Alignment: one document per point
        - Parameters: one document per alignment
    """
    global global_manager
    global ss_name_ 
    
    params_fileName =""
    alm_fileName = ""
    # alignment
    js = global_manager.serialize_refined_alm()
    if save_local_copy:

        alm_fileName= global_manager._a_ID +"_refined_v"+str(global_manager._a_rfnd_version)+'.json'
        file_path = os.path.join(sPATH_JSON,"metrics",alm_fileName)

        # path = os.path.join(outputPathJSON, global_manager.alignment_refined_ID+'.json')
        with open(file_path, 'w') as jf:
            json.dump(json.loads(js), jf)
    
    # params
    jw = json.dumps(global_manager.alignment_refined_params, cls=RemapperEncoder)
    if save_local_copy:

        params_fileName= global_manager._a_ID +"_refined_v"+str(global_manager._a_rfnd_version) +'_params.json'
        file_path = os.path.join(sPATH_JSON,"metrics",params_fileName)

        # path = os.path.join(outputPathJSON, global_manager.alignment_refined_ID+str(global_manager.alignment_refined_version)+'_params.json')
        with open(file_path, 'w') as jf:
            json.dump(json.loads(jw), jf)
    
    

    
    
    return {"params_fileName":params_fileName,"alm_fileName":alm_fileName}

def manager_get_alignment_to_coords(fileName="",crs="wgs",geoDataFrame=True,save="json",refined=False):

    global global_manager
    global sPATH_JSON

    if(refined==False):
        fileName=global_manager._a_ID+ "_geopath_v"+str(global_manager._a_version)
        # fileName=global_manager.alignment._id
        file_path = os.path.join(sPATH_JSON,fileName)

        
        if os.path.exists(file_path+".json"):
            os.remove(file_path+".json")

        global_manager.get_alignment_to_coords(fileName=file_path,crs=crs,geoDataFrame=geoDataFrame,save=save)
    
    else:
        
        fileName=global_manager._a_ID+"_georefined_v"+str(global_manager._a_rfnd_version)
        # fileName=global_manager.alignment_refined_ID
        file_path = os.path.join(sPATH_JSON,fileName)

        
        if os.path.exists(file_path+".json"):
            os.remove(file_path+".json")

        global_manager.get_refined_alignment_to_coords(fileName=file_path,crs=crs,geoDataFrame=geoDataFrame,save=save)

    return {"json_path":fileName+".json"}




def get_w3w(w3w_list):

    coords=gt.w3wGetCoordinates(w3w_list)

    return coords


def get_map_center(coords):
    
    
    latLng=gt.get_UG_centroid_from_wgs(coords)

    return latLng


"""
refine={'distance':
            {
                'resampling_idx': 3.,
                'max_deviation': 10.,
                'min_value': 1.,
                'max_value': 10.,
                'include_ends': True,
                'iterations': 10
            }
        }
"""