## Input Data
The complete list of required data contains data in three different formats (SHP, CSV, TIF). Some of the data require some pre-processing in order to comply with the functionalities of the tool. 



The folder hierarchy of the input data is shown in the diagram below:

![InputData](/docs/images/methodology/InputData_hierarchy.PNG)









#### 1. 'csv' folder



- *Buffers:* A CSV file stored in .../flask_UI/InputData/csv that contains suggested buffer distances to be created around each one of the input layers. For more, see section of [Data Processing](docs/methodology_docs/data_processing_rasterization.md).

  > Save in: .../flask_UI/InputData/csv



- *Lower Layer Super Output Areas (LSOA) population estimates:* A CSV file of the population density per LSOA, provided by the Office for National Statistics under the Open Government Licence, [OGL](http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/). File should be named as *'lsoa_population'*.

  > Link for download: [here](https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/populationestimates/datasets/lowersuperoutputareapopulationdensity)
  >
  > Save in: .../flask_UI/InputData/csv.





#### 2. 'DTM' folder



- *Digital Terrain Model:* LIDAR Composite DTM of 2-metre resolution provided by the Environment Agency under the Open Government Licence, [OGL](<https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/> ). Data should be renamed as *DTM.tif*.

> Link for download: [here](<https://environment.data.gov.uk/DefraDataDownload/?Mode=survey> )
>
> Save in: .../flask_UI/InputData/DTM



**Note** that the DTM data needs to be converted from *.ascii* format and to *.tif*  (see the documentation for the relevant function - *translate_raster*).

The embedded clipping function in the package clips a raster *.tif* to the area's bounding box, resamples it to 3x3 metre pixel resolution and re-projects it to British National Grid (EPSG:27700). 'No data' values are denoted as *-9999*.





#### 3. 'shp' folder



##### *ONS folder*



- *Lower Layer Super Output Areas (LSOA) boundaries:* Shapefile data that contains the LSOA full extent boundaries in England and Wales, provided by Office for National Statistics (ONS) under the Open Government Licence [OGL](<https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/> ).

> Link for download: [here](<https://geoportal.statistics.gov.uk/datasets/lower-layer-super-output-areas-december-2001-full-extent-boundaries-in-england-and-wales?geometry=-2.401%2C52.867%2C-1.939%2C52.940> )
>
> Save in: .../flask_UI/InputData/shp/ONS.





##### *OS*



- *Data of the surrounding area:* A list of shapefiles provided by Ordnance Survey under the Open Government Licence, [OGL](http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/). Layers need to be renamed accordingly.

  > To download the data for a specific area go [here](https://www.ordnancesurvey.co.uk/opendatadownload/products.html) and select the *OS Open Map - Local* product for your preferred National Grid Reference square. 
  >
  > Save in: .../flask_UI/InputData/shp/OS/data/.

  

  _**Important Note** : The naming of the input data needs to follow the following convention. (The TOPO_LAYERS are defined inside the **config.py** file)._ 

  ```
  TOPO_LAYERS  = ['Building',
  			      'CarChargingPoint',
                  'DTM',
                  'ElectricityTransmissionLine',
                  'Foreshore',
                  'FunctionalSite',
                  'Glasshouse',
                  'ImportantBuilding',
                  'MotorwayJunction',
                  'RailwayStation',
                  'RailwayTrack',
                  'RailwayTunnel',
                  'Road',
                  'Greenspaces',
                  'NationalParks',
                  'RoadTunnel',
                  'Roundabout',
                  'SurfaceWater_Area',
                  'SurfaceWater_Line',
                  'TidalBoundary',
                  'TidalWater',
                  'Woodland']
  ```

- *Data of the surrounding area:* Two shapefile layers, Greenspaces and National Parks, provided by OpenStreetMap under the [ODbL Licence](opendatacommons.org/licenses/odbl/). The data sets for the area between Manchester Victoria and York are stored in the folder.

  > Save in: .../flask_UI/InputData/shp/OS/data.

  

- *Level crossings:*  Data on the position of more than 6,000 level crossings across the UK, provided by Network Rail. Data format conversion is required, from CSV to SHP. The shapefile should be projected to British National Grid (EPSG: 27700) and renamed to *'LevelCrossings'*.

  > Link for download: [Network Rail](https://www.networkrail.co.uk/who-we-are/transparency-and-ethics/transparency/datasets/)



- *Bridges:* A shapefile layer containing all the bridges (underbridge, overbridge, viaducts) extracted using spatial analysis functions from OpenStreetMap under the [ODbL Licence](opendatacommons.org/licenses/odbl/). The data for the area between Manchester Victoria and York station are provided in the folder. The name convention for the layer is *Bridges*.

  > Save in: .../flask_UI/InputData/shp/OS/data.





##### *Railway_Lines*





- *Manchester_York_Simple_Left_Line:* A shapefile of the railway alignment between Manchester Victoria and York stations extracted from OpenStreetMap under the [ODbL Licence](opendatacommons.org/licenses/odbl/), and processed accordingly. 
  It is provided in the relevant folder as an example of the how the input existing alignment data should be formatted. 

  > Save in: .../flask_UI/InputData/shp/Railway_Lines

**Note:** The tool can only process single-line alignments, meaning that railways with branches cannot be inserted as input data.




