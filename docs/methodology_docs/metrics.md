﻿# Alignment Metrics

Alignments metrics are an important piece of **raid**. Metrics are measurable quantities which represent topographic, context conditions or railway characteristics. Such metrics can be run on existing and newly generated alignment instances. Metrics can be used to compare different alignments and to refine design iterations.



### Curvature

The curvature is calculated at each alignment control point by fitting a circle which passes by the focal control point (B), and two more points before (A) and after (C) the focal point. The curvature is defined as:
```math
C = 1/r
```
where *r*  (m) is the radius of the circle. 



As an evaluation of curvature values, two bands are displayed on our graphs. The first highlights very low values of curvature i.e straight segments (Radius > 8,000 metres) and the second one high values of curvature that exceed the minimum allowed radius of curve (Radius < 300 metres).



The user can either run the curvature analysis for the entire alignment or query the curvature value at specific control point of the alignment.



### Permissible Speed

The permissible speed is calculated based on the curvature and the maximum allowed values for cant per alignment control point: 
```math
V = \sqrt{\dfrac{R(E + D)}{11.82}}
```
where: 

- *R* is the radius of the curve

- *E* is the applied Cant (mm)

- *D* is the Cant Deficiency (mm)

  

> > **Track Standards Manual - Section 8: Track Geometry** © Copyright 1998 Railtrack PLC



The range of maximum cant values based on the radius of curve is depicted on the following table:

| Radius in metres      | Maximum Applied Cant in mm |
| --------------------- | -------------------------- |
| more than 200m        | 150mm                      |
| between 200m and 150m | 100mm                      |
| between 150m and 100m | 50mm                       |
| less than 100m        | 25mm                       |



The user can either run the permissible speed analysis for the entire alignment or query the curvature value at specific control point of the alignment.



### Actual Speed

The actual speed has been calculated based on the permissible speed considering the acceleration and de-acceleration of the train and the length of the rail segment where the permissible speed is constant. 

A specific type of train was taken into consideration.



The user can either run the actual speed analysis for the entire alignment or query the curvature value at specific control point of the alignment.

### Ground Elevation

The ground elevation is the actual terrain height of each control point of the alignment. The alignment is translated into raster coordinates of a grid that represents the area that is explored and the values of the DTM are passed to these alignment grid cells. 

For the pixels of the DTM that there are missing data, the average heights of their surrounding cells are calculated and assigned to them to fill the data gaps.



### Alignment Elevation

The alignment elevation is calculated by considering the elevation difference between the origin and destination points and the total length of the alignment. 

We calculate the height that each alignment point needs to have, starting from the origin, in order to reach the ground elevation at the destination point progressively.



### Elevation Difference

The elevation difference is the difference between the ground and alignment elevation at each control point of the alignment. 



### Cut and Fill

The cut and fill calculation takes as inputs the elevation difference and and based on specific thresholds calculates the required amount of earthwork and the type of structure (bridge or tunnel) that is required in order to construct a railway with the allowed slope throughout.



### Gradient Smooth

The gradient smooth is actually the gradient but calculated in such way to avoid as much as possible noise in the data. For an alignment with granularity of 3 metres and different ground elevation values per control point, the resulting gradient profile contains many peaks and valleys. 

To overcome this issue and create a smoother and more realistic gradient profile we grouped together the sequential points that have elevation differences less than 1.5 metres (arbitrary threshold - assuming that below this threshold the ground is almost flat throughout). 

Once all the points have been assigned to a group, the average height has been considered and the ground elevation differences of these segments have been used to calculate the gradient ratios and percentages.



### Gradient Ratio

The gradient ratio is calculated with from the input values of the Digital Terrain Model. For each control point of the alignment (A), the distance between (A) and the next control point (B) is divided by the height difference between these two points.


```math
\dfrac{Distance (A,B)}{Height B - Height A}
```


The result of this calculation is a ratio denoted as _1: X metres_ 



### Gradient Percentage

The gradient calculated as a percentage, by dividing the height difference between two consequent control points A and B with the distance between these points and multiplying the result by 100.


```math
\dfrac{HeightB - HeightA}{Distance(A,B)}\times100
```


The result of this calculation is a percentage representing the gradient between two locations denoted as _X%_.



### Gradient Labels

The gradient labels indicate the alignment control points where the gradient values change.