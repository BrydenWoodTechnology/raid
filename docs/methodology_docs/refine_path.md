
# Methodology

## Chapter 3: Path Refinement Process

The Path refinement process aims to modify a least cost path generated alignment on a grid to an actual railway alignment. The logic is to smooth a pixel-based path into a curve which takes into account required characteristics. These can be geometrical (e.g. curvature) and or of other nature (e.g. cut and fill). Current smoothing implementations optimizes only curvature throughout the infrastructure.

> **Note:** The path refinement process can be invoked during the design phase of a new alignment
> 

### Smoothening Methodology

The smoothening process simplifies a raster-based least-cost path to address the curvature constraint of the alignment. Here, we show the process logic.



1. A least cost path is generated on the context grid, accordingly to user-defined weights.

![](..\images\path_refine\logic_00.png)



2. Interpolating the centre of each grid cell would produce a geometry characterised by curvature values which are unsuitable for an alignment.

![](..\images\path_refine\logic_01.png)



3. Instead of interpolating the centre of the cells, a regression can be run by ensuring an error allowance for a curve to fit.

![](..\images\path_refine\logic_02.png)



4. Still, the original cells might cause high curvature values on the alignment. A curvature analysis is performed to understand which subsets of the alignment can be loosen up to avoid high-curvature values.  Here, red values represent higher curvatures.

![](..\images\path_refine\logic_02_crv.png)



5. Curvature values are re-assigned to the closest cells.

![](..\images\path_refine\logic_02_crv_px.png)



6. Such curvature values are used to assign weight for the regression phase. Cells with higher value of curvature tend to be less-determinant in pulling the curve, to reduce their effect of create kinks. Here we report the curvature values as reverse radius of the circle which fits the alignment at each point throughout (unit: 1/m).

| weight | c~1~                  | c~2~                  |
| ------ | --------------------- | --------------------- |
| 1      | 1.0                   | 0.0033333333333333335 |
| 2      | 0.0033333333333333335 | 0.002                 |
| 3      | 0.002                 | 0.001                 |
| 4      | 0.001                 | 0.0002                |
| 5      | 0.0002                | 0.000125              |
| 6      | 0.000125              | 0.0                   |



5. The regression is applied again, with different weights as shown.

![](..\images\path_refine\logic_04.png)






## Smoothening Process

The whole process can be summarised with the following diagram. Moreover, it can be run iteratively to a finite number of iterations or until a specific condition is satisfied (not currently implemented).

```mermaid
graph LR
A(Path on grid)-->Z(regression)
Z-->B(Alignment)
B -->C(Curvature Analysis)
C -->D{curvature acceptable<br/>OR<br/>iteration ends}
D --Y-->E((Output))
D --N-->F(calculate regression weights)
F-- update weights -->Z
```
