﻿

# Methodology

## Chapter 2 : Path generation

Using the rasterized data derived from **Rasterisation Process** we form a multi dimensional array (A) of shape (*m,n.k*) where m,n is the number of  rows and columns of the 3m lattice constructed over the user specified region of interest and k is the number of different input topographical layers.

Each layer of the multi dimensional array is then multiplied by a user defined scalar to finally form a fitness layer L:

```math

L = \sum\limits_{n=1}^k A_n*w_n
```

where  $`w \in W=[w_1..w_k]`$ a vector containing scalar weights for each of the input topographical layer and can be modified by the user through the RAID UI. 



<div style="text-align:center">
    <p align="center">
        <img  style="width:1024px;height:auto" src="docs/images/path_generation/layers_smaller.jpg">
    </p>
    <p align="center"> Constructing the cost surface </p>
</div>


To generate a path between two points O,D ($`O=[i_o ,j_o]`$, $`D=[i_d,j_d] `$ where $`i \in [0,m] `$ and $`j \in [0,n] `$ we make use an implementation of the Dijkstra's minimum cost path algorithm from [scikit-image ](https://scikit-image.org/docs/dev/api/skimage.graph.html#skimage.graph.shortest_path) which  iterates over the fitness layer to identify the least cost path. 
 
 In addition to the input  topographical layers we optionally add  two layers of shape (m,n):
 - proximity to the existing alignment (using [Distance Transform](https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.ndimage.morphology.distance_transform_edt.html)) )
 - maximum circuity (Li et al, 2016)

>**Technical Note** :  More on multi dimensional arrays can be found here :  [numpy nd.arrays]([https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.html#numpy.ndarray)


