# Methodology

## Chapter 1 : Grid - Data Processing - Rasterization

### Grid

A core element of RAID is the use of a lattice grid. Any input data layer related to the cost evaluation is mapped to our custom grid layout.
Our lattice consists of an ortho canonical grid of 3x3 meters cells in British National reference system. 

Technically our grid tessellates the British National Grid and for it is also aligned with the what three words (w3w) service. 
To make a distinction between the general underlying grid and the one that we compute for each area of interest, we name the first as Universal Grid (UG) and the other as Region of Interest Grid (ROIG). 
For indexing purposes, the UG has a user specified origin point, indexed as (0,0) and follows a cartesian system where positive values grow from east to west in the horizontal axis and from south to north for the vertical axis. 

Each individual ROIG varies in width and height, depending on the bounding box of the area in question. The underlaying UG allows us to avoid errors when we evaluate areas in proximity keeping the orientation and the structure of the grid unanimous. 


As a result one of the initial steps that the user should do, is to define the region of interest by creating a bounding box.

>**Note:**  To create a bounding box follow either [draw a bounding box]() or [define a boudning box]() method. 



## Data Processing


Once the user has defined the bounding box the next step is to start processing the input data. 
During this stage, input data are clipped and a distance buffer is being generated for each attribute of the topographical layers. 

The buffer distances can be viewed and updated by the user by accessing the file: _.../flask_UI/InputData/csv/Buffers.csv_
Then buffer distances based on the type of the asset are applied to generate new shapes. The creation of these areas of influence around the assets stands as a safety factor when generating the cost layers for the path generation algorithm. For example, a buffer distance of 100 metres around a building, makes sure that no rail alignment will be generated closer than 100 metres from this building, which approximates the real site conditions when constructing a railway.


### Input Data



The input data can be divided into three main categories: 
 - Data on the alignment's surrounding area (buildings, water surface areas, woodland, etc.)
 - Data on the railway assets (tunnels, bridges, level crossings & stations)
 - Data on the ground elevation (Digital Terrain Model)

The first two categories are imported into the model as shapefiles stored at the same directory. The shapefiles are first clipped to the boundaries of the bounding box.
The third category, is the Digital Terrain Model (DTM) that needs to be clipped to the boundaries of the user-specified area.



For more information on the complete list of required input data please refer to [Input Data](docs/general_docs/Input Data.md)






## Rasterization 

The data processed layers are rasterized to match the Universal Grid (UG) template. Through this process each pixel of the final raster image represents a unique cell in our UG. The raw information of each pixel follows either a binary or a graduated scale. In the binary format 0 represents the absence of an attribute and 1 represents the existence of it in every cell. 

