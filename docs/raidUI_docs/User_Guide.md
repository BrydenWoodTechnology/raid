## User Guide


> **Note:** Make sure you have completed the Installation Guide and the Configuring UI notes. 

- [Setup New Session](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#1-setup-new-session)
- [Load Existing Session](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#2-load-existing-session)
- [Generate Path](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#3-generate-path)
- [Refine Existing Path](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#4-refine-path)
- [Compute Metrics](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#5-compute-metrics)
- [Marker Queries](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#6-marker-queries)
- [Export ](https://gitlab.com/bwtcreativetech/19029_raid_nr/blob/demo_test/docs/raidUI_docs/User_Guide.md#7-export)

---
### 1. Setup new Session 

<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/Setup new Session.jpg">
  
</p>


</div>


Before you enter the design studio you are prompting with a window to fill some user credentials. The reason of this process is to allow users to keep any generated information separated. It also allows users to revisit old sessions of them without waiting or preprocessing their input data again. Information for loading an existing environment head to [Load Existing Session]()



#### 1.1 Select Box Area

 Specifying the corner coordinates ( coordinates needs to be in BNG system) of the bounding box.


#### 1.2 Create Bbox

Invoke the creation of the bounding box shapefile. 


#### 1.3 Preprocess Input Data
Input data needs to be clipped in the selected area, so go and hit **Preprocess Data** (Setup > Preprocess Data).



#### 1.4 Tile Clipped Data

To allow better visual control we also provide the ability to tile the clipped input data for the region of your selection. Go and press **Tile Clipped Data** ( Setup new Session > Tile Clipped Data ) .

> **Debug:** Data are saved under the FlaskUI>static>test_tile directory. 



#### 1.5 Initialize 
This step initializes some core classes to store and handle information during the path generation and refinement stages.

---

### 2. Load existing Session 


<div style="text-align:center">
    <p align="center">
      <img  src="docs/images/raid_ui/load_existing.jpg">
    </p>
    
</div>

In case you are revisiting an existing session you will be prompt with an alert notifying you that you can load your previous environment.
To do so head to the Load Existing Session tab. Your available options here are similar with the ones which you used when created the session in the first place. 

**Important Notice** : Currently there is no indication in the UI of what processes have been completed during a previous session.

#### 2.1 Read Bbox ( load existing )
As the name suggests, the application will load the existing bounding box which you created and it was saved in the *FlaskUI>OutputData>shp>bbox>{session ID}*

#### 2.2 Update Tiles ( load existing )
Similarly with the **Read Bbox** functionality. Update Tiles will load the tile layers located in the *FlaskUI>static>test_tile* directory. 

#### 2.3 Initialize 
This step initializes some core classes to store and handle information during the path generation and refinement stages.

---

### 3. Generate Path 

<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/generate_path.jpg">
  
</p>


</div>

#### 3.1 Path End Points
In this tab you can specify the Origin and Destination point for the generated path. In addition the user is able to  control if the generated path should follow or not the existing alignment, when it is possible. 

#### 3.2 Grid Weights
In this tab user can specify the weight of the available data layers. 

#### 3.3 Generate Path
Generate a path!

#### 3.4 Update fitness layer
Adds the latest fitness layer which used in the generate path process to your map. 

---

### 4. Refine Path 


<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/refine_path.jpg">
  
</p>


</div>

#### 4.1 Smoothing Parameters 
Window where the user can control different smoothing parameters for one or multiple scenarios. 

#### 4.2 Smoothing Path
Invoke the smoothing process for the most recent generated path. 


---
### 5. Compute Metrics



<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/compute_metrics.jpg">
  
</p>


</div>

#### 5.1 Analyze New Path
Invoke a series of functions to comput metrics for the generated paths. 

#### 5.2 Analyze Refined Path
Similar with the **Analyze New Path** step, but instead the most recent refined path which was generated is analysed in this step. 

---

### 6. Marker Queries 



<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/marker_queries.jpg">
  
</p>


</div>

#### 6.1 Query on center marker ( blue ) 
Queries the [fitness layer]() under  the blue marker. 


#### 6.2 Query population ( origin - destination ) 
Invokes a spatial query for origin and destination markers in the [population data](). 

---

### 7. Export


<div style="text-align:center">
<p align="center">
  <img  src="docs/images/raid_ui/export.jpg">
  
</p>


</div>

#### 7.1 Export Files 
The available ouputs. In this section user can overview the available outputs for all the processes that have been conducted in the current session. 


> **Note:** All the files are kept locally under the FlaskUI>OutputFiles or FlaskUI>static>images.

---