## Initialize UI




### 1. Input Data

[Input data](docs/general_docs/Input%20Data.md) needs to be extracted in the InputData directory. 


### 2. Initialize Flask
The RAID UI was built with **Flask**. To initialize the webserver go to FlaskUI folder ( RAID>FlaskUI ) and in a CLI with your python environment activated  run the following. 

	start startUI.bat

> **Note:** Make sure you have activated the raid virtual env in conda. To do that in a CLI or anaconda prompt run the command : activate raid_env 
> 

### 3. Port configuration

The default is set to 4000. To change that edit the ipconfig.txt located in (RAID>FlaskUI>Info ) directory
