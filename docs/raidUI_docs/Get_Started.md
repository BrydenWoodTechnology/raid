## Get Started

Welcome to the Get Started for the RAID UI. In this section you can find the basic information of how to use the RAID UI in order to generate and review a series of paths. 

**What you will need**: Make sure you have followed the [Input Data]() instructions. 

 


### 1. Create a new session



In your landing page you will prompt with the credential window. Fill at least one of the following *username*, *password* and *sessionID* fields.
This will create a folder hierarchy for generated information to be stored accordingly avoiding any future conflicts.  

In order to start generating paths we will need to  specify a bounding box. This can be either done using the [1.1 Select Bbox Area ]() or by drawing a custom rectangular with the side toolbar.

<div style="text-align:center">
<p align="center">
  <img  src="docs/videos/2019-09-25 10-20-24.gif">
  
</p>

<p align="center">Drawing a bounding box & preprocess your data. </p>

</div>


Next press [Create Bbox]()  followed by the [Preprocessing Data ]() function. 
When the preprocessing of the data finishes we are ready to generate the first path. 



> **Debug:** Preprocessing data might take some time depending on how big is your selected area. You can always check the process of your action in the FlaskUI console.



### 2. Generate a path



Having our bounding box we can specify the origin and destination for the new path.  From the right toolbar press the button and two markers will spawn in the center of your active view in the map area. Drag the origin (green) and destination (red marker) marker accordingly and place them within the boundaries of your bounding box. 

Lets go and review some other options under the [Path End Points]() tab. For this example lets use the [Existing Alignment]() as an attractive factor of the generated path. 


> **Note:** Alternatively you can specify the origin and destination coordinates in the [Path End Points]() window. 

Next you will have to specify the weights for the available data layers from the [Grid Weights]() tab.

> **Note:**  Consider custom processing when including quantitative and qualitative input data layers. 
  
 and then finally press [Generate Path]().  If no error occurs the generated path will be placed on the map. We can also review the fitness layer that generated based on our grid weights input. 
 
 
### 3. Refine a path

We have a path!.. If don't follow the [1. Create a new session]() and the [2. Generate a path]() instructions. 

The generated path is now ready for refining. In the current version we are able to use only curvature as a constrain for refining the generated path. The user needs to specify the number of iterations which the algorithm will run and the distance of refinement ( sum of rsquared more info in [scipy]()) .

To run the refinement, press the **Smoothen Path** button..
When the refinement of the path is complete, a path will be placed on the map. You are now ready to compute metrics for the specified path.

Alternatively we can generate more than one refined option.. More specifically multiple options can generated when toggling the Create Mutliple Option in the **Smoothening Parameters** tab.  Under the extended menu we can specify :
* number of paths 
* iteration Number
*  min and max distance 

and again to run the process we need to press the **Smoothen Path**. This time the process will take more time to be completed but we also compute metrics for every refined path. When the refinement is complete, we can go directly to the **Designed Alignment Metrics**.


### 4. Compute Metrics 
If we haven't used the  Create Multiple Refined Paths option, we will need to analyze separately the generated and the refined paths. In any case the paths that can be analyzed are the most recent ones.

After generating a path and refining it we are now ready to analyze the paths by selecting [Analyse New Path]() and/or [Analyse Refined Path](). 
The results of the analysis can be reviewed in the **Designed Alignment Metrics** section. 

