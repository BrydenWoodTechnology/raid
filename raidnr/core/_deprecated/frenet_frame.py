"""

"""

# scientific modules
import numpy as np
from sympy import Plane
# modules
from shapely.geometry import LineString
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# import raidnr
from raidnr.config import CRS
from raidnr.util.ext_methods import tangent_vector

class FrenetFrame(Plane):

    def __new__(self,o, i, j, k, **kwargs):
        return super().__new__(self, p1=tuple(o), a=tuple(i), b=tuple(j), **kwargs)

    def __init__(self, o, i, j, k, **kwargs):
        """
        Plane class
        
        Parameters
        ----------
        o : ()
        """
        
        b = np.array([i,j,k])
        b = b / np.linalg.norm(b)
        self._base = b

    @property
    def base(self):
        return self._base
    @base.setter
    def base(self, base):
        raise ValueError("base is a read only attribute")

    @property
    def i(self):
        return self.base[0]
    @property
    def j(self):
        return self.base[1]
    @property
    def k(self):
        return self.base[2]

    def frame_triplet(self):
        """ Returns a (v1,v2,v3) of lines starting at the origin, of dimension 1.0
        """
        v1 = tangent_vector(self.p1, self.i)
        v2 = tangent_vector(self.p1, self.j)
        v3 = tangent_vector(self.p1, self.k)
        return np.array([v1,v2,v3])
    
    def display(self, ax=None, **kwargs):
        """ 
        """
        if ax == None:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        ff = self.frame_triplet()
        ff = ff.transpose() # shape to (start,final) coordinates
        start, end = ff
        x, y, z = [self.p1[0]]*3, [self.p1[1]]*3, [self.p1[2]]*3
        u, v, w = end[0]-start[0], end[1]-start[1], end[2]-start[2]
        color=['red','green','blue']
        c = renderColorsForQuiver3d(color)
        ax.quiver(x, y, z, u, v, w, length=1.0, normalize=True, color=c)

        if ax == None:
            plt.show()


def repeatForEach(elements, times):
    return [element for element in elements for _ in range(times)]
    
def renderColorsForQuiver3d(colors):
    # filter out 0-length vector corresponding to rgb=(0,0,0) that won't be drawn
    colors = filter(lambda x: x!=(0.,0.,0.), colors) 
    # set up rgb color for each lines of arrows
    return colors + repeatForEach(colors, 2) 