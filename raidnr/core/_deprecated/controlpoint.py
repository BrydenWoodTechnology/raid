"""

"""
# import dependencies
import numpy as np

# import raidnr


class ControlPoint(object):

    def __init__(self, x, y, z):
        """
        ControlPoint class
        """
        # checks
        assert(isinstance(x,float) and isinstance(y,float) and isinstance(z,float)), f"{x}, {y}, {z} are not all floats"
        # geometry data
        self._Pos = np.array([x,y,z], dtype=float)
        self._Dir = np.full(3, np.nan)
        # other properties
        self.Frozen = False

    @property
    def X(self):
        return self._Pos[0]
    @X.setter
    def X(self, x):
        assert(isinstance(x,float)), f"{x} is not a float"
        self._Pos[0] = x

    @property
    def Y(self):
        return self._Pos[1]
    @Y.setter
    def Y(self, y):
        assert(isinstance(y,float)), f"{y} is not a float"
        self._Pos[1] = y

    @property
    def Z(self):
        return self._Pos[2]
    @Z.setter
    def Z(self, z):
        assert(isinstance(z,float)), f"{z} is not a float"
        self._Pos[2] = z
    
    @property
    def Dir(self):
        return self._Dir
    @Dir.setter
    def Dir(self, dir):
        try:
            dir = np.array(dir,dtype=float)
        except:
            raise ValueError(f"{dir} cannot be parsed into a numpy array of dtype float")
        self._Dir = dir
        self.unit_dir()

    
    def is_valid(self):
        return True
    

    def has_dir(self):
        """Check if a direction has been set
        """
        return not np.isnan(self.Dir).any()
    

    def unit_dir(self):
        """Unitize direction vector
        """
        if self.has_dir():
            mag = np.linalg.norm(self._Dir)
            self._Dir = self._Dir/mag


    def __str__(self):
        return f'<ControlPoint ({self.X}, {self.Y}, {self.Z})>'