"""

"""

# scientific modules
import numpy as np
# modules
from shapely.geometry import LineString

# import raidnr
from raidnr.core.controlpoint import ControlPoint
# from raidnr.util.ext_methods import 

class Alignment(object):

    def __init__(self, id):
        """
        Alignment class
        """
        self._id = id
        
        raise NotImplementedError("To be implemented")
    
    @property
    def ID(self):
        return self._id
    @ID.setter
    def ID(self, id):
        raise ValueError("ID is read-only")


    @staticmethod
    def FromShpPoly(shp_file):
        """[Static] Create an alignment from a polyline shapefile 
        """

        # do stuff...

        raise NotImplementedError()

        return Alignment(id="test")

