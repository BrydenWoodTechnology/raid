# ---------------------------------------------------
# ¦¦¦¦¦¦¦    ¦¦¦¦¦   ¦¦¦¦  ¦¦¦¦¦¦¦¦  
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦¦¦¦¦   ¦¦¦¦¦¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦  
# ¦¦¦   ¦¦¦ ¦¦   ¦¦¦ ¦¦¦¦  ¦¦¦¦¦¦¦¦    
# ---------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Claudio Campanile, Konstantina Spanou
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Alignment class to model infrustructure alignments
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------

# modules
import json
import os
from copy import copy, deepcopy
# scientific modules
import numpy as np
from numpy.core.umath_tests import inner1d
from scipy import interpolate
from scipy.spatial import distance
import matplotlib.pyplot as plt
from math import sqrt
from numpy import inf
# geospatial
import geopandas as gpd
from shapely.geometry import LineString, Point

# import raidnr
from raidnr.config import CRS, GRANULARITY, METRICS , TOPO_LAYERS
from raidnr.config import CRS, GRANULARITY, METRICS, DEBUG, TOLERANCE_bng, TOLERANCE_ij

from raidnr.util.ext_methods import resample_poly, circle_3pt, moving_average, angle_deviation_3pt, purge_poly, reconstruct_poly,distance_deviation_3pt,extract_elevation,speedOnDiffCant,concatenate_arrays,slope
import raidnr.util.ext_methods as em
import raidnr.util.SpatialToolbox as st
from raidnr.core.frenet_frame import FrenetFrame
from raidnr.core.speed_curve import find_speed_curve
import raidnr.bwgrid.grid_transformations as gt

import math
class Alignment(object):

    def __init__(self, linestring, id, granularity, speed_geometry=None, speed_values=None, resampling=300.):
        """
        Alignment class

        Parameters
        ----------
        linestring: LineString
            Linestring object representing the alignment
        
        id: string
            Alignment unique ID
        
        granularity: float
            samplying distance
        """
        if not (isinstance(linestring, LineString) and 
        (isinstance(id, int) or isinstance(id,str))):
            raise ValueError("linestring must be a LineString object and id must be int or str, got {} and {}".format(type(linestring),type(id)))
        self._id = id
        self._granularity = granularity
        # linestring 
        self._resampling = resampling
        line = resample_poly(linestring, resampling)
        self._line = line
        # alignment interpolated curve 
        self.pt_length = None
        self._curve, self._control_pt = self._extract_curve()
        self._i = 0
        self.crv_length = self._recalc_length()
        # speed function 
        self._speed_curve = None
        self._speed_geometry = speed_geometry
        self._speed_values = speed_values
        self.calc_speed_curve()
        # metrics
        self.metrics = {METRICS[i]:None for i in range(len(METRICS))}
        self.assets_intervals = {}
        self.assets_at_point = {}
        
        # TO BE DELETED ?????
        self._least_cost_path = None
        self._non_smoothed = None
        # TMP - DANGER ZONE
        self._elevations = None
        self._ground_elevation  = None
        self._alignment_heights = None
        self.smooth_gradient = []
        self.gradient_labels = []
        self.gradient_pct =[]

    @property
    def ID(self):
        return self._id
    @property
    def granularity(self):
        return self._granularity
    # geometry
    @property
    def resampling(self):
        return self._resampling
    @property
    def line(self):
        return self._line
    @property
    def curve(self):
        return self._curve
    @property
    def points(self):
        return np.array([ff.o for ff in self])
    @property
    def points2D(self):
        return self.points.transpose()[:2].transpose()
    @property
    def T1(self):
        return self[0]
    @property
    def T2(self):
        return self[-1]
    # speed
    @property
    def speed_curve(self):
        return self._speed_curve
    @speed_curve.setter
    def speed_curve(self, v):
        self._speed_curve = v
    @property
    def speed_geometry(self):
        return self._speed_geometry
    @property
    def speed_values(self):
        return self._speed_values
    # TMP - DANGER ZONE
    @property
    def least_cost_path(self):
        return self._least_cost_path
    @least_cost_path.setter
    def least_cost_path(self,path):
        self._least_cost_path = path
    @property
    def non_smoothed(self):
        return self._non_smoothed
    @non_smoothed.setter
    def non_smoothed(self,path):
        self._non_smoothed = path


    # magic methods
    def __next__(self):
        if self._i < self.pt_length:
            u = self._control_pt[self._i]
            self._i+=1
            return self.frenet_frame_at(u)
        self._i = 0
        raise StopIteration()

    def __len__(self):
        return  len(self._control_pt)

    def __iter__(self):
        return self

    def __getitem__(self, key):
        u = self._control_pt[key]
        return self.frenet_frame_at(u)

    def __str__(self):
        return 'Alignment object at {}\n  ID: {}\n  length: {:.2f}\n  no of points: {}\n  granularity: {}m'.format(id(self), self.ID, self.crv_length, self.pt_length, self.granularity)

    def __add__(self, a):
        """ Add two alignments if consecutive (distance between ends < threshold as per config.py)
        """
        assert(isinstance(a, Alignment)), "'+' operation not valid between {} and {}".format(type(self), type(a))
        s_s, s_e, e_s, e_e, d = self.check_proximity(a)

        if d > self.granularity:
            #TODO put points to join the two alignments
            pass
        # make sure stat self's end and a's start are consecutive
        if s_s:
            line = list(reversed(list(self.points))) + list(a.points)
        elif s_e:
            line = list(reversed(list(self.points))) + list(reversed(list(a.points)))
        elif e_s:
            line = list(self.points) + list(a.points)
        elif e_e:
            line = list(self.points) + list(reversed(list(a.points)))
        line = LineString(line)
        # recalc alignment interpolated curve 
        ID = deepcopy(self.ID)
        granularity = deepcopy(self.granularity)
        speed_geometry = deepcopy(self.speed_geometry)
        speed_values = deepcopy(self.speed_values)
        resampling = deepcopy(self.resampling)
        return Alignment(line, ID, granularity, speed_geometry=speed_geometry, speed_values=speed_values, resampling=resampling)


    # private methods
    def _recalc_length(self):
        """ Calculate the alignment interpolated curve length. This does not equal the alignment linestring polyline

        Returns
        -------
        float
            curve length
        """
        # l = self._line.length #approx length
        # steps = l / (self.granularity * 0.5)
        # t = 1.0 / steps
        l = 0
        for i in range(1, len(self._control_pt)):
            u0 = self._control_pt[i-1]
            u1 = self._control_pt[i]
            pt0 = np.array(interpolate.splev(u0, self.curve))
            pt1 = np.array(interpolate.splev(u1, self.curve))
            l += np.sqrt(np.sum(np.square(pt1-pt0)))
        return l


    def _extract_curve(self):
        """ Extract interpolated curve through alignment points

        Returns
        -------
        tuple (tuple, float)
            tck, curve parameter
        """
        pts = np.array(self._line.coords).transpose()
        tck, u = interpolate.splprep(pts, s=0)
        self.pt_length = len(u)
        return tck, u


    def extract_subset(self, a, b):
        """ Returns a subset of the alignment

        Parameters
        ----------
        a : int
            starting trim index
        
        b : int
            endind trim index
        
        Returns
        -------
        raidnr.core.alignment.Alignment 
            alignment subset
        """
        if a > b:
            tmp=a
            a=b
            b=tmp
        assert(a>=0 and b<len(self)),"Clipping indices invalid. {} must be > 0 and {} < {}".format(str(a),str(b),str(len(self)))
        id             = deepcopy(self.ID)
        granularity    = deepcopy(self.granularity)
        resampling     = deepcopy(self.resampling)
        pts            = deepcopy(self.points[a:b])
        linestring     = LineString(pts)
        speed_geometry = self._speed_geometry
        speed_values   = self._speed_values
        return Alignment(linestring, id=id, granularity=granularity, speed_geometry=speed_geometry, speed_values=speed_values, resampling=resampling)


    # methods - processing
    def calc_speed_curve(self):
        """ Calculate speed curve, given speed segments and assigned maximum speed values

        Parameters
        ----------
        speed_geometry : list<LineString>
            segment at which speed is constant

        speed_values : numpy.array
            speed limit associated with segments
        
        Returns
        -------
        None
        """
        speed_geometry = self.speed_geometry
        speed_values   = self.speed_values
        if not(isinstance(speed_geometry, type(None)) and isinstance(speed_values,type(None))):
            try: # speed segments might be passed as points
                speed_geometry = st.speed_pts_to_segs(speed_geometry, line, id, bf_dist=20.)
            except:
                pass
            _,y,ps = find_speed_curve(speed_geometry, speed_values)
            ps = np.array(ps).transpose()
            tmp= np.expand_dims(np.zeros(ps.shape[1]), axis=0)
            ps = np.concatenate((ps,tmp))
            ps = ps.transpose()
            x = self.closest_u(ps)
            self._speed_curve = [x,y]
        else:
            self._speed_curve = None


    def calc_gradient_curve(self,gradient_points,gradient_values):
        """ WIP

        Parameters
        ----------
        
        

        Returns
        -------
        WIP
        """
        gradient_points = np.array(gradient_points).transpose()
        tmp= np.expand_dims(np.zeros(gradient_points.shape[1]), axis=0)
        gradient_points = np.concatenate((gradient_points,tmp))
        gradient_points = gradient_points.transpose()
        x = self.closest_u(gradient_points)
        
        array = [x,gradient_values]
        
        
        return np.interp(x=np.arange(len(self.points)),xp=array[0],fp=array[1])


    def extract_slope(self):
        """ Returns the gradient values as a ratio (distance/elevation difference), denoted as 1:X metres
        
        Parameters
        ----------
        

        Returns
        -------
        numpy.array
        """
        elevation = np.round(self.extract_ground_elevation())
        
        #splitting the ground elevation values where the difference is higher than 1.5 meters
        result = np.split(elevation, np.where(np.diff(elevation[:])>1.5)[0]+1)
        clear = concatenate_arrays(result)

        counter = 0
        gradients = []
        pts = []
        indexes = []

        for i in range(len(clear)):
            try:
                if i == len(clear):
                    pts.append(self.pts[-1].o[0:2])
                    indexes.append(counter)     
                else:
                    pts.append(self[counter + len(clear[i])-1].o[0:2])
                    indexes.append(counter)
                #gradient
                gradient = clear[i].mean()
                gradients.append(gradient) 
                counter = counter +  len(clear[i])
            except:
                print(counter)
                print(len(clear[i]))
                print(i)
                print(len(self.points))

        pointDistance = [self.line.project(Point(p)) for p in pts]

        X = np.insert(pointDistance, 0,0.0, axis=0)
        Y = np.insert(gradients,0,elevation[0], axis=0)

        gradient_ratio = self.calc_gradient_curve(pts,slope(X,Y)[0])
        self.gradient_pct = self.calc_gradient_curve(pts,slope(X,Y)[1])
        
        self.smooth_gradient = self.calc_gradient_curve(pts,gradients)

        self.gradient_labels = np.full(np.array(self.smooth_gradient).shape,float(0))
        np.put(self.gradient_labels, indexes, float(1))

        return np.array(gradient_ratio)


    def return_gradient_pct(self):
        """ Returns the gradient per alignment control point as a percentage

        Parameters
        ----------
        

        Returns
        -------
        numpy.array
        """
        return np.array(self.gradient_pct)

    
    def return_smooth_gradient(self):
        """ Returns the ground elevation values for the alignment, smoothened - average values for sequence of points with elevation difference less than 1.5 metres

        Parameters
        ----------
        self
            Alignment element

        Returns
        -------
        numpy.array
        """
        return np.array(self.smooth_gradient)
    
    def return_gradient_labels(self):
        """ Returns values of 0 & 1, where 1 indicates the position of the gradient value labels should be placed. For visualisation purposes

        Parameters
        ----------
        

        Returns
        -------
        numpy.array
        """
        return np.array(self.gradient_labels)

    # methods - geometry
    def frenet_frame_at(self, u):
        """ WIP

        Parameters
        ----------
        

        Returns
        -------
        WIP
        """
        return FrenetFrame(self._curve, u)


    def project(self, pt):
        """ Given an external 3d point, find the index of alignment point, which is the closest to it

        Parameters
        ----------
        pt : shapely.geometry.Point or array-like
            point

        Returns
        -------
        int
            index of the alignment control pt, closest to pt
        """
        if isinstance(pt, FrenetFrame):
            pt = Point(pt.o)
        elif not isinstance(pt, Point):
            try:
                pt = Point(pt)
            except Exception as e:
                print("ERROR: pt to project to alignment is not valid. \n}".format(e))
        pt_on_line = self.line.interpolate(self.line.project(pt))
        dists = distance.cdist(self, [pt_on_line])
        min_d = np.apply_along_axis(np.min, 0, dists)
        idx   = np.where(dists==min_d)[0][0]
        return idx


    def extend(self, dist, end="T1"):
        """ Extend the alignment by a given distance

        Parameters
        ----------
        dist : float
            must be positive

        end : str
            [optional] default='T1', end point to extend. T1 is the start and T2 the end point

        Returns
        -------
        None
        """
        if end=="T1":
            x = self.T1.x
            y = self.T1.y
            z = self.T1.z
            t = self.T1.i
            t *= -1. #reverse tangent vector
        elif end=="T2":
            x = self.T2.x
            y = self.T2.y
            z = self.T2.z
            t = self.T2.i
        steps = int(dist/self.granularity)
        pts = []
        for step in range(1,steps+1):
            # sum vector components
            pts.append(
                list(step*t + np.array([x,y,z]))
            )
        # extend linestring 
        if end=="T1":
            line = list(reversed(pts)) + list(self.points)
        elif end=="T2":
            line = list(self.points) + pts
        self._line = LineString(line)
        # recalc alignment interpolated curve 
        self.pt_length = None
        self._curve, self._control_pt = self._extract_curve()
        self._i = 0
        self.crv_length = self._recalc_length()
        # recalc speed function - note that speed points and values
        self._speed_curve = None
        self.calc_speed_curve()
        # reset metrics
        self.metrics = {METRICS[i]:None for i in range(len(METRICS))}
        # TMP - DANGER ZONE
        self._elevations = None
        return


    def check_proximity(self, a, tolerance=None):
        """ Check if the alignment is close to another one
        
        Parameters
        ----------
        a1 : Alignment
            first alignment object
        
        tolerance : float
            [optional] default=TOLERANCE_bng as per config.py (12.)
        
        Returns
        -------
        tuple<bool>
            (s_s, s_e, e_s, e_e, d) where s=start and e=end. sum(tuple)=1 as only one condition must be valid. d is the minimum distance

        """
        if not tolerance:
            tolerance = TOLERANCE_bng
        d1 = self.T1.distance_to(a.T1)
        d2 = self.T1.distance_to(a.T2)
        d3 = self.T2.distance_to(a.T1)
        d4 = self.T2.distance_to(a.T2)
        dists = sorted(zip([d1,d2,d3,d4],list(range(4))), key=lambda x:x[0])
        dist= dists[0][0]
        idx = dists[0][1]
        ends = [False]*4
        ends[idx] = True
        assert (dist<tolerance),"Alignments are not close enough"
        s_s,s_e,e_s,e_e = ends
        return (s_s,s_e,e_s,e_e,dist)


    def closest_u(self, pts):
        """ Find the closest point to the alignment

        Parameters
        ----------
        pts : array-like
            point cloud to search for
        
        Returns
        -------
        int
            index of the closest point in pts
        """
        if not isinstance(pts,type(np.array([]))):
            pts = np.array(pts)
        samples = np.array([self[i].o for i in range(len(self))])
        deltas = pts[:,np.newaxis]-samples
        mag = inner1d(deltas,deltas)
        ind = np.argmin(mag,axis=1)
        return ind


    # analyses at point along alignment
    def radius_at(self, u, tol=None, shift=1.0):
        """ 
        """
        assert(u>=0 and u<=1.0), "u must be within 0. and 1."
        if not tol:
            tol = self.granularity
        d = tol / self.crv_length
        A = np.array(interpolate.splev(u-d*shift, self._curve, der=0))
        B = np.array(interpolate.splev(u        , self._curve, der=0))
        C = np.array(interpolate.splev(u+d*shift, self._curve, der=0))
        R = circle_3pt(A,B,C)
        return R


    def curvature_at(self, u, tol=None, lim=1e4, shift=1.0):
        """ Returns the curvature values at a specified point of the Alignment
        
        Parameters
        ----------
        
        u : float
            takes values between 0 and 1, the position of the point on the Alignment, 0 is the first point and 1 the last
            
        tolerance : float
            [optional] default=TOLERANCE_bng as per config.py (12.)
            
        Returns
        -------
        
        float 
            curvature value in 1/m
        """
        assert(u>=0 and u<=1.0), "u must be within 0. and 1."
        R = self.radius_at(u, tol=tol, shift=shift)
        return 1./R if R<lim else 0.


    def permissible_speed_at(self, u,max_speed=60,tol=None,lim=1e4, shift=1.0):
        """Returns the permissible speed at a specified point of the Alignment
        
        Parameters
        ----------
        self
        
        u : float
            takes values between 0 and 1, the position of the point on the Alignment, 0 is the first point and 1 the last
            
            
        tolerance : float
            [optional] default=TOLERANCE_bng as per config.py (12.)
            
        max_speed : int 
            60 m/s (200 km/hr) which the maximum speed of the suggested type of train that will operate on the railway
        
        Returns
        -------
        
        x : float
            speed in m/s 
        """
        assert(u>=0 and u<=1.0), "u must be within 0. and 1."

        R = self.radius_at(u, tol=None, shift=1.0)
        x = speedOnDiffCant(R)


        if x>max_speed:

            x=max_speed

        return x


    # def angle_deviation_at(self,u,tol=None,lim=1e4):
    #     """ 
    #     """
    #     assert(u>=0 and u<=1.0), "u must be within 0. and 1."
    #     if not tol:
    #         tol = self.granularity
        
    #     A = np.array(interpolate.splev(u, self._curve, der=0))
    #     B = np.array(interpolate.splev(0  , self._curve, der=0))
    #     C = np.array(interpolate.splev(1, self._curve, der=0))
    #     a = angle_deviation_3pt(A,B,C)
    #     return a


    # def distance_deviation_at(self, u, tol=None, lim=1e4):
    #     """ 
    #     """
    #     assert(u>=0 and u<=1.0), "u must be within 0. and 1."
    #     if not tol:
    #         tol = self.granularity
        
    #     origin = np.array(interpolate.splev(0,self._curve,der=0))
    #     destination = np.array(interpolate.splev(1,self._curve,der=0))
    #     P = np.array(interpolate.splev(u,self._curve,der=0))

    #     distance = distance_deviation_3pt(origin, destination, P)
    #     return distance


    # def gradient_at(self, u, dtm_grid, tol=None, shift=1.0):
    #     """ 
    #     """
    #     assert(u>=0 and u<=1.0), "u must be within 0. and 1."
    #     if not tol:
    #         tol = self.granularity
    #     d = tol / self.crv_length
    #     A = np.array(interpolate.splev(u-d*shift, self._curve, der=0))
    #     B = np.array(interpolate.splev(u        , self._curve, der=0))
    #     C = np.array(interpolate.splev(u+d*shift, self._curve, der=0))
    #     e = [extract_elevation(pt, dtm_grid) for pt in [A,B,C]]
    #     e = ((e[0]*.5+e[1]*.5)*.5 + (e[1]*.5+e[2]*.5)*.5)/self.granularity
    #     return e


    # analyses througout alignment
    def curvature_analysis(self, n=3, shift=1.0):
        """ 
        """
        steps = self.crv_length / self.granularity
        t = 1.0 / steps
        c = []
        for u in self._control_pt:
            c.append(self.curvature_at(u, shift=shift))
        return moving_average(np.array(c), n=n)


    # def gradient_analysis(self, dtm_array, n=3, shift=1.0):
    #     """ 
    #     """
    #     steps = self.crv_length / self.granularity
    #     t = 1.0 / steps
    #     c = []
    #     for u in self._control_pt:
    #         c.append(self.gradient_at(u, dtm_array, shift=shift))
    #     c = np.nan_to_num(c)
    #     return moving_average(np.array(c), n=n)


    # def angle_deviation_analysis(self,n=3):
    #     """ 
    #     """
    #     steps = self.crv_length / self.granularity
    #     t = 1.0 / steps
    #     c = []
    #     for u in self._control_pt:
    #         c.append(self.angle_deviation_at(u))
    #     return np.array(c)


    # def distance_deviation_analysis(self,n=3):
    #     """ 
    #     """
    #     steps = self.crv_length / self.granularity
    #     t = 1.0 / steps
    #     c = []
    #     for u in self._control_pt:
    #         c.append(self.distance_deviation_at(u))
    #     return np.array(c)



    def permissible_speed_analysis(self):
        """ 
        """
        steps = self.crv_length / self.granularity
        t = 1.0 / steps
        c = []
        for u in self._control_pt:
            c.append(self.permissible_speed_at(u))
    
        return np.array(c)


    def cut_fill(self, grid_element):
        """ Cut and Fill analysis. Provides cut (negative) or fill (positive) distances

        Parameters
        ----------
        grid_element : 
            grid

        Returns
        -------
        nd-array
            representing the difference in elevation
        """
        alm_raster_coords = self.to_raster_coords(grid_element)

        raster_j,raster_i=zip(*alm_raster_coords)

        alignment_elevation=gt.return_cost(grid_element._A,2,raster_i,raster_j,ksize=1)  

        elevation=gt.fill_zeros_elevation(alignment_elevation)

        cut,fill,tunnels,bridges,ydif,alignment_heights,actual_heights=gt.get_cut_and_fills(len(raster_j),elevation)

        self._ground_elevation  = elevation
        self._alignment_heights = alignment_heights
        
        return np.array(ydif)


    def extract_ground_elevation(self):
        """
        """
        return np.array(self._ground_elevation)


    def extract_aligment_heights(self):
        """ 
        """
        return np.array(self._alignment_heights)


    def actual_speed_analysis(self):
        """ Calculate the actual speed curve for the alignment

        Parameters
        ----------
        self

        Returns
        -------
        tuple(LineString, array-like)
            (speed_geometry, speed_values) where speed_geometry represents the Linestrings with the constant-speed segments, speed_values represents the actual speed values
        """
        pms_speed = self.permissible_speed_analysis()

        if not isinstance(pms_speed,type(None)):
        # 1. split the array whenever the speed value is changing (https://stackoverflow.com/questions/31863083/python-split-numpy-array-based-on-values-in-the-array)
            split_points = np.split(pms_speed, np.where(np.diff(pms_speed[:]))[0]+1)
            clear = concatenate_arrays(split_points)
        # 2. get the segments and speeds 
            counter = 0
            segments = []
            speed_v = []

            coords = list(zip(self.curve[1][0],self.curve[1][1]))
            for i in range(len(clear)):
                
                if i == len(clear)-1:
                    line = LineString(coords[counter:counter + len(clear[i])])
                    segments.append(line)
                else:
                    #line segments
                    line = LineString(coords[counter:counter + len(clear[i]) + 1])
                    segments.append(line)
                #speed
                s = clear[i].mean()
                s = s - (s%5) 
                speed_v.append(s)  
                
                counter = counter +  len(clear[i])
                
                self._speed_geometry = segments
                self._speed_values = speed_v
            
            return self.speed_geometry, self.speed_values
        else:
            return None,None


    def headway_analysis(self):
        """ Calculate the actual speed per each point. Train type not available

        Returns
        -------
        nd-array
            speed value (m/s) per each point of the alignment
        """
        self.actual_speed_analysis()
        # if not (self.speed_segments & self.speed_values):
        #     return
        self.calc_speed_curve()

        if not self._speed_curve:
            return
        h = np.interp(x=np.arange(len(self)), xp=self._speed_curve[0], fp=self._speed_curve[1])
        return np.array(h)


    def run_metrics(self, metrics=METRICS, dtm_grid=None, reset=True):
        """ Main function to run metrics on the alignment. Values are stored in self.metrics

        Parameters
        ----------
        metrics : list<str>
            [optional] default as per raidnr.config.py file. It is not recommended to change it. If necessary, a subset of such list can be passed
        
        dtm_grid : None or raidnr.bwgrid.grid.Grid
            [optional] default=None. Add a grid to access context information for specific analyses (e.g. gradient)
        
        reset : bool
            [optional] default=True. Delete previous run analyses
        
        Returns
        -------
        None
        """
        if reset:
            del self.metrics
            self.metrics = {METRICS[i]:None for i in range(len(METRICS))}
        
        if METRICS[0] in metrics:
            print('Running {}...'.format(METRICS[0]))
            self.metrics[METRICS[0]] = self.curvature_analysis(shift=1.)
        
        if METRICS[1] in metrics:
            print ('Running {}...'.format(METRICS[1]))
            self.metrics[METRICS[1]] = self.permissible_speed_analysis()
        
        if METRICS[2] in metrics:
            print('Running {}...'.format(METRICS[2]))
            self.metrics[METRICS[2]] = self.headway_analysis()

        if METRICS[3] in metrics:
            print ('Running {}...'.format(METRICS[3]))
            self.metrics[METRICS[3]] = self.cut_fill(dtm_grid)

        if METRICS[4] in metrics:
            print ('Running {}...'.format(METRICS[4]))
            self.metrics[METRICS[4]] = self.extract_ground_elevation()

        if METRICS[5] in metrics:
            print ('Running {}...'.format(METRICS[5]))
            self.metrics[METRICS[5]] = self.extract_aligment_heights()

        if METRICS[6] in metrics:
            print ('Running {}...'.format(METRICS[6]))
            self.metrics[METRICS[6]] = self.extract_slope()

        if METRICS[7] in metrics:
            print ('Running {}...'.format(METRICS[7]))
            self.metrics[METRICS[7]] = self.return_gradient_pct()

        if METRICS[8] in metrics:
            print ('Running {}...'.format(METRICS[8]))
            self.metrics[METRICS[8]] = self.return_smooth_gradient()

        if METRICS[9] in metrics:
            print ('Running {}...'.format(METRICS[9]))
            self.metrics[METRICS[9]] = self.return_gradient_labels()


    # methods - IO
    def to_coords_wgs(self):
        """ 
        """
        lons=[]
        lats=[]
        
        for i in range(len(self)):

            # lon_bng,lat_bng = gt.raster_to_coords(bbox,raster_coords=(self[i].o[0],self[i].o[1]),pixel_size=self.granularity)

            lon_wgs,lat_wgs=st.bng_to_wgs((self[i].o[0],self[i].o[1]))
            
            lons.append(lon_wgs)
            lats.append(lat_wgs)

        return lons,lats


    def to_coords_bng(self):
        """ 
        """
        lons=[]
        lats=[]
        
        for i in range(len(self)):
            
            # lon_bng,lat_bng = gt.raster_to_coords(bbox,raster_coords=(self[i].o[0],self[i].o[1]),pixel_size=self.granularity)
            
            lons.append(self[i].o[0])
            lats.append(self[i].o[1])
        
        return lons,lats     

    def to_coords(self,fileName="",crs="wgs",geoDataFrame=True,save=""):
        """
        """
        if(crs=="wgs"):
            print("default to epsg:4326")

            lons,lats= self.to_coords_wgs()
        
        elif(crs=="bng"):
            lons,lats= self.to_coords_bng()
        else:

            print("given crs not supported")
            return 

            
        if(geoDataFrame):

            gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(LineString(zip(lons,lats))))
            gdf.crs = {'init' :'epsg:4326'}


            if(save=="shp"):
                gdf.to_file(fileName, driver='ESRI Shapefile')
            elif(save=="json"):
                gdf.to_file(fileName+".json", driver='GeoJSON')
            else:
                print("save driver is not supported")
                return

            return gdf

        else:
            
            return list(zip(lons,lats))


    def to_json_str(self, **kwargs):
        """ Serialise to JSON-encoded string
        
        Parameters
        ----------
        None except self

        Returns
        -------
        None
        """
        docs = []
        extend = kwargs.get('extend', {})
        
        cost_dict = kwargs.get('cost_dict')
        for i in range(len(self)):
            vals = {}
            for metric in METRICS:
                try:
                    x = self.metrics[metric][i]
                    val = x if not np.isnan(x) else 0
                except:
                    val = 0
                vals[metric]=val
            
            land_uses_vals={}
            for layer in TOPO_LAYERS:
                try:
                    land_uses_vals[layer]= cost_dict[layer][i]
                except:
                    land_uses_vals[layer] = 0
                    
            land_uses_proximity = {"land_uses_proximity":land_uses_vals}    

            # lon_bng,lat_bng = gt.raster_to_coords(extend['bbox'],raster_coords=(self[i].o[0],self[i].o[1]),pixel_size=self.granularity)

            lon,lat=st.bng_to_wgs((self[i].o[0],self[i].o[1]))
            d = {
                    "X_coord":self[i].o[0],
                    "Y_coord":self[i].o[1],
                    # "Z_coord":self[i].o[2],
                    "Z_coord":self._ground_elevation[i],
                    "Latitude":lat,
                    "Longitude":lon,
                    "Index":i,
                }

            
            d.update(vals) # add metrics to dict
            d.update(extend) # add any extra item
            d.update(land_uses_proximity)
            docs.append(d)
        return json.dumps(docs)


    def to_file(self, fn):
        """ Save alignment as shapefile

        Parameters
        ----------
        fn : str
            file path
        """
        gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(LineString(list(self))))
        gdf.crs = CRS
        gdf.to_file(fn)


    def to_raster_coords_Alignment(self, grid, to_int=False):
        """ Convert the alignment from BNG to raster coordiantes

        Parameters
        ----------
        grid : raidnr.bwgrid.grid.Grid object
            grid where the alignment stands on

        to_int : bool
            [optional] default=False. To round the raster coordinates as [i,j]

        Retruns
        -------
        raidnr.core.alignment.Alignment
            alignment converted to raster coordinates
        """
        raster_coords=[]
        for c in self._line.coords:
            try:
                raster_coords.append(grid.to_raster(c, to_int=to_int))
            except:
                continue

        newAlignment = Alignment(LineString(raster_coords),self.ID,self.granularity)

        return newAlignment


    def to_raster_coords(self, grid, to_int=True):
        """ Convert the alignment points from BNG to raster coordiantes

        Parameters
        ----------
        grid : raidnr.bwgrid.grid.Grid object
            grid where the alignment stands on

        to_int : bool
            [optional] default=False. To round the raster coordinates as [i,j]
        
        Retruns
        -------
        list<list<float>>
            points as [[i1,j1],[i2,j2],...] in raster coordinates
        """
        raster_coords=[]
        for c in self.points:
            try:
                raster_coords.append(grid.to_raster(c, to_int=to_int))
            except:
                continue

        return raster_coords


    def plot(self, ax=None, figsize=None, fn=None):
        """ Plot alignment
        
        Parameters
        ----------
        ax : matplotlib.pyplot.axis
            [optional] to merge the alignment plot to another plot
        
        figsize : tuple
            [optional] (i,j) dimensions. If ax is passed, figsize is skipped
        
        Returns
        -------
        None
        """
        if ax == None:
            flag = True
            figsize = figsize if figsize else (10,10)
            fig, ax = plt.subplots(1, 1, figsize=figsize)
        else:
            flag = False
        pts  = np.array(self._line.coords).transpose()
        ax.plot(pts[0],pts[1])
        if flag and fn:
            plt.savefig(fn)


    def plot_metrics(self, fn=None, dim=6., axs=None, figsize=None):
        """ Plot alignment metrics

        Parameters
        ----------
        dim : float
            [optional] height of one metric plot
        
        axs : list<matplotlib.pyplot.axis>
            [optional] subplots, if required
        
        figsize : tuple
            [optional] (i,j) dimensions. If ax is passed, figsize is skipped
        
        Returns
        -------
        None
        """
        measured_metrics = [k for k,v in self.metrics.items() if v is not None]

        if axs == None:
            flag = True
            figsize = figsize if figsize else (10,10)
            fig, axs = plt.subplots(len(measured_metrics), 1, figsize=(dim*2,(dim+1)*len(measured_metrics)))
            
        else:
            flag = False
        
        assert len(axs)==len(measured_metrics), "axs length must equal the metrics. Got {}, expected {}".format(len(axs), len(measured_metrics))
        
        if isinstance(fn,str):
            case=True
        else:
            case=False
        
        for i,(ax,m) in enumerate(zip(axs,measured_metrics)):
            try:
                x = np.arange(0,len(self.metrics[m])*GRANULARITY,GRANULARITY)
                ax.plot(x,self.metrics[m])
                ax.title.set_text(m)
                if case:
                    fig.tight_layout()
                    extent=ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                    fig.savefig(os.path.join(fn, m + '.png'), bbox_inches=extent)
            except Exception as e:
                print(e)
                print("Metric {} for this alignment are not available or contain an error".format(m))
        plt.suptitle('METRICS')



    # alternative constructors
    @staticmethod
    def from_shp_poly(fn, id, resample_dist, granularity, speed_geometry=None, speed_values=None):
        """[Static] Create a list of alignments from a polyline shapefile
        """
        poly = purge_poly(fn)['geometry'][0]
        poly = resample_poly(poly, resample_dist)
        rebuilt = reconstruct_poly(poly, granularity)
        a = Alignment(LineString(rebuilt), id, granularity, speed_geometry=speed_geometry, speed_values=speed_values)
        return a


    @staticmethod
    def from_points_on_grid(pts, bbox, id, resample_dist, granularity, speed_geometry=None, speed_values=None):
        """ [Static] Return an alignment from a grid-referenced alignment. This is to re-reference new alingment from raster indices to BGN

        Parameters
        ----------
        pts: nd-array of points as [[X,Y],...]
        """
        pts = [bbox.idx_to_coords(x,y) for x,y in pts]
        a = Alignment(LineString(pts), id, granularity, speed_geometry=speed_geometry, speed_values=speed_values)


    @staticmethod
    def fillet(a1, a2, radius, tolerance=None):
        """ [Static] Fillet two alignments with a circle

        Parameters
        ----------
        a1 : Alignment
            first alignment object
        
        a2 : Alignment
            second alignment object
        
        radius : float
            radius of the fillet circle
        
        tolerance : float
            [optional] default=6. - tolerance to test alignment proximity

        Returns
        -------
        tuple
            a1, circle, a2
        """
        if not tolerance:
            tolerance = TOLERANCE_bng
        s_s = a1.T1.distance_to(a2.T1) < tolerance
        s_e = a1.T1.distance_to(a2.T2) < tolerance
        e_s = a1.T2.distance_to(a2.T1) < tolerance
        e_e = a1.T2.distance_to(a2.T2) < tolerance
        check_sum=int(sum([s_s,s_e,e_s,e_e]))
        assert(check_sum==1),"Alignments are not close enough, or one"
        # centre of circle
        c = None
        if s_s:
            c = a1.T1.mid_point(a2.T1)
        elif s_e:
            c = a1.T1.mid_point(a2.T2)
        elif e_s:
            c = a1.T2.mid_point(a2.T1)
        elif e_e:
            c = a1.T2.mid_point(a2.T2)
        # create trimming circle
        pitch = min(a1.granularity,a2.granularity)*.5
        lngth = 2. * np.pi * radius
        print(lngth/pitch)
        theta = np.linspace(0, 2*np.pi, 100) # change 100 to lngth/pitch
        cx = np.array([math.cos(t)*radius + c[0] for t in theta])
        cy = np.array([math.sin(t)*radius + c[1] for t in theta])
        # intersect (triming circle on a1 and a2)
        ax_1,ay_1,_ = np.array(list(a1.points)).transpose()
        tx_1,ty_1 = em.intersection(cx,cy,ax_1,ay_1)
        tx_1,ty_1 = tx_1[0],ty_1[0]
        ax_2,ay_2,_ = np.array(list(a2.points)).transpose()
        tx_2,ty_2 = em.intersection(cx,cy,ax_2,ay_2)
        tx_2,ty_2 = tx_2[0],ty_2[0]
        # find closest points on alignment and tangents
        # i1 = int(a1.line.project(Point([tx_1,ty_1]))) # NOT WORKING - alignment.line seems not deepcopied
        # i2 = int(a2.line.project(Point([tx_2,ty_2]))) # NOT WORKING - alignment.line seems not deepcopied
        i1 = a1.closest_u(np.array([[tx_1,ty_1,0.]]))[0]
        i2 = a2.closest_u(np.array([[tx_2,ty_2,0.]]))[0]
        if s_s or s_e: # check which portion to keep
            # a1 = a1.extract_subset(0,i1)
            a1 = a1.extract_subset(i1,len(a1)-1)
            trim_1 = a1.T1 # trim1 at start
        else:
            # a1 = a1.extract_subset(i1,len(a1)-1)
            a1 = a1.extract_subset(0,i1)
            trim_1 = a1.T2
        if s_s or e_s:
            a2 = a2.extract_subset(i2,len(a2)-1) #TODO: check if should be len-1 or len-2
            trim_2 = a2.T1 # trim2 at start
        else:
            a2 = a2.extract_subset(0,i2)
            trim_2 = a2.T2
        # intersect tangents
        itr = FrenetFrame.intersect_tangents(trim_1,trim_2)
        # measure distance trims to tg_intersection
        t_i_1 = trim_1.distance_to(itr)
        t_i_2 = trim_2.distance_to(itr)
        case = t_i_1 > t_i_2
        d = abs(t_i_1 - t_i_2)
        # extend the furthest trim to be as close as the closest trim to tg_intersect (equal) => extend the alignment
        if case:
            if s_s or s_e:
                a1.extend(d, end="T1")
            else:
                a1.extend(d, end="T2")
        else:
            if s_s or e_s:
                a2.extend(d, end="T1")
            else:
                a2.extend(d, end="T2")
        # calculate the circle for 2 pts, 2 tgs
        # use p1, p2, p3 as (align_1, pt2, align_2) where pt2 is the intersection by the tangents at pt1, pt3
        if s_s:
            p1 = a1.T1.o
            t1 = a1.T1.i
            p3 = a2.T1.o
            t3 = a2.T1.i
            p2 = em.line_intersection((p1,p1+t1),(p3,p3+t3))
            p2 = np.array(list(p2)+[0.])
        elif s_e:
            p1 = a1.T1.o
            t1 = a1.T1.i
            p3 = a2.T2.o
            t3 = a2.T2.i
            p2 = em.line_intersection((p1,p1+t1),(p3,p3+t3))
            p2 = np.array(list(p2)+[0.])
        elif e_s:
            p1 = a1.T2.o
            t1 = a1.T2.i
            p3 = a2.T1.o
            t3 = a2.T1.i
            p2 = em.line_intersection((p1,p1+t1),(p3,p3+t3))
            p2 = np.array(list(p2)+[0.])
        elif e_e:
            p1 = a1.T2.o
            t1 = a1.T2.i
            p3 = a2.T2.o
            t3 = a2.T2.i
            p2 = em.line_intersection((p1,p1+t1),(p3,p3+t3))
            p2 = np.array(list(p2)+[0.])
        # produce fillet curve points
        c_pts = em.apply_circle(p1,p2,p3)
        circle = Alignment(LineString(c_pts), str(a1.ID), a1.granularity, a1.speed_geometry, a1.speed_values, a1.resampling)
        if DEBUG:
            plt.scatter(
                np.array(a1.points).transpose()[0],
                np.array(a1.points).transpose()[1],
                c='blue',
                linewidth=1.)
            plt.scatter(
                np.array(a2.points).transpose()[0],
                np.array(a2.points).transpose()[1],
                c='red',
                linewidth=1.)
            plt.scatter(
                np.array(circle.points).transpose()[0],
                np.array(circle.points).transpose()[1],
                c='green',
                linewidth=1.)
            plt.plot(cx,cy)
            plt.show()
        a = a1 + circle
        a = a + a2
        return a

