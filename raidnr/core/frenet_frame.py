# ---------------------------------------------------
# ¦¦¦¦¦¦¦    ¦¦¦¦¦   ¦¦¦¦  ¦¦¦¦¦¦¦¦  
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦¦¦¦¦   ¦¦¦¦¦¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦  
# ¦¦¦   ¦¦¦ ¦¦   ¦¦¦ ¦¦¦¦  ¦¦¦¦¦¦¦¦    
# ---------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Claudio Campanile
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Frenet Frame class to model parametrically a plane perpendicular to a curve
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------

# modules
from copy import copy
# scientific modules
import numpy as np
from scipy import interpolate
from sympy import Plane
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# geo modules
from shapely.geometry import LineString

# import raidnr
from raidnr.config import CRS
import raidnr.util.ext_methods as em


class FrenetFrame(object):

    def __init__(self, crv, u):
        """
        Frenet Frame class, referenced to a curve
        
        Parameters
        ----------
        crv : scipy.interpolate
            scipy curve
        
        u : float
            parameter on curve
        """
        self._u = u
        self._curve = crv

    @property
    def o(self):
        return self._eval(0, norm=False)
    @property
    def x(self):
        return self.o[0]
    @property
    def y(self):
        return self.o[1]
    @property
    def z(self):
        return self.o[2]
    @property
    def i(self):
        return self._eval(1)
    @property
    def j(self):
        return  self._eval(2)
    @property
    def k(self):
        v = np.cross(self.i, self.j)
        return v  / np.linalg.norm(v)
    @property
    def base(self):
        return [self.i, self.j, self.k]

    def _eval(self, der, _3d=True, norm=True):
        """ Private: evaluate B-Spline at param u, of derivative n

        Parameters
        ----------
        der: int
            derivative
        
        _3d: bool
            True if you are in 3d space

        norm: bool
            True to normalize the output. Turn to False for derivative=0 (retrieve origin origin)
        
        Return
        ------
        nd-array
            vector representing the evaluated curve where the Frenet Frame stands
        """
        v = interpolate.splev(self._u, self._curve, der=der); v = np.array(v).squeeze()
        if (len(v)==2 and _3d): v = np.append(v,np.array([0.]))
        if norm: v /= np.linalg.norm(v)
        return v


    def distance_to(self, ff):
        """ Calculate distance to another frenet frame

        Parameters
        ----------
        ff : FrenetFrame or array

        Returns
        -------
        float
            distance of self to ff
        """
        if isinstance(ff, FrenetFrame):
            pt = ff.o
        elif len(ff) is 2:
            pt = np.array(list(ff) + [0.])
        elif len(ff) is 3:
            pt = list(ff)
        else:
            raise ValueError("ff must be a FrenetFrame object or an iterable of len 2 or 3, got {}".format(ff))
        
        return np.linalg.norm(self.o - pt)


    def mid_point(self, ff):
        """ Calculate the midpoint between self and another frenet frame or point
        
        Parameters
        ----------
        ff : FrenetFrame or array

        Returns
        -------
        array
            [x,y,z] point
        """
        assert(isinstance(ff, FrenetFrame)), "ff must be a FrenetFrame object, got {}".format(type(ff))
        mid = copy((self.o + ff.o)/2)
        return mid


    def frame_triplet(self):
        """ Returns a (v1,v2,v3) of lines starting at the origin, of dimension 1.0

        Returns
        array
            three vectors as [tan ,norm, binorm]
        """
        v1 = em.tangent_vector(self.o, self.i)
        v2 = em.tangent_vector(self.o, self.j)
        v3 = em.tangent_vector(self.o, self.k)
        return np.array([v1,v2,v3])


    def display(self, ax=None, **kwargs):
        """ Display the frenet frame

        Parameters
        ----------
        ax : matplotlib axis or None
            [optional] axis to plot the frenet frame on
        """
        if ax == None:
            fig = plt.figure()
            ax = fig.gca(projection='3d')
        ff = self.frame_triplet()
        ff = ff.transpose() # shape to (start,final) coordinates
        start, end = ff
        x, y, z = [self.o[0]]*3, [self.o[1]]*3, [self.o[2]]*3
        u, v, w = end[0]-start[0], end[1]-start[1], end[2]-start[2]
        color=['red','green','blue']
        c = em.renderColorsForQuiver3d(color)
        ax.quiver(x, y, z, u, v, w, length=1.0, normalize=True, color=c)
        if ax == None:
            plt.show()


    @staticmethod
    def intersect_tangents(ff1, ff2):
        """ Intersect two tangent (i) vectors. 
        NOTE: Works only in 2D

        Parameters
        ----------
        ff1 : FrenetFrame
            first point
        
        ff2 : FrenetFrame
            second point

        Returns
        -------
        tuple
            intersection point as (x,y)
        """
        itr = em.line_intersection((ff1.o, ff1.o+ff1.i),(ff2.o, ff2.o+ff2.i))
        return itr


class Point3D(object):

    def __init__(self, v):
        """ NOT CURRENTLY USED
        """
        self.x = v[0]
        self.y = v[1]
        self.z = v[2]

    def __getitem__(self,key):
        if key==0:
            return self.x
        if key==1:
            return self.y
        if key==2:
            return self.z