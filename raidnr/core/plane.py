"""

"""

# scientific modules
import numpy as np
from sympy import Plane
# modules
from shapely.geometry import LineString

# import raidnr
from raidnr.config import CRS
from raidnr.util.ext_methods import tangent_vector

class Plane(Plane):

    def __init__(self, o, i, j, k, **kwargs):
        """
        Plane class
        
        Parameters
        ----------
        o : ()
        """

        super().__init__(o, a=i, b=j, **kwargs)
        
        b = np.array([i,j,k])
        b = b / np.linalg.norm(b)
        self._base = b

    @property
    def base(self):
        return self._base
    @base.setter
    def base(self, base):
        raise ValueError("base is a read only attribute")

    @property
    def i(self):
        return self.base[0]
    @property
    def j(self):
        return self.base[1]
    @property
    def k(self):
        return self.base[2]

    def frame_triplet(self):
        """ Returns a (v1,v2,v3) of lines starting at the origin, of dimension 1.0
        """
        v1 = tangent_vector(self.p1, self.i)
        v2 = tangent_vector(self.p1, self.j)
        v3 = tangent_vector(self.p1, self.k)
        return np.array([v1,v2,v3])
    