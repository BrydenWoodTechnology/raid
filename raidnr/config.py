"""
Configuration file for raidnr globals - variables must be upper case

"""

#system config
import sys
import os
import platform
import base64 as bs
import inspect
# Python version
try:
    PY_IMPLEMENTATION = platform.python_implementation()
except:
    PY_IMPLEMENTATION = 'IronPython'

PYTHON_3 = bool(sys.version_info[0] > 2)
IRONPYTHON = PY_IMPLEMENTATION == 'IronPython'

# debug
DEBUG = False
data_path = r''

# HyperParameters
TOLERANCE_bng = 12.
TOLERANCE_ij  = 4.

# DATA DEFAULT FOLDERS

cur_dir = inspect.getfile(inspect.currentframe())

modulePath=os.path.abspath(os.path.join(cur_dir,os.path.pardir,os.path.pardir))

flask_dir = os.path.join(modulePath,"flask_UI")

# input Directories
inputPath = os.path.join(flask_dir,"InputData")
# -----Shp
inputPathShp = os.path.join(inputPath,"shp")


# output Directories
# ----shp
outputPath = os.path.join(flask_dir,"OutputFiles")
outputPathShp = os.path.join(outputPath,"shp")
# ----json
outputPathJSON = os.path.join(outputPath,"json")


# Images Directories
staticPath = os.path.join(flask_dir,"static")
imagePath = os.path.join(staticPath,"images","Sessions")
# ----plots
PATH_IMG = os.path.join(imagePath,"plots")
# ---- tif
PATH_RASTER = os.path.join(imagePath,"raster")
# --- tiles 
PATH_TILE = os.path.join(staticPath,"test_tile")


# input Directories
PATH_OS = os.path.join(inputPathShp,"OS","data")
PATH_DTM = os.path.join(inputPath,"DTM")
PATH_SPEED = os.path.join(inputPathShp,"Speed")
PATH_RAILWAY = os.path.join(inputPathShp,"Railway_Lines")


# Specific shp paths for each session
PATH_BBOX = os.path.join(outputPathShp,"bbox")
PATH_BBOX_OFF = os.path.join(outputPathShp,"bbox_offset")
PATH_CLIP_CONTEXT = os.path.join(outputPathShp,"clipped_context")
PATH_CLIP_CONTEXT_BUFF = os.path.join(outputPathShp,"clipped_context_buff")
PATH_CLIP_SPEED = os.path.join(outputPathShp,"clipped_speed")
PATH_CLIP_ALIGNMENT = os.path.join(outputPathShp,"clipped_alignment")



# buffer config
FNAME_BUFFER = os.path.join(inputPath,"csv","Buffers.csv")


# for i in neededPaths:
#     if(os.path.exists(i)==False):
#         os.makedirs(i)
#     os.makedirs(i)

# curvature smoothing
crv_thresholds = {0: [1.0, 0.0033333333333333335], 
                  1: [0.0033333333333333335, 0.002], 
                  2: [0.002, 0.001], 
                  3: [0.001, 0.0002], 
                  4: [0.0002, 0.000125], 
                  5: [0.000125, 0.0] 
                  }





#RAID Global parameter
#define the CRS for all spatial data - British National Grid
CRS={'proj': 'tmerc',
 'lat_0': 49,
 'lon_0': -2,
 'k': 0.9996012717,
 'x_0': 400000,
 'y_0': -100000,
 'datum': 'OSGB36',
 'units': 'm',
 'no_defs': True}

GRANULARITY = 3.0

METRICS = [

    "curvature",
    "permissible_speed",
    "actual_speed",
    "elevation_difference",
    "ground_elevation",
    "alignment_elevation",
    "gradient_ratio",
    "gradient_pct",
    "gradient_smooth",
    "gradient_labels"
]



METRICS_dict = [

    "curvature",
    "gradient",
    "permissible_speed_analysis",
    "actual_speed_analysis",
    "headway",
    "elevation_difference",
    "ground_elevation",
    "alignment_elevation"
]

TOPO_LAYERS = ['Building',
               'CarChargingPoint',
               'DTM',
               'ElectricityTransmissionLine',
               'Foreshore',
               'FunctionalSite',
               'Glasshouse',
               'Greenspaces',
               'ImportantBuilding',
               'MotorwayJunction',
               'NationalParks',
               'RailwayStation',
               'RailwayTrack',
               'RailwayTunnel',
               'Road',
               'RoadTunnel',
               'Roundabout',
               'Slope',
               'SurfaceWater_Area',
               'SurfaceWater_Line',
               'TidalBoundary',
               'TidalWater',
               'Woodland']

# global variables

# acceleration curve 
acceleration_x = [0,17.8816,20.1168,22.352,24.587,26.822,60]
acceleration_y = [0.49,0.240,0.195,0.156,0.126,0.120,0.05]


