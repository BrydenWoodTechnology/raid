
# ---------------------------------------------------
# ¦¦¦¦¦¦¦    ¦¦¦¦¦   ¦¦¦¦  ¦¦¦¦¦¦¦¦  
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦¦¦¦¦   ¦¦¦¦¦¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦  
# ¦¦¦   ¦¦¦ ¦¦   ¦¦¦ ¦¦¦¦  ¦¦¦¦¦¦¦¦    
# ---------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Claudio Campanile
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Grid class to model infrustructure contexts
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------

# modules
import os
from collections import OrderedDict
import json
import uuid
# scientific modules
import numpy as np
from numpy.core.umath_tests import inner1d
from scipy import interpolate
from scipy.spatial import distance
import skimage.draw as skd
# geo modules
import geopandas as gpd
from shapely.geometry import LineString
# plot
import matplotlib.pyplot as plt

# import raidnr
from raidnr.config import CRS, GRANULARITY, METRICS, TOPO_LAYERS, DEBUG
import raidnr.bwgrid.grid_transformations as gt
import raidnr.util.SpatialToolbox as st
from raidnr.util.least_cost_path import get_least_cost
from raidnr.util.ext_methods import resample_poly, bezier, points_from_spline, smoothen_poly
from raidnr.core.frenet_frame import FrenetFrame
from raidnr.core.alignment import Alignment
from raidnr.core.speed_curve import find_speed_curve

from scipy import ndimage

from multiprocessing import current_process
try:
    current_process()._config
except AttributeError:
    current_process()._config = {'semprefix': '/mp'}


class Grid(object):

    def __init__(self, a, bbox, granularity):
        """
        Grid class to model a context

        Parameters
        ----------
        a: nd-array
            array containing context data
        
        bbox: array-like
            (x0,x1,y0,y1)
        
        granularity: float
            grid granularity
        """
        assert(a.dtype=='float64')
        # basics
        self._A = a
        self._shape = (a.shape[0],a.shape[1]) #(height, width)
        self._bbox = bbox 
        self._granularity = granularity
        # extra layers
        self._fitness_layer = None
        self._naa_layer = None
        # alignment
        self._a = None # alignment object
        self._a_idx = None # [i,j] points
        self._a_raster = None # as an array of self.shape
        self._a_ID = None # keep track of the ID in case an alignment is regenrated
        # costs
        self._surroundings = None
        self._k_size = None
        self._layer_names = TOPO_LAYERS
        # maybe this can go to alignment # IT to clarify
        self._least_cost_path_smoothed = []
        self._least_cost_path_simplified = []


    @property
    def metrics(self):
        return self._a.metrics
    @property
    def shape(self):
        return self._shape
    @property
    def bbox(self):
        return self._bbox 
    @property
    def granularity(self):
        return self._granularity
    @property
    def fitness(self):
        return self._fitness_layer
    @property
    def new_alignment(self):
        return self._a
    @property
    def new_alingment_ij(self):
        return self._a_idx
    @property
    def new_alignment_rr(self):
        return self._a_raster
    @property
    def new_alignment_ID(self):
        return self._a_ID
    @property
    def surroundings(self):
        return self._surroundings
    @property
    def layers(self):
        return self._layer_names


    def __getitem__(self, key):
        if isinstance(key,int):
            try:
                return self._A[...,key]
            except:
                raise IndexError("Index out of bounds. Maximum: {}".format(len(TOPO_LAYERS)-1))
        elif isinstance(key,str):
            try:
                i = TOPO_LAYERS.index(key)
                return self._A[...,i]
            except:
                raise KeyError("layer name not in {}".format(TOPO_LAYERS))

    def __str__(self):
        return "Grid object at {}:\n  Bounds: {}\n  Granularity: {} m".format(str(id(self)), str(self.bbox), str(self.granularity))

    # COORDS conversion
    def to_raster(self, coords, to_int=True):
        """ Transfrom coordinates to raster row,col coords. 

        Parameters
        ----------
        bbox : array
            minx,miny,maxx,maxy
        
        coords : tuple
            In format (northing, easting)
        """
        try:
            assert self._is_bng(coords), "Coordinates out of the Grid. {} not within {} as [minx,maxx,miny,maxy]".format(coords, self._bbox)
            return gt.coords_geo_to_raster(self._bbox, coords, pixels_size=self._granularity, to_int=to_int)
        except Exception as e:

            # TODO here we need to check if the coords fall outside of the bbox and if negative to
            # match them with the closer points to the boundaries

            # gt.coords_geo_to_raster(self._bbox, coords, pixels_size=self._granularity, to_int=to_int)
            print("ERROR: {}".format(e))
            return (0,0)


    def to_coords(self, indices):
        """ Transfrom coordinates from raster to coords. 
        
        Parameters
        ----------
        bbox : array
            minx,miny,maxx,maxy
            
        indices : array-like
            In format (row, col)
        
        Returns
        -------
        tuple
            In format (x, y)
        """
        if self._is_raster(indices): 
            return gt.raster_to_coords(self._bbox, indices, pixel_size=self._granularity)
        else:
            print(indices)
            print("Raster coordinates out of bounds - shape: {}".format(self._shape))
            return (0,0)

        return gt.raster_to_coords(self._bbox, indices, pixel_size=self._granularity)


    def _is_bng(self, coords):
        """ Check if a geo coordinate is within the bounding box

        Parameters
        ----------
        coords : tuple
            (x,y)

        Returns
        -------
        bool
            True if coords are within the bbox boundaries, else False
        """
        return (coords[0] >= self._bbox[0] and coords[0] <= self._bbox[2] and coords[1] >= self._bbox[1] and coords[1] <= self._bbox[3])


    def _is_raster(self, indices):
        """ Check if a raster coordinate is within the grid bounds

        Parameters
        ----------
        indices : tuple
            (i,j)

        Returns
        -------
        bool
            True if coords are within the raster boundaries, else False
        """
        return (indices[0] >= 0 and indices[0] <= self._shape[0] and indices[1] >= 0 and indices[1] <= self._shape[1])


    def process_coords(self, coords, to='raster'):
        """ Transform coordinates between raster and BNG. If the coordinate is already in the reference system, no transformation is applied

        Parameters
        ----------
        coords : tuple
            In format (northing, easting) or (row, col)
        
        to : str
            [optional] 'raster' or 'bng' destination reference system (default='raster')
        
        Returns
        -------
        coords : tuple
            In format (northing, easting) or (row, col)
        """
        if to=='raster' and self._is_bng(coords):
            return self.to_raster(coords)
        elif to=='raster' and self._is_raster(coords):
            return coords
        elif to=='bng' and self._is_raster(coords):
            return self.to_coords(coords)
        elif to=='bng' and self._is_bng(coords):
            return coords
        else:
            raise ValueError("coordinates not valid")


    # VALUES + COST
    def compute_fitness_layer(self, weights, origin=None, destination=None, gmax=1.35, alignment=None, naa=None, naa_w=100.):
        """ Compute grid fitness layer by given weights and store it into self.fitness

        Parameters
        ----------
        weights : dict
            dictionary with weights for each topo layer
        
        origin : int
            [optional] origin on the raster, required if naa='circuity'
        
        destination : int
            [optional] destination on the raster, required if naa='circuity'
        
        gmax : float
            [optional] default=1.35
        
        alignment : Alignment or None
            [optional] default=None, existing alignment to calculate the distance from; required if naa='alignment'

        naa : str or None
            'circuity' or 'alignment' to bind the path to a specific area. None would produce no NAA.
        
        naa_w : float
            [optional] default=100. represent the weight to apply as NAA, used if naa='circuity'
        
        Returns
        -------
        None
        """
        # collect weights
        ws = []
        for _,tl in enumerate(TOPO_LAYERS):
            ws.append(weights.get(tl,1.0)) #if not present, => 0.0
        
        # expand dimension of weight matrix to match terrain
        for i in range(len(self._A.shape)-1):
            ws = np.expand_dims(ws,axis=1)
        
        # Add NAA array
        if naa == 'circuity' and origin and destination:
            origin = self.process_coords(origin, to='raster')
            destination = self.process_coords(destination, to='raster')
            naa_layer = st.circuity_area(self.shape, origin, destination, gmax, fill_value=naa_w)
        elif naa == 'alignment' and alignment:
            # aligment_raster_coords = alignment.to_raster_coords(self) # <= pass grid
            a = np.zeros(self.shape)
            a += np.inf
            if isinstance(alignment, Alignment):
                alignment = alignment.points2D
            for i,j in alignment:
                try:
                    a[int(i),int(j)]=0.
                except:
                    print("wrong coordinates", i,j)
                    continue
            # compute distance from alignment
            a[0,0] = np.inf
            naa_layer = ndimage.distance_transform_edt(a)
        else:
            if naa=='circuity':
                print("Circuity not calculated. check start and end")
            elif naa=='alignment':
                print("Got a {} for existing alignment, proximity to exisitng alignment not calculated".format(alignment))
            naa_layer = np.zeros(shape=self._shape, dtype='float64')
        print("NAA min-max:",naa_layer.min(), naa_layer.max())

        self._naa_layer = naa_layer

        # calc FITNESS and add NAA layer
        fitness = np.sum(self._A.transpose(2,0,1)*ws, axis=0, dtype=np.float64).squeeze() 
        fitness = fitness + 1.
        fitness = fitness + naa_layer
        self._fitness_layer = fitness


    # LEAST COST PATH
    def least_cost_path(self, origin, destination, weights, naa=None, naa_w=None, gmax=1.35, alignment=None, plot=False):
        """ Calculate the least cost path given an origin and destination. Uses route_through_array from skimage.graph
        
        Parameters
        ----------
        origin : int
            origin on the raster
        
        destination : int
            destination on the raster
        
        weights : dict
            dictionary with weights for each topo layer
        
        naa : str
            'circuity' or 'alignment' to bind the path to a specific area

        plot : bool
            True to plot the path on the raster
        
        Returns
        -------
        nd-array 
            path as [(i1,j1), ...]
        """
        # fitness layer
        self.compute_fitness_layer(weights=weights, origin=origin, destination=destination, gmax=gmax, alignment=alignment, naa=naa, naa_w=naa_w)
        # to raster coords
        origin = self.process_coords(origin, to='raster')
        destination = self.process_coords(destination, to='raster')
        # compute path
        p,r = get_least_cost(world=self.fitness, origin=origin, destination=destination, plot=plot)
        self._a_raster = r
        self._a_idx = p


    def simplify_path(self, path_idx, resampling_idx=3., max_deviation=3.):
        """ Simplify the path from a polyline-like set of points to another set with less or equal amount of points.

        Parameters
        ----------
        path_idx : array-like
            points as [[i,j], ...]
        
        resampling_idx : float
            [optional] default=3. maximum allowed length for a single segment between two consecutive points

        max_deviation : float
            [optional] default=3. maximum allowe distance from the new polyline to the original set of points

        Returns
        -------
        array-like
            points as [[i,j], ...]
        """
        if(path_idx.shape[1]>2):
            path_idx=path_idx[...,:1]
        path_idx = resample_poly(LineString(path_idx), resampling_idx)
        path_idx = np.array(list(path_idx.coords))
        path_idx = smoothen_poly(path_idx, max_deviation) #name should be resample_poly
        return path_idx


    def smooth_path(self, path_idx, s, w=None):
        """ regressor to interpolate a series of points [i,j]

        path_idx : array-like
            points as [[i,j], ...]
        
        s : float
            smoothening factor as the maximum allowed cumulative squared distance vetween points and the curve

        w : weights
            [optional] None or weights to multiply the error distances
        
        Returns
        -------
        nd-array
            points as [[i,j], ...]
        """
        tck, u = interpolate.splprep(path_idx.transpose(), w=w, s=s)
        step = 1./len(path_idx)
        u = np.arange(0.,1.+step,step)
        pts = points_from_spline(tck,u)
        return pts


    # smoothening weights
    def update_path_weights_by_dist(self, pts, ref_pts, min_value=1., max_value=10., include_ends=True):
        """ Generate weights to interpolate pts based on the distance to ref_pts (focal points)

        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]

        ref_pts : array-like
            focal points as [[x,y], ...]
        
        include_ends : bool
            [optional] True to stiff the weights at the start and end points
        """
        dists = distance.cdist(pts, ref_pts) #calc dist points to closest ref_val
        dist = np.apply_along_axis(np.min, 1, dists)
        dist = dist + 1. # avoid division per 0
        a = 1./dist
        w = np.interp(a, (a.min(), a.max()), (min_value, max_value))
        if include_ends:
            w[0]  = max_value*10.
            w[-1] = max_value*10.
        return w


    def update_path_weights_by_metrics(self, pts, mweights, mfuncs, min_value=1., max_value=10., include_ends=True):
        """ Calculate how to update weights for interpolation

        Parameters
        ----------
        mweights : dict
            metric weights as dictionary {"metric":weight, ...}
        
        mfuncs : dict
            metric functions as dictionary {"metric":call_1, ...}
        
        Returns
        -------
        nd-array
            weights
        """
        assert mweights.keys() == mfuncs.keys(), "mw and mf must share the same keys."
        metrics = mweights.keys()
        #
        vals = []
        tot_w = sum(mweights.values())
        # build tmp alignment to run metrics onto
        pts = np.array([self.process_coords(pt ,to='bng') for pt in pts])
        tmp_a = Alignment(LineString(pts),0,self.granularity)
        tmp_a.run_metrics(metrics=metrics, dtm_grid=self['DTM'])

        for m in metrics:
            mw = mweights[m]
            mf = mfuncs[m]
            if DEBUG: print(tmp_a.metrics[m][:5])
            mv = np.array(list(map(mf, tmp_a.metrics[m]))) #metrics vals
            mv = np.interp(mv, (mv.min(), mv.max()), (min_value, max_value)) #remap within desired domain
            vals.append(mw * mv / tot_w) # multiply by weight and normalize
            if DEBUG: print((mw * mv / tot_w)[:5],'\n')
        w = np.array(vals)
        w = np.apply_along_axis(np.mean,0,w).squeeze()
        if include_ends:
            w[0]  = max_value*10.
            w[-1] = max_value*10.
        
        return w


    def update_path_dist(self, pts, s, **kwargs):
        """ Smooth the path due to its geometry
        
        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]
        
        s : float
            smoothening factor as the maximum allowed cumulative squared distance vetween points and the curve

        kwargs:
            resampling_idx  = 3.
            tolerance       = 10.
            min_value       = 1.
            max_value       = 10.
            include_ends    = True
        
        Returns
        -------
        array-like
            points of the smoothed curve
        """
        resampling_idx  = kwargs.get('resampling_idx', 3.)
        max_deviation   = kwargs.get('max_deviation', 10.)
        min_value       = kwargs.get('min_value', 1.)
        max_value       = kwargs.get('max_value', 10.)
        include_ends    = kwargs.get('include_ends', True)

        pts_ = self.simplify_path(pts, resampling_idx=resampling_idx, max_deviation=max_deviation)
        w = self.update_path_weights_by_dist(pts, pts_, min_value=min_value, max_value=max_value, include_ends=include_ends)
        pts = self.smooth_path(pts, s, w)
        return pts


    def update_path_metrics(self, pts, s, **kwargs):
        """ Smooth the path due to metrics

        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]

        s : float
            smoothening factor as the maximum allowed cumulative squared distance vetween points and the curve

        kwargs:
            resampling_idx  = 3.
            max_deviation   = 10.
            min_value       = 1.
            max_value       = 10.
            include_ends    = True
            mweights        = {}
            mfuncs          = {}

        Returns
        -------
        array-like
            points of the smoothed curve
        """
        resampling_idx  = kwargs.get('resampling_idx', 3.)
        max_deviation   = kwargs.get('max_deviation', 10.)
        min_value       = kwargs.get('min_value', 1.)
        max_value       = kwargs.get('max_value', 10.)
        include_ends    = kwargs.get('include_ends', True)
        mweights        = kwargs.get('mweights', {})
        mfuncs          = kwargs.get('mfuncs', {})

        w = self.update_path_weights_by_metrics(pts, mweights, mfuncs, min_value=min_value, max_value=max_value, include_ends=include_ends)
        pts = self.smooth_path(pts, s, w)
        return pts


    def refine_new_alignment(self, pts, refine, s):
        """ Smooth the path (as a point list) 

        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]
        
        refine : dict or None
            contains refinement options

        s : float
            smoothing factor. 0=not smooth

        Returns
        -------
        array-like
            points of the smoothed curve
        """
        if refine == None:
            pts = self.simplify_path(pts, resampling_idx=self._granularity*2.)
            pts_tmp = self.smooth_path(pts, s)
            if pts_tmp.min()<0 or False in [self._is_raster(idx) for idx in pts_tmp]:
                print("Smooth path results outside of the site boundary. Operation failed")
                pts_tmp = self.smooth_path(pts, 0.)
            pts = pts_tmp
            pts = list(map(self.to_coords, pts))
        elif 'distance' in refine.keys():
            parameters = refine['distance']
            pts = self.update_path_dist(pts, s, **parameters)
        elif 'metrics' in refine.keys():
            parameters = refine['metrics']
            pts = self.update_path_metrics(pts, s, **parameters)
        else:
            raise ValueError("input 'refine' is not a dictionary with either 'distance' or 'metrics'. Got {}".format(refine))
        return pts


    def _compute_new_alignment(self, origin, destination, weights, naa=None, gmax=1.35, alignment=None, plot=False, assign_id=None):
        """ Compute a new alignment for the grid. 

        Parameters
        ----------
        origin : int
            origin on the raster
        
        destination : int
            destination on the raster
        
        weights : dict
            dictionary with weights for each topo layer

        naa : str
            'circuity' or 'alignment' to bind the path to a specific area

        plot : bool
            True to plot the path on the raster

        Returns
        -------
        Alignment
            new alignment as Alignment object
        """
        if type(self.new_alingment_ij) != type(None):
            print("WARNING: Alignment solution is being overwritten")
        self.least_cost_path(origin, destination, weights, naa=naa, gmax=gmax, alignment=alignment, plot=plot)
        self._new_alignment_ID = int(uuid.uuid1())

        if DEBUG: # debug
            a = np.zeros(self.shape)
            for i,j in self.new_alingment_ij:
                a[int(i),int(j)]=1.
            plt.figure(figsize=(20,20))
            plt.imshow(a)
            plt.show()
            # plt.savefig(r'C:\19029_RAID_Git\notebooks\static\images\Sessions\plots\test_session')
        if assign_id is None:
            _id = str(uuid.uuid4())
        else:
            _id = assign_id
        self._a_ID = _id
        pts = [self.to_coords(pt) for pt in self.new_alingment_ij] 
        return Alignment(LineString(pts), self.new_alignment_ID, self._granularity)


    def compute_new_alignment(self, origin, destination, weights, naa=None, gmax=None, alignment=None, plot=False, assign_id=None):
        """ Compute a new alignment for the grid

        Parameters
        ----------
        origin : int
            origin on the raster
        
        destination : int
            destination on the raster
        
        weights : dict
            dictionary with weights for each topo layer

        naa : str
            'circuity' or 'alignment' to bind the path to a specific area

        plot : bool
            True to plot the path on the raster

        Returns
        -------
        Alignment
            new alignment as Alignment 
        """
        return self._compute_new_alignment(origin, destination, weights, naa, gmax=gmax, alignment=alignment, plot=plot, assign_id=assign_id)


    def compute_new_alignment_(self, origin, destination, weights, naa=None,  gmax=None, alignment=None, plot=False, assign_id=None):
        """ Compute a new alignment for the grid, and stores it locally
        
        Parameters
        ----------
        origin : int
            origin on the raster
        
        destination : int
            destination on the raster
        
        weights : dict
            dictionary with weights for each topo layer
        
        naa : str
            'circuity' or 'alignment' to bind the path to a specific area

        plot : bool
            True to plot the path on the raster

        Returns
        -------
        None
        """
        self._a = self._compute_new_alignment(origin, destination, weights, naa, gmax=gmax, alignment=alignment, plot=plot, assign_id=assign_id)


    # ALIGNMENT
    def evaluate_alignment_(self, metrics=METRICS):
        """ run metrics on the alignment. Values are stored in its relative metrics member
        
        Parameters
        ----------
        metrics : list<str>
            [optional] default as per raidnr.config.py file. It is not recommended to change it. If necessary, a subset of such list can be passed

        Returns
        -------
        None
        """
        if self._a == None:
            raise ValueError("Alignment must be computed first")
        
        self.new_alignment.run_metrics(metrics=metrics)


    def evaluate_alignment(self, alignment, metrics=METRICS):
        """ run metrics on the refined alignment. Values are stored in its relative metrics member

        Parameters
        ----------
        metrics : list<str>
            [optional] default as per raidnr.config.py file. It is not recommended to change it. If necessary, a subset of such list can be passed

        Returns
        -------
        None
        """
        alignment.run_metrics(metrics=metrics)
        return alignment


    def _rasterize_alignment(self, alignment, metrics=METRICS):
        """ Rasterize alignment on the grid

        Parameters
        ----------
        alignment: rGrid

        Returns
        -------
        tuple of arrays
            occupied cells [[i,j],...], metrics as [{metric_1:value_1,...},...]
        """
        # add first point
        path = [np.array(self.to_raster(alignment[0].o))] 
        tmp = {}
        for metric in metrics:
            try:
                x = alignment.metrics[metric][0]
                tmp[metric] = x if not np.isnan(x) else None
            except:
                tmp[metric] = None
        values = [tmp]; del tmp

        for i in range(len(alignment)-1):
            p1, p2 = alignment[i].o, alignment[i+1].o
            p1, p2 = np.array(self.to_raster(p1)), np.array(self.to_raster(p2))
            idx = list(np.array(skd.line(p1[0],p1[1],p2[0],p2[1])).transpose())[1:]
            us = [np.dot(ii-p1, p2-p1) / np.linalg.norm(p2-p1)**2 for ii in idx[:-1]]+[1.] # idx normalised on p1-to-p2. 1.0 is added instead of computed due to decimals precision
            # extract metrics
            vals = [{metric:None for metric in metrics} for _ in us]
            for metric in metrics:
                try:
                    x1,x2 = alignment.metrics[metric][i], alignment.metrics[metric][i+1]
                    val1 = x1 if not np.isnan(x1) else None
                    val2 = x2 if not np.isnan(x2) else None
                    # interpolate metrics 
                    if val1 and val2:
                        for u,val in zip(us,vals):
                            val[metric]=val1 + u*(val2-val1)
                    elif val2:
                        vals[-1][metric]=val2
                    else:
                        pass # values are kept None
                except:
                    pass # values are kept None
            # add data - do not put into a try
            path += idx
            values += vals
        # np-array
        path = np.array(path, dtype='int64')
        values = np.array(values, dtype='object')
        
        return path, values


    def rasterize_alignment(self, metrics=METRICS):
        """ Rasterize (local) alignment on the grid

        Parameters
        ----------
        metrics: array-like
            list of metrics to export

        Returns
        -------
        tuple of arrays
            occupied cells [i,j], metrics as {metric:value,...}
        """
        return self._rasterize_alignment(self._a, metrics=metrics)


    def alignment_cost_surroundings(self, raster_coords,k_size,specific_topo_layers=None):
        """ Select a path, or a collection of points in which you want to apply the kernel

        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]

        topo_layers: list<string>
            layers against to calculate the cost of the alignment
        
        k_size : int
            kernel size. Must be an odd number
        """
 
        wy,wx = zip(*raster_coords)


        # idx = [TOPO_LAYERS.index(tl) for tl in topo_layers]
        
        
        # output
        cost_surroundings = OrderedDict()
        
        for index_layer,layer_name in enumerate(TOPO_LAYERS):

            cost_min = np.min(self._A[:,:,index_layer])
            cost_max = np.max(self._A[:,:,index_layer])
            
            if(cost_min==cost_max==0):
                continue
            
            else:

                cost_surroundings[layer_name] = gt.return_cost(self._A, index_layer, wx, wy, k_size)
        
        self._surroundings = cost_surroundings
        self._k_size = k_size
        
        return cost_surroundings


    def point_cost_surroundings(self,k_size, i, j,specific_topo_layers=None):
        """ Compute the cost surrounding for one ore more given point(s)

        Parameters
        ----------
        topo_layers: list<string>
            layers against to calculate the cost of the alignment
        
        k_size : int
            kernel size. Must be an odd number
        
        i : int
            i coordinate
        
        j : int
            j coordinate
        
        Returns
        -------
        OrderedDict
        """
        # idx = [TOPO_LAYERS.index(tl) for tl in TOPO_LAYERS]
        # output
        cost_surroundings = OrderedDict()
        
        for ii,tl in enumerate(TOPO_LAYERS):
            
            cost_surroundings[tl] = gt.return_cost(self._A, ii, i, j, k_size)
        
        
        return cost_surroundings


    # PLOT
    def plot_cost_surroundings(self,fileName, figsize=(20,10), alpha=0.7,show=False,kernel_size=21):
        """ Plot cost surroundings if analysis has been performed

        Parameters
        ----------
        figsize : tuple
            matplotlib figsize
        
        alpha : float
            alpha value
        """
        if not self._surroundings:
            raise ValueError("Surrounding costs must be computed first")
        
        cmap = plt.cm.get_cmap("inferno", len(self._surroundings))

        y, pal, titles = [],[],[]
        k = float(21)**2
        i=0
        
        for tl,cost in self._surroundings.items():

            if(tl=="DTM"):
                continue

            if np.max(cost) > 1.:
                y.append(100.*cost/((kernel_size**2)*255.)) #cost is an array
                pal.append(cmap(i))
                titles.append(tl) # used a ordered dict
            i+=1
        
        # data to plot
        r=np.arange(len(y[0]))
        # title_label= self._surroundings.keys()
        fig,ax = plt.subplots(figsize=figsize)

        plt.stackplot(r,y, labels=titles, colors=pal,alpha=alpha)
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.7))
        ax.set_axis_bgcolor("white")
        plt.xlabel("index of path")
        plt.ylabel("% of the kernel size")
        
        if(show):
            plt.show()
        else:
            plt.savefig(fileName)

        


    def plot_raster_layer_to_ax(self,name,ax,color,size,z,title,markerS="o"):
        """ WIP

        Parameters
        ----------
        WIP

        Returns
        -------
        None
        """
        if markerS!="o":
            size=None
        railwayIndex= TOPO_LAYERS.index(name)
        railWay=self._A[:,:,railwayIndex]

        if ( np.max(railWay)>0):
            rail_rasterY,rail_rasterX=np.where(railWay>0)

            ax.scatter(rail_rasterX,rail_rasterY,c=color,s=size,alpha=0.3,label=title,marker=markerS,zorder=z)


    def plot(self,fileName="",origin=None,destination=None,layer=None,pts_alignment=[],pts_simplified=[],show=False):
        """ WIP

        Parameters
        ----------
        WIP

        Returns
        -------
        None
        """
        fig,ax = plt.subplots(figsize=(20,10))
        # baseplot   
        if(layer!=None):
            testarray=self._A
            ax.imshow(testarray[:,:,layer],"viridis")
        else:        
            sumarray=self._fitness_layer
            ax.imshow(sumarray,"viridis")
        if(origin!=None and destination!=None):
            origin_ = gt.coords_geo_to_raster(self._bbox, origin, pixels_size=self._granularity,)
            destination_ = gt.coords_geo_to_raster(self._bbox, destination, pixels_size=self._granularity)

        # origin and destination
            ax.scatter(origin_[1],origin_[0],c='green',s=100,label='origin')
            ax.scatter(destination_[1],destination_[0],c='red',s=100,label='destination')
            plt.title("Least Cost Path")
        else:
            plt.title(TOPO_LAYERS[layer])
        try:
            # least cost path found_before smoothing
            if(len(pts_alignment)>0):
                simpley,simplex=zip(*pts_alignment)
                ax.plot(simplex,simpley,c='cyan',alpha=0.9,label='initial path found')
            if(len(pts_simplified)>0):
            # least cost path found_before smoothing
                smoothy,smoothx=zip(*pts_simplified)
                ax.plot(smoothx,smoothy,c='orange',alpha=0.9,label='smoothed path found')
        except:
            next

        # if near railwayTracks exist
        self.plot_raster_layer_to_ax("RailwayStation",ax,'orange',1,10,'existing railway stations',"3")

        # if near RailwayStation exists
        self.plot_raster_layer_to_ax("RailwayTrack",ax,'yellow',1,2,'existing railway tracks')

        # deal with legend
        thisLegend = ax.legend()
        for lh in thisLegend.legendHandles: 
            lh.set_alpha(1)


        plt.xlabel('longitude')
        plt.ylabel('latitude')

        minx,miny,maxx,maxy = self._bbox
        # ax ticks
        ticks=[int(i) for i in np.arange(minx,maxx,300)]
        plt.xticks(np.arange(0,self._A.shape[1],300),ticks)

        ticks=[int(i) for i in reversed(np.arange(miny,maxy,300))]
        plt.yticks(np.arange(0,self._A.shape[0],300),ticks)


        # locaiton of the legend
        ax.legend( bbox_to_anchor=(1.1,0.5) ,loc='center')
        
        if(show):
            plt.show()
        else:
            plt.savefig(fileName+".png")
    


    # IO
    def alignment_as_json_str(self):
        """ Return the new alignment as a json-encoded string
        """
        return self.alignment.to_json_str()


    #static
    @staticmethod
    def from_shapefiles(bbox, buffers, shp_folder, bbox_folder, array_folder, granularity=3.0):
        """ 
        """
        pass
        return Grid(a, bbox, granularity)


