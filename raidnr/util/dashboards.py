import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import os

import geopandas as gp
import shapely
from shapely.geometry import Point,LineString,Polygon,MultiPolygon,MultiPoint,mapping
from shapely.ops import cascaded_union,unary_union,nearest_points,split



#constant
#meters to miles conversion: meters * conv = miles 
conv = 0.000621371192

#operations on points

def to_mileage(pointLayer,line):
    """
    Finds the mileage of each point that is part of a line

    Parameters:
    pointLayer: geopandas geodataframe of point features
    line: shapely LineString
    """


    pointLayer['mileage']=float(0)
    #find the mileage
    for i in range(len(pointLayer)):
        p=pointLayer['geometry'][i]
        pointLayer['mileage'][i]=round(line.project(p)*conv,3)

    return pointLayer


#save files 
def save_for_tableau(df,fileType,fileName):
    cwd=os.getcwd()
    src=src=os.path.dirname(cwd)

    dash_dir=os.path.join(src,'notebooks','dashboards')

    if fileType == 'csv':
        df.to_csv(os.path.join(dash_dir,fileName + '.csv'))

    elif fileType == 'shp':
        df.to_file(os.path.join(dash_dir,fileName + '.shp'))

