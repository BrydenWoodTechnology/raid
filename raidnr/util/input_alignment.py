import pandas as pd,numpy as np
import json
import geopandas as gpd, fiona, os, shapely
from shapely.geometry import *
from shapely.ops import nearest_points,unary_union,cascaded_union, split
import itertools

import glob
import time

from raidnr.util.ext_methods import *
import raidnr.util.vertical_alignment as va
import raidnr.util.SpatialToolbox as st

from raidnr.core.alignment import Alignment
from raidnr.config import CRS, GRANULARITY, METRICS


#set the paths
note_dir=os.getcwd()
src=src=os.path.dirname(cwd)
dataPath=os.path.join(src,"data")
assetPath=os.path.join(dataPath,'RailAssets') #define the path where the shapefiles are stored
contextPath=os.path.join(dataPath,"RailContext")



#recreate the line - interpolation - using the alignment module
pl=purge_poly(os.path.join(dataPath,'Railway','Simplified Railline','Manchester_York_Simple_Left_Line.shp'))['geometry'][0]
pl=resample_poly(pl,300) #resample
a=reconstruct_poly(pl,3) #reconstruct
newLine=LineString(a) #store the reconstruced line as a shapely LineString 

bbox=newLine.envelope #shapely polygon


#cut line at specific points - specify the segment between stations 

#1.import the stations
stations=gp.read_file(os.path.join(dataPath,'RailAssets','RailwayStations.shp'))

#check if shapefile is multipoint - if it is convert it to Point
#from MultiPoint to Point
if any(stations.geom_type=='MultiPoint'):
    stations=st.MultipointToPoint(stations)
else:
    pass


#2. clip to the bounding box
clipped_stations=st.clip_polyPoints(stations,bbox)#!!!!!!!!!!!!!!bbox is shapely polygon

#3. snap them to the railway line
snapped_stations=st.snap_points(clipped_stations,newLine) 


#4.pick the start & end stations
orig_name='Slaithwaite  Station'
dest_name='Huddersfield Station'

if (orig_name in list(stations['NAME'])) & (dest_name in list(stations['NAME'])):

    orig_pt=snapped_stations.loc[snapped_stations['NAME']==orig_name].reset_index(drop=1)['geometry'][0]
    dest_pt=snapped_stations.loc[snapped_stations['NAME']==dest_name].reset_index(drop=1)['geometry'][0]
    
    segment=st.select_segment(orig_pt,dest_pt,newLine) #the alignment we work with 
    bbox=segment.envelope #the bounding box
    
else:
    print('Please provide valid origin and destination points')

    segment=newLine
    bbox=newLine.envelope


#Context Data
#Import the CSV with the buffer distances and weights
bf=pd.read_csv(os.path.join(dataPath,'RailContext','Buffers.csv'))

#list of the context layers that might appear in the area of interest
contextNames=['Buildings','CarChargingPoint','ElectricityTransmissionLine'
,'FunctionalSite'
,'Greenspaces'
,'NationalParks'
,'SurfaceWater_Area'
,'SurfaceWater_Line'
,'Woodland']










#Asset Data

#list of the assets that are available as inputs
assetNames=['Stations','LevelCrossings','Tunnels','Bridges']

assetShp=glob.glob(assetPath + "/*.shp")  #list with all shapefiles in the folder

assetList=[]

for file_ in assetShp: 

    geoasset=gp.read_file(file_)

    geoasset['Layer']=str(os.path.splitext(os.path.split(file_)[1])[0]) #create an identifier for each asset layer










    