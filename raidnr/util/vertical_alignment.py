
# ---------------------------------------------------
# ¦¦¦¦¦¦¦    ¦¦¦¦¦   ¦¦¦¦  ¦¦¦¦¦¦¦¦  
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦¦¦¦¦   ¦¦¦¦¦¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦  
# ¦¦¦   ¦¦¦ ¦¦   ¦¦¦ ¦¦¦¦  ¦¦¦¦¦¦¦¦    
# ---------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Konstantina Spanou
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# A list of functions for creating an applicable area where the algorithm should search for a path
#-----------------------------------------------------
# Docs Reference : link to module docs
# Attribution : W. Li et al., 2016
#-----------------------------------------------------


import numpy as np, math
from osgeo import gdal
import os



def airline_dist(startX, startY, endX, endY, x, y):
    """
    Compute the airline distances between each grid cell and the origin & destination of the path.
    
    Parameters
    ----------
    startX, startY: tuple
        the position of the origin in the array (index)

    endX, endY: tuple
        the position of the destination in the array (index)

    X: the row index of all grid cells
    Y: the column index of all grid cells
    """

    Ls=(x-startX)**2 + (y-startY)**2
    Le=(x-endX)**2 + (y-endY)**2

    distS=np.sqrt(Ls)
    distE=np.sqrt(Le)
    
    #set the start & end point of the path 
    L_start_end=np.sqrt((endX-startX)**2 + (endY-startY)**2) 
    

    circuity=(distS+distE)/L_start_end
    
    return circuity,distS,distE




#evaluate the circuity of each cell based on the angle that is formed  between the cell and the start&end cells
#need to assign a maximum allowable circuity coefficient - gmax
def evaluate_circuity(circuity,gmax=4):
    """
    Circuity is given by: (Ls + Le)/Lse 
    Where:
    - Ls,Le is the distance between the cell and the start & end point of the alignment, respectively
    - Lse: the distance between the start & end cells of the alignment
    
    """
    evaluation=int(circuity< gmax)
    
    return evaluation




#evaluate if the length of the alignment is enough to overpass the height difference
def evaluate_elevation(Hs,He,airline_dist,imax=0.025,gmax=4):
    evaluation=np.array(airline_dist*imax*gmax > abs(He-Hs),dtype=int)
    
    return evaluation


#   evaluate the elevation range at each grid cell 
def elevation_bounds(Hs,He,distS,distE,imax=0.025,gmax=4):
    Hs_max=Hs + (imax*gmax*distS)
    Hs_min=Hs - (imax*gmax*distS)

    He_max=He + (imax*gmax*distE)
    He_min=He - (imax*gmax*distE)
    
    myarray=np.array(list(zip(Hs_min,Hs_max,He_min,He_max)))
    
    return myarray


def elevation_intersection(myarray):
    #myarray=np.array(list(zip(Hs_min,Hs_max,He_min,He_max)))
    
    c=np.arange(round(myarray[0],1),round(myarray[1],1),0.1)
    d=np.arange(round(myarray[2],1),round(myarray[3],1),0.1)

    if len(c)<len(d):
        iterator=c
        search=d
    else:
        iterator=d
        search=c   
        
    count=0
    for number in iterator:
        if (number>=search.min()) & (number<=search.max()):
            count+=1          
        
    if count==0:
        return 0
    else:
        return 1