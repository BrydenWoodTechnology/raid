
# ---------------------------------------------------
# ¦¦¦¦¦¦¦    ¦¦¦¦¦   ¦¦¦¦  ¦¦¦¦¦¦¦¦  
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦¦¦¦¦   ¦¦¦¦¦¦¦   ¦¦    ¦¦   ¦¦¦   
#  ¦¦   ¦¦  ¦¦   ¦¦   ¦¦    ¦¦   ¦¦¦  
# ¦¦¦   ¦¦¦ ¦¦   ¦¦¦ ¦¦¦¦  ¦¦¦¦¦¦¦¦    
# ---------------------------------------------------
# Bryden Wood
# License: MIT, see full license in LICENSE.txt
# Web: repository-link
#-----------------------------------------------------
# Date: 30.09.2019
# Authors: Claudio Campanile
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# Design manager to handle railway design
#-----------------------------------------------------
# Docs Reference : link to module docs
#-----------------------------------------------------

# modules
import os
from collections import OrderedDict
import json
import uuid
from copy import copy, deepcopy
# scientific modules
import numpy as np
from numpy.core.umath_tests import inner1d
from scipy import interpolate
from scipy.spatial import distance
import skimage.draw as skd
# geo modules
import geopandas as gpd
from shapely.geometry import LineString
# plot
import matplotlib.pyplot as plt

# import raidnr
from raidnr.config import CRS, GRANULARITY, METRICS, TOPO_LAYERS, DEBUG
from raidnr.bwgrid.grid import Grid
from raidnr.core.alignment import Alignment
import  raidnr.util.ext_methods as em

class DesignManager(object):

    def __init__(self, grid, session_name, existing_alm=None, image_path=None):
        """ DM consumes a Grid object to produce, analyse and compare new alignment design iterations
        """
        self._grid = grid
        self._design_params = None
        
        # existing alignment
        self._a_exst = existing_alm

        # new alignment
        self._a_ID = str(uuid.uuid4()) # keep track of the ID in case an alignment is regenrated
        self._a_version = 0
        self._a_weights = None
        self._a = None # alignment object
        self._a_idx = None # [i,j] points
        self._a_raster = None # as an array of self.shape
        self._a_cost_srr = None
        
        # refined alignment
        self._a_rfnd_version = 0
        self._a_rfnd_params = None
        self._a_rfnd = None
        self._a_rfnd_idx = None
        self._a_rfnd_raster = None
        self._a_rfnd_cost_srr = None
        
        # session
        self.session_name = session_name
        
        # IMPORTANT
        # _a_idx is in Raster coords
        # while _a_rfnd_ix is in BNG 
        #-----------------------------------
        #-----------------------------------
        # WHAT WE NEED IS
        # alignments=[]
        # refined_alignments=[]
        # alignment= Alignment()
        # refined_alignment = Alignment()
        # coords of the alignment 
        # either raster, bng or raster should be moved there
        # global manager doesn't have to store this
        # methods to translate coords exist in grid transformation we can call them directly 

        self.refined_alignments_dict = {} 
        
        # extra
        self._k_size = None
        self._layer_names = TOPO_LAYERS
        # paths
        self._imagePath = image_path


    # base properties
    @property
    def grid(self):
        return self._grid
    @property
    def alignment_existing(self):
        return self._a_exst
    # new alignment
    @property
    def design_params(self):
        return self._design_params
    @property
    def alignment_ID(self):
        return self._a_ID
    @property
    def alignment_version(self):
        return self._a_version
    @property
    def alignment_weights(self):
        w = self._a_weights.copy()
        w.update({'ID':self.alignment_ID,'version':self.alignment_version})
        return w
    @property
    def alignment(self):
        return self._a
    @property
    def alignment_ij(self):
        return self._a_idx
    @property
    def alignment_rr(self):
        return self._a_raster
    @property
    def alignment_cost(self):
        return self._a_cost_srr
    @property
    def alignment_metrics(self):
        return self.alignment.metrics
    # refined alignment
    @property
    def alignment_refined_ID(self):
        return self.alignment_ID + '_ver_' + str(self.alignment_refined_version)
    @property
    def alignment_refined_version(self):
        return self._a_rfnd_version
    @property
    def alignment_refined_params(self):
        p = self._a_rfnd_params.copy()
        p.update({'ID':self.alignment_refined_ID,'version':self.alignment_refined_version})
        return p
    @property
    def alignment_refined(self):
        return self._a_rfnd
    @property
    def alignment_refined_ij(self):
        return self._a_rfnd_idx
    @property
    def alignment_refined_rr(self):
        return self._a_rfnd_raster
    @property
    def alignment_refined_cost(self):
        return self._a_rfnd_cost_srr
    @property
    def alignment_refined_metrics(self):
        return self.alignment_refined.metrics


    def design_new_alm(self, origin, destination, weights, naa=None, gmax=None, alignment=None, plot=False):
        """ Compute a new alignment by using the embedded grid and store it into the object properties alignment, alignment_ij, alignment_rr
        
        Parameters
        ----------
        origin : int
            origin on the raster
        
        destination : int
            destination on the raster
        
        weights : dict
            dictionary with weights for each topo layer
        
        naa : str
            'circuity' or 'alignment' to bind the path to a specific area

        plot : bool
            True to plot the path on the raster

        Returns
        -------
        None
        """
        if(self._a_version>0):
            self._a_ID = str(uuid.uuid4())
            self._a_rfnd_version=0
        if alignment is None and self.alignment_existing is not None:
            print(self.alignment_existing)
            alignment=self.alignment_existing.to_raster_coords_Alignment(self.grid)
            
        self._design_params = {"origin":origin,"destination":destination,"weights":weights}
        self.grid.compute_new_alignment_(origin, destination, weights, naa=naa, gmax=gmax, alignment=alignment, plot=plot, assign_id=self.alignment_ID)
        self._a_weights = weights
        self._a = self.grid.new_alignment   

        #_a_idx CONTAINS RASTER COORDS  
        self._a_idx = self.grid.new_alingment_ij

        # self.plotGridLayer(pts_alignment=self.alignment_ij)
        # plt.show()
        self._a_raster = self.grid.new_alignment_rr
        self._a_version += 1


    def refine_new_alm(self, refine=None, iterations=5, s=500.):
        """ Refine the curve by three methods:
          * Smooth uniformly
          * Smooth by discontinuity points
          * Smooth by metrics
        
        Parameters
        ----------
        refine : dict, str or None
            [optional] default=None
        """
        assert(isinstance(refine,str) or isinstance(refine,dict) or refine is None), "WARNING: refine expects refine parameters as per documentation, got {}".format(refine)
        # check the smooth cases
        case_distance = False
        case_metrics = False
        case_standard = refine is None or refine == 'uniform'
        try:
            case_distance = 'distance' in refine.keys() #or refine == 'distance'
            case_metrics = 'metrics' in refine.keys() #or refine == 'metrics'
        except:
            pass
        if sum([int(case_distance), int(case_metrics), int(case_standard)]) != 1:
            print("WARNING: refine expects refine parameters as per documentation, got {}".format(refine))
            return
        
        # set smoothing parameters. Process inputs and set as per grid
        if case_standard:
            refine = None
        pts = copy(self.alignment_ij)
        for ii in range(iterations):
            pts = self.grid.refine_new_alignment(pts, refine=refine, s=s)
            
            if DEBUG:
                plt.figure(figsize=(20,20))
                plt.imshow(self._ij_to_raster(pts))
                fileName = os.path.join(self._imagePath,'path_'+str(ii)+'.png')
                plt.savefig(fileName)
                plt.show()
        
        # CONTAINS RASTER COORDS  
        self._a_rfnd_idx = pts
        self._a_rfnd_raster = self._ij_to_raster(pts)
        
        # HAD TO COMMENT OUT .. _a_rfnd has to be in the same format as _a 
        pts = [self.grid.to_coords(pt) for pt in pts]
        
        # output
        self._a_rfnd = Alignment(LineString(pts), self.alignment_refined_ID, self.grid.granularity)
        self._a_rfnd_params = refine if refine else {}
        self._a_rfnd_version += 1
        return 



    def refine_new_alm_with_fillet(self, refine=None, iterations=5, s=500., fillet_dist=60.):
        """ Refine new alignment only when detached from the existing
        
        Parameters
        ----------
        refine : dict, str or None
            [optional] default=None
        """
        assert(isinstance(refine,str) or isinstance(refine,dict) or refine is None), "WARNING: refine expects refine parameters as per documentation, got {}".format(refine)
        # check the smooth cases
        case_distance = False
        case_metrics = False
        case_standard = refine is None or refine == 'uniform'
        try:
            case_distance = 'distance' in refine.keys() #or refine == 'distance'
            case_metrics = 'metrics' in refine.keys() #or refine == 'metrics'
        except:
            pass
        if sum([int(case_distance), int(case_metrics), int(case_standard)]) != 1:
            print("WARNING: refine expects refine parameters as per documentation, got {}".format(refine))
            return
        
        # set smoothing parameters. Process inputs and set as per grid
        if case_standard:
            refine = None
        
        # separate parts of new paths coincident and decatched from existing alignment
        attached, detached, mask = self._separate_alignment_subset()
        existing_list = self._trim_existing_alignment(attached)
        align_new_list = self._trim_new_alignment(detached)
        to_refine_list = deepcopy(align_new_list)
        
        # Smooth detached segments
        for i in range(iterations): # iterations
            for ii in range(len(align_new_list)): # 
                try:
                    to_refine_list[ii] = self.grid.refine_new_alignment(to_refine_list[ii], refine=refine, s=s)

                    if DEBUG:
                        self.plotGridLayer(pts_alignment=align_new_list[ii], pts_simplified=to_refine_list[ii], i=i*ii)

                        plt.show()
                except Exception as e:
                    print("WARNING: Smoothing not possible due to:\n{}".format(e))
                    
        # convert existing to raster and join back
        existing_ij_list = [self.grid._rasterize_alignment(ex)[0] for ex in existing_list]
        joined_list = em.pick_from_arrays(to_refine_list, existing_ij_list, mask)
        
        # for a in joined_list:
        #     print(a[0],",",a[-1])
        # create alignments and fillet
        alms = []
        for i,j in enumerate(joined_list):
            # use incremental i to define ids
            try:
                a = Alignment(LineString(j), i, self.grid.granularity)
            except:
                a=None
            alms.append(a)

        # fillet and combine alignments back
        a_combined = alms[0]
        for i in range(1,len(alms)):
            a_combined = Alignment.fillet(a_combined, alms[i], fillet_dist)

        # Register output
        self._a_rfnd = a_combined
        pts = np.array(list(a_combined.points)).transpose()[:2].transpose() # delete z coord
        if DEBUG:
            self.plotGridLayer(pts_alignment=align_new_list, pts_simplified=pts)
            plt.show()
        self._a_rfnd_idx = pts
        self._a_rfnd_raster = self._ij_to_raster(pts)
        self._a_rfnd_params = refine if refine else {}
        self._a_rfnd_version += 1
        
        return


    def _separate_alignment_subset(self, lim_dist=2., lim_n=15):
        """ Separate a new generated path in (i,j) coords between segments which overlap with an existing alignment and segments which detach from it

        Parameters
        ----------
        lim_dist : float
            threshold above which the path is considered detached
        
        lim_n : int
            threshold representing the minimum amount of continously detached cells 

        Returns
        -------
        dict
            {
                'attached':[[T0_0,T1_0], [T0_1,T1_1], ...],
                'detached':[[T0_0,T1_0], [T0_1,T1_1], ...],
                'mask':[1,0,1,0 ...],
            }
            where 'mask' list has 1 for attached and 0 for detached. It can be use to weave the segments back
        pts : array-like
            points as [[x,y], ...], tipically the design alignment as [i,j] NOTE: end is included
        """
        pts = copy(self.alignment_ij)
        # find parts coincident with original alignment
        al_pts = np.array(list(self.alignment_existing.line.coords))
        tmp = []
        for pt in al_pts:
            try:
                ij=self.grid.process_coords(pt) 
            except:
                ij=[-lim_dist,-lim_dist]
            tmp.append(ij)
        al_pts = np.array(tmp); del tmp
        dists_ = distance.cdist(pts, al_pts)
        dists  = np.apply_along_axis(np.min, 1, dists_)
        masks  = em.distance_mask(dists, lim_dist=lim_dist, lim_n=lim_n)
        # attached = [deepcopy(pts[i[0]:i[1]]) for i in masks['attached']]
        # detached = [deepcopy(pts[i[0]:i[1]]) for i in masks['detached']]

        return masks['attached'], masks['detached'], masks['mask']


    def _trim_existing_alignment(self, attached):
        """ extract subsets of existing alignment to be re-used for the new one

        Parameters
        ----------
        attached : array-like
            Indices of points which define the subset [[idx_start_0,idx_end_0],...], referred to self.alignment_ij
        
        Returns
        -------
        list<Alignment>
            new alignment parts coincident with existing
        """
        # extract pts from existing alignment and convert to raster
        al_pts = np.array(list(self.alignment_existing.line.coords))
        tmp = []
        for pt in al_pts:
            try:
                ij=self.grid.process_coords(pt) 
            except:
                ij=[-1,-1]
            tmp.append(ij)
        al_pts = np.array(tmp); del tmp
        
        pts = copy(self.alignment_ij)
        subsets = []
        for att in attached:
            # take start,end i-j points indices and take actual [i,j] pts
            pt0=pts[att[0]]
            pt1=pts[att[1]+1]
            # calc dist from all alignment pts to [[i0,j0],[i1,j1]] and get the closest ones as indices
            dists_0  = distance.cdist(al_pts, [pt0])
            dists_1  = distance.cdist(al_pts, [pt1])
            min_0    = np.min(dists_0)
            min_1    = np.min(dists_1)
            al_start = np.where(dists_0==min_0)[0][0]
            al_end   = np.where(dists_1==min_1)[0][0]
            # cut alignment
            subsets.append(
                self.alignment_existing.extract_subset(al_start, al_end)
            )
        return subsets


    def _trim_new_alignment(self, detached):
        """ Trim self.alingment_ij in alignment subset by keeping only the detached parts
    
        Parameters
        ----------
        detached : array-like
            Indices of points which define the subset [[idx_start_0,idx_end_0],...], referred to self.alignment_ij
        
        Returns
        -------
        list
            [[[i_0,j_0],[i_1,j_1],...], ...] as list of list of points for each subset
        """
        subsets = []
        for dd in detached:
            t1=dd[0]
            t2=min(dd[1]+1,len(self.alignment_ij)-1)
            subsets.append(
                self.alignment_ij[t1:t2]
            )
        return subsets


    def analyse_new_alm(self, metrics=METRICS,  k_size=11):
        """ Analyse new generated alignment

        Parameters
        ----------
        metrics : list<str>
            [optional] default as per raidnr.config.py file. It is not recommended to change it. If necessary, a subset of such list can be passed
        
        k_size : int
            kernel size, must be an odd number
        
        Returns
        -------
        None
        """
        if self.alignment is None:
            raise Exception("New alignment has not been calculated")
        
        self.alignment.run_metrics(metrics=metrics, dtm_grid=self.grid)

        # TMP - NOT USED
        # self._a_cost_srr = self.grid.alignment_cost_surroundings(pts=self.alignment_ij,  k_size=k_size)
        # self.alignment.run_metrics(metrics=['permissible_speed'], dtm_grid=self.grid)
        # self._extract_new_alm_elevation()


    def analyse_refined_alm(self, metrics=METRICS,  k_size=11):
        """ Analyse new generated alignment, after refinement

        Parameters
        ----------
        metrics : list<str>
            [optional] default as per raidnr.config.py file. It is not recommended to change it. If necessary, a subset of such list can be passed
        
        k_size : int
            kernel size, must be an odd number
        
        Returns
        -------
        None
        """
        if self.alignment_refined is None:
            raise Exception("Refined alignment has not been calculated")
        self.alignment_refined.run_metrics(metrics=metrics, dtm_grid=self.grid)
        
        # TMP - NOT USED
        # self._a_rfnd_cost_srr= self.grid.alignment_cost_surroundings(pts=self.alignment_refined_ij, k_size=k_size)
        # self._extract_refined_alm_elevation()


    # output
    def serialize_new_alm(self):
        """ Output the new alignment as JSON-serialized string

        Returns
        -------
        str
            JSON-encoded string
        """
        return self.alignment.to_json_str(extend={"session_ID":self.session_name,'parent_ID':self.alignment_ID, 'alignment_type':'new','version':self.alignment_version,"bbox":self.grid.bbox},cost_dict=self.grid._surroundings)


    def serialize_refined_alm(self):
        """ Output the refined alignment as JSON-serialized string

        Returns
        -------
        str
            JSON-encoded string
        """
        return self.alignment_refined.to_json_str(extend={"session_ID":self.session_name,'parent_ID':self.alignment_ID,'alignment_type':'refined', 'version':self.alignment_refined_version,"bbox":self.grid.bbox},cost_dict=self.grid._surroundings)


    def rasterize_new_alm(self):
        """ Rasterize new alignment to base grid

        Returns
        -------
        tuple of arrays
            occupied cells [[i,j],...], metrics as [{metric_1:value_1,...},...]
        """
        return self.grid._rasterize_alignment(self.alignment)


    def rasterize_refined_alm(self):
        """ Rasterize refined alignment to base grid

        Returns
        -------
        tuple of arrays
            occupied cells [[i,j],...], metrics as [{metric_1:value_1,...},...]
        """
        return self.grid._rasterize_alignment(self.alignment_refined)


    def _ij_to_raster(self, pts, val=1.):
        """ Convert a list of raster position to a raster with such cells valued not zero

        Parameters
        ----------
        pts : array-like
            points as [[x,y], ...]
        
        val : float
            [optional] default=1. value to assign to path cells
        """
        a = np.zeros(self.grid.shape)
        for i,j in pts:
            a[int(i),int(j)]=1.
        return a


    # plot
    def plot_alm(self,fileName="",origin=None,destination=None,layer=None,pts_simplified=None,show=False):
        """
        """
        print("Ploting refined alignment...")
        self.grid.plot(fileName=fileName,origin=origin,destination=destination,layer=layer,pts_alignment=self._a_idx,pts_simplified=self._a_rfnd_idx,show=show)


    def get_alignment_to_coords(self,fileName="",crs="wgs",geoDataFrame=True,save="shp"):
        """
        """
        self.alignment.to_coords(fileName=fileName,crs=crs,geoDataFrame=geoDataFrame,save=save)


    def get_refined_alignment_to_coords(self,fileName="",crs="wgs",geoDataFrame=True,save="shp"):
        """
        """
        self.alignment_refined.to_coords(fileName=fileName,crs=crs,geoDataFrame=geoDataFrame,save=save)


    # TEMPORARY
    def _extract_new_alm_elevation(self):
        """
        """
        e = [em.extract_elevation(pt, self.grid) for pt in self.alignment]
        e = np.array(e)
        e = np.nan_to_num(e)
        self.alignment._elevations = e


    def _extract_refined_alm_elevation(self):
        """
        """
        e = [em.extract_elevation(pt, self.grid) for pt in self.alignment_refined]
        e = np.array(e)
        e = np.nan_to_num(e)
        self.alignment_refined._elevations = e


    def plotGridLayer(self, pts_alignment=None, pts_simplified=None, layer=None, i=None, path=None, show=False):
        """ 
        """
        global_start = self.design_params['origin']
        global_end = self.design_params['destination']
        if path is None:
            path = ''

        if pts_alignment is None:
            try:
                pts_alignment = self.alignment_ij
            except:
                pts_alignment = []
        if pts_simplified is None:
            try:
                pts_simplified = self.alignment_refined_ij
            except:
                pts_simplified = []

        if(layer!=None):
            filename= str(layer)+"_%s"%(str(np.random.randint(0,10000)))
            filePath = os.path.join(path,filename)
        elif i is not None:
            filename = "least_cost_path_%s"%(str(i))
            filePath = os.path.join(path,filename)
        else:
            filename = "least_cost_path_%s"%(str(np.random.randint(0,10000)))
            filePath = os.path.join(path,filename)

        self.grid.plot(fileName=filePath,origin=global_start,destination=global_end,
            pts_alignment=pts_alignment,pts_simplified=pts_simplified, show=show)
        
        return {"plot":filename}

