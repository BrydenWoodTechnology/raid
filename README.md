<div style="text-align:center">
    <p align="center">
        <img  style="width:1024px;height:auto" src="flask_UI/static/images/raid_logo.png">
    </p>
    <p align="center" >  Project Website https://raid-app.io </p>

</div>



# Overview 


RAID is an open source app which accelerates the design process of alignments on a railway network.
As a tool, it provides the ability to quickly assess the rail network and explore the ‘route’ performance of different route options on ‘macro’ scale. 
Macro level solutions refer to adjustments on existing routes or suggestions of new alignments. The user can create multiple scenarios, which can be explored through series of dashboards.

An exploratory analysis of the rail route between Manchester Vistoria and York stations can be found [here](https://public.tableau.com/profile/creativetech.bw#!/). The dashboards were created using open data.

The documentation that follows describes the methodology applied in RAID along with the source code developed.


## Table of Contents 


- [Installation Guide](#installation-guide)
    - [Input Data](#input-data)
- [Methodology](#methodology)
- [Examples](#examples)
- [RAID UI](#raid-ui)
    - [Using RAID UI](#using-raid-ui)
    - [Initialize UI](docs/raidUI_docs/Initialize_UI.md)
    - [Get Started](docs/raidUI_docs/Get_Started.md)
    - [User Guide](docs/raidUI_docs/User_Guide.md)





##  Installation Guide

To install and use raid code follow the instructions below

### Windows

On a windows command cell copy the following : 


	 git clone https://gitlab.com/BrydenWoodTechnology/raid.git
	 cd raid
	 conda env create -f environment.yml






### Input Data

Input data needs to be extracted in the InputData directory. 
A detailed guide can be found here [docs](docs/general_docs/Input%20Data.md)




##  Methodology

A short description in the logic of our main functionalities.
More specific : 

#### Chapter 1. [Data Processing - Rasterization](docs/methodology_docs/data_processing_rasterization.md)
#### Chapter 2. [Path Generation](docs/methodology_docs/generate_path.md)
#### Chapter 3. [Refining Path](docs/methodology_docs/refine_path.md)
#### Chapter 4. [Computing Alignment Metrics](docs/methodology_docs/metrics.md)

---

##  Examples 

RAID functionalites can be used/integrated to other projects outside of the  **RAID UI** environment.
We encourage users to see the collection of jupyter notebooks.

#### - [Grid Creation](notebooks_to_publish/Grid_Creation_.ipynb)
#### - [Path generation](notebooks_to_publish/test_ui.ipynb)
#### - [Raster Calculations](notebooks_to_publish/Raster%20Calculations.ipynb)
#### - [Path refinement](notebooks/Path%20refinement.ipynb)
#### - [Railway Metrics](notebooks_to_publish/Alignment_Metrics.ipynb)




---


# RAID UI

<div style="text-align:center">
    <p align="center">
        <img  style="width:1024px;height:auto" src="docs/videos/raidUI_general_smaller.gif">
    </p>
    <p align="center"> Using the Design Studio of the RAID UI ( video takes some time to load ..)</p>
</div>


## Using RAID UI

Having activated the virtual environment where you installed the required packages for RAID

    cd {repository-directory}/BW_RAID/Flask_UI
    start start_UI.bat
    

RAID UI will start serving in http://localhost:4000



## RAID UI in a Notebook!

Although we recommend using the RAID UI through FlaskUI users are able to use most of the functionalities
through a [jupyter notebook](notebooks_to_publish/test_ui.ipynb). 
    


## RAID UI Documentation
For the most up-to-date information, see the available documentation : 

#### [Initialize UI](docs/raidUI_docs/Initialize_UI.md)
On how to set up and initialize flask.

#### [Get Started](docs/raidUI_docs/Get_Started.md)
Short guides over our main functionalities which you can run from the Design Studio of the RAID UI. 

#### [User Guide](docs/raidUI_docs/User_Guide.md)
Detailed guide for the UI and all the available options we have. 




# Contributing

Bug reports and pull requests are welcome.

# Attribution

Data attribution and information about licences can be found in the InputData folder. 

  