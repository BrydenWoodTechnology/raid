"""
Test description

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
import numpy as np
import matplotlib.pyplot as plt

import geopandas as gpd
from shapely.geometry import LineString

# import rempy root
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.util.least_cost_path import generateWorld, get_least_cost
from raidnr.util.ext_methods import smoothen_poly, bezier, points_from_spline, load_speed_limits, resample_poly, fetch_shp
from raidnr.core.alignment import Alignment
from raidnr.bwgrid.grid_transformations import coords_geo_to_raster, raster_to_coords
from raidnr.config import CRS, GRANULARITY


# read world
world_fn = os.path.join(mainPath,r'data\building_world_test.txt')
w = np.loadtxt(world_fn, dtype=np.float64)
w += 1.
# w = gpd.read_file(r'L:\Projects\19\19029 RAID NR\02_BIM\01_WIP\06_Python\Data\raster\SE_Road.tif')
# print(type(w))
start = (405439, 412548)
end =   (407179, 413639)
x1, y1, x2, y2 = [404090.6344816111, 411100.1071910278, 408361.4373172173, 414931.40906368574]
bbox = [x1,x2,y1,y2]
start = coords_geo_to_raster(bbox, start)
end = coords_geo_to_raster(bbox, end)

# calc short path
coords, _ = get_least_cost(w, start, end)

# simplyfy poly
d = 5.
r = 0.05

poly = np.array(coords)
r1 = smoothen_poly(poly, d)
r2 = smoothen_poly(poly, r, True)

#resample empty segments
r1 = resample_poly(LineString(r1), 5.0)
r1 = np.array(list(r1.coords))

b,_ = bezier(r1, 10000.)
u = np.arange(0.,1.01,.01)
pts = points_from_spline(b,u)

# # send it to shp file (tmp task: a placeholder speed point shp is created)
pts_coords = [raster_to_coords(bbox,(pt[1]+abs(y2-y1),pt[0]-abs(x2-x1))) for pt in pts]
pts_coords2 = [raster_to_coords(bbox,(pt[1]+abs(y2-y1),pt[0]-abs(x2-x1))) for pt in poly]
# gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(LineString(list(pts_coords))))
# gdf.crs = CRS
# gdf.to_file(r'C:\Temp\new_alignment.shp')

# load segments and speeds from shp
# segments, speeds = load_speed_limits(fileName=fetch_shp(os.path.join(mainPath,r'data\shp'), 'speed', 'BNG'))

metrics=['curvature','dir_deviation_analysis']
a = Alignment(LineString(pts_coords), '00', 3.0)
a.run_metrics(metrics=metrics)
# a.to_file(r'C:\Temp\new_alignment.shp')

# 
crvs = []
for m in metrics:
    crvs.append(a.metrics[m])

# plot metrics
dim = 7
fig, axs = plt.subplots(len(metrics), 1, figsize=(dim*2,(dim+1)*len(metrics)))
for i,(ax,crv) in enumerate(zip(axs,crvs)):
    x = np.arange(0,len(crv)*GRANULARITY,GRANULARITY)
    ax.plot(x,crv)
    ax.title.set_text(metrics[i])
plt.suptitle('METRICS')
plt.show()


fig,ax = plt.subplots(figsize=(12,12))
ax.scatter(start[1],start[0],color='red')
ax.scatter(end[1],end[0],color='green')

ax.imshow(w)
ax.plot(r1.transpose()[0],r1.transpose()[1], color='orange')
ax.plot(r2.transpose()[0],r2.transpose()[1], color='red')
ax.plot(poly.transpose()[0], poly.transpose()[1], linestyle=':', color='blue')
ax.plot(pts.transpose()[0], pts.transpose()[1])
ax.legend(['dist max = {}'.format(d), 'ratio max = {}'.format(r),'initial','smooth'])
plt.show()

