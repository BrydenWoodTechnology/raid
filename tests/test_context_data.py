import pandas as pd,numpy as np
import json
import geopandas as gpd, fiona, os, shapely
from shapely.geometry import *
from shapely.ops import nearest_points,unary_union,cascaded_union, split
from shapely.geometry import *


import csv
import matplotlib.pyplot as plt
#ogr/gdal/osr
import ogr, gdal, osr
import glob
import time

cwd=os.getcwd()
src=src=os.path.dirname(cwd)

import sys
#append the path to the PYTHONPATH
sys.path.insert(0, src)

#import RAID modules

#insert the modules
import raidnr
from raidnr import *
from raidnr.config import CRS, GRANULARITY, METRICS


import raidnr.bwgrid.grid_transformations as gt

from raidnr.util.ext_methods import *
import raidnr.util.vertical_alignment as va
import raidnr.util.SpatialToolbox as st

from raidnr.core.alignment import Alignment



#set paths:
cwd=os.getcwd() #current working directory
src=src=os.path.dirname(cwd) #main directory

dataPath=os.path.join(src,'data')
contextPath=os.path.join(dataPath,'shp','OS','data')
clippedPath=os.path.join(dataPath,'shp','clipped')
outputPath=os.path.join(dataPath,'shp','output')
bboxPath=os.path.join(dataPath,'shp','bbox')




# If existing alignment
#recreate the line - interpolation - using the alignment module
pl=purge_poly(os.path.join(dataPath,'Railway','Simplified Railline','Manchester_York_Simple_Left_Line.shp'))['geometry'][0]
pl=resample_poly(pl,300) #resample
a=reconstruct_poly(pl,3) #reconstruct
newLine=LineString(a) #store the reconstruced line as a shapely LineString 

bbox=newLine.envelope #shapely polygon
#1.import the stations
stations=gpd.read_file(os.path.join(dataPath,'RailAssets','RailwayStations.shp'))
#check if shapefile is multipoint - if it is convert it to Point
#from MultiPoint to Point
if any(stations.geom_type=='MultiPoint'):
    stations=st.fromMultiToSimpleGeometry(stations)
else:
    pass

#2. clip to the bounding box
clipped_stations=st.clip_polyPoints(stations,bbox)#!!!!!!!!!!!!!!bbox is shapely polygon

#3. snap them to the railway line
snapped_stations=st.snap_points(clipped_stations,newLine) 


#4.pick the start & end stations
orig_name='Slaithwaite Station'
dest_name='Huddersfield Station'

if (orig_name in list(stations['NAME'])) & (dest_name in list(stations['NAME'])):

    orig_pt=snapped_stations.loc[snapped_stations['NAME']==orig_name].reset_index(drop=1)['geometry'][0]
    dest_pt=snapped_stations.loc[snapped_stations['NAME']==dest_name].reset_index(drop=1)['geometry'][0]
    
    segment=st.select_segment(orig_pt,dest_pt,newLine) #the alignment we work with 
    #bbox=segment.buffer(250) #the bounding box
    bbox=segment.envelope
    bbox.crs=CRS
    bbox=bbox.buffer(50)
else:
    print('You have not provided any origin and destination points or the input is invalid.')

    segment=newLine
    bbox=newLine.envelope
    bbox.crs=CRS
    bbox=bbox.buffer(50)


#Export the bbox as a shapefile
bbox_gdf=gpd.GeoDataFrame(geometry=gpd.GeoSeries(bbox))
bbox_gdf.crs=CRS
bbox_gdf.to_file(os.path.join(bboxPath,'bbox.shp'))


#Clip the layers
st.clipShp(contextPath,clippedPath,os.path.join(bboxPath,'bbox.shp'))

#Apply buffer distances and export

bf=pd.read_csv(os.path.join(dataPath,'Buffers.csv')) #Import the CSV with the buffer distances 
st.bufferShp(bf,clippedPath,outputPath)

















