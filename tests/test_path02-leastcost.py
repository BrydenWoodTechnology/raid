"""
Test description

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry import LineString

# import rempy root
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from raidnr.util.least_cost_path import generateWorld, get_least_cost
from raidnr.core.alignment import Alignment
from raidnr.util.ext_methods import smoothen_poly, bezier, points_from_spline

fig,ax = plt.subplots(figsize=(7,7))

mult = 0.5

# create world anc calc short path
w = generateWorld((int(1000*mult),int(1000*mult)))
coords, _ = get_least_cost(w,(int(100*mult),int(100*mult)),(int(800*mult),int(800*mult)),True, ax)

# simplyfy poly
d = 5.
r = 0.05

poly = np.array(coords)
r1 = smoothen_poly(poly, d)
r2 = smoothen_poly(poly, r, True)

b,_ = bezier(r1)
u = np.arange(0.,1.01,.01)
pts = points_from_spline(b,u)

a = Alignment(LineString(pts), '00', 3.0)
a.run_metrics(metrics=['curvature','gradient'])

ax.plot(r1.transpose()[0],r1.transpose()[1], color='orange')
ax.plot(r2.transpose()[0],r2.transpose()[1], color='red')
ax.plot(poly.transpose()[0], poly.transpose()[1], linestyle=':', color='blue')
ax.plot(pts.transpose()[0], pts.transpose()[1])
ax.legend(['dist max = {}'.format(d), 'ratio max = {}'.format(r),'initial','smooth'])
plt.show()


