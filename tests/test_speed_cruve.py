
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

from shapely.geometry import LineString

import os

import geopandas as gpd

from matplotlib.colors import Normalize
from matplotlib import cm




cur_dir = os.getcwd()
modulePath=os.path.join(cur_dir,os.pardir,"raidnr")

sys.path.insert(0,modulePath)

from raidnr	import speedCurve	
import imp
imp.reload(speedCurve)




# speed curve
mainPath = os.path.abspath("..")
dataPath = os.path.join(mainPath,"data")
shpPath= os.path.join(dataPath,"shp")
resultPath=os.path.join(mainPath,"notebooks","results")


def fetchSHP(name,projection):

    directory=os.path.join(shpPath,name,projection)

    files=os.listdir(directory)
    for i in files:
        if i.split(".")[1]=="shp":
            return os.path.join(directory,i)


def test_speed_curve():

	"""
	Test to compute and visualize a speed curve given a specific aligment. 
	For more infos about the algorithm inspect speedcurve.py

	Parameters
	----------

	- read from speed file 
	- compute speed curve
	- visualize 
	

	"""

	fileName=fetchSHP("speed","BNG")

	railway_lines=gpd.read_file(fileName)

	distances = list(railway_lines['geometry'].apply(lambda x: int(x.length) if int(x.length)>0 else 1))   

	speeds=np.array(railway_lines['Speed_mph'])

	speeds[0]=30

	speeds=speeds/2.237

	railway_lines['speed_ms']=speeds

	segments=railway_lines['geometry']

	dist,v,ps=speedCurve.findSpeedCurve(segments,speeds)

	lastDist=0
	overallDistances=[]
	speedTest=[]
	jIndex=0
	for i in segments:
	    
	    overallDistances.append(lastDist)
	    overallDistances.append(i.length+lastDist)
	    
	    speedTest.append(speeds[jIndex])
	    speedTest.append(speeds[jIndex])

	    jIndex+=1
	    lastDist+=i.length

	fig,ax =plt.subplots(figsize=(40,10))

	ax.plot(dist,v)

	ax.plot(overallDistances,speedTest)

	plt.show()

	print("updates")
	return [dist,v,ps]


