

# ----------------------------------------------------
# 
# 
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# 
# 
# ----------------------------------------------------
# 
# 
#-----------------------------------------------------
# 
# test_grid_creation v.02
# 28.08.19
# Claudio Campanile 
# 
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# 
# Collection of tests, for the grid class
# 
#-----------------------------------------------------
# Usefull Links, References
#-----------------------------------------------------
#
#-----------------------------------------------------


import os
import math
import sys
import imp
import requests
import json

import numpy as np
import pandas as pd

import ogr
import gdal
import geopandas as gpd
from shapely.geometry import Point
from pyproj import Proj, transform
from shapely.geometry.linestring import LineString

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from skimage.graph import route_through_array

# raidnr imports
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.bwgrid.grid import Grid
import raidnr.bwgrid.grid_transformations as gt
from raidnr.config import GRANULARITY, METRICS, URL, crv_thresholds
from raidnr.manager.remapper import Remapper

sys.path.insert(0, os.path.join(os.environ['USERPROFILE'],r'source\repos\RAID_NR\flask_UI'))
import base_methods as bm


# Load area and query
bm.init_session('test_session')
a=[407712.2503762188,413445.1119170117]
b=[414570.2503762188,417282.1119170117]
a_=gt.bng_to_wgs(a)
b_=gt.bng_to_wgs(b)
bm.create_bbox([
    a_[0], 
    a_[1],
    b_[0],
    b_[1]
    ])
bm.read_bbox()
# bm.preprocess_context_data()
bm.grid()
bm.init_existing_alignment()
# bm.query_alignment(push_to_mongo=False, save_plots_to=None, save_json_to=None)

# Design parameters
# start = (408439, 414548)
# end =   (413579, 416339)
# start=gt.bng_to_wgs(start) # input will be in wgs 
# end=gt.bng_to_wgs(end)     # input will be in wgs 
# weights = {'Building':1., 'Road':1., 'Woodland':10., 'SurfaceWater_Area':1.}
start = (-1.8068218231201174, 53.63705606607417)
end  =  (-1.8815803527832033, 53.62367017257395)
weights = {
    "Building":5,
    "CarChargingPoint":0,
    "DTM":0,
    "ElectricityTransmissionLine":0,
    "Foreshore":0,
    "FunctionalSite":0,
    "Glasshouse":0,
    "Greenspaces":0,
    "ImportantBuilding":0,
    "MotorwayJunction":0,
    "NationalParks":0,
    "RailwayStation":0,
    "RailwayTrack":0,
    "RailwayTunnel":0,
    "Road":0,
    "RoadTunnel":0,
    "Roundabout":1,
    "Slope":0,
    "SurfaceWater_Area":0,
    "SurfaceWater_Line":0,
    "TidalBoundary":5,
    "TidalWater":0,
    "Woodland":0
}

# bm manager
bm.manager_init()
bm.manager_design(start, end, weights, naa='alignment')
print('one...',bm.global_manager.alignment_ID,'ver:',bm.global_manager.alignment_version)
# bm.manager_design(start, end, weights, naa='alignment')
# print('two...',bm.global_manager.alignment_ID,'ver:',bm.global_manager.alignment_version)

bm.manager_refine(refine='curvature', iterations=5, s=553)
# analyse
bm.manager_analyse_new_alm()
bm.manager_analyse_refined_alm()
# output to DB
bm.manager_output_new_alm()
bm.manager_output_refined_alm()
