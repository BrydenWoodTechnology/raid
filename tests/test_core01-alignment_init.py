"""
Test for purging polyline shapefile in contiguous polyline

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import LineString
import geopandas as gpd

# import rempy root
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from raidnr.util.ext_methods import purge_poly, resample_poly, interpolate_poly, reconstruct_poly
from raidnr.config import CRS

# testing controlpoint functionality
d = r'L:\Projects\19\19029 RAID NR\02_BIM\01_WIP\07_QGIS\Data\Route_Manchester_York\Railway\Simplified Railline'
fn = 'Manchester_York_Simple_Left_Line.shp'
fn = os.path.join(d,fn)

# resampling distance
granularity = 3.0

# load polyline in dataframe
pl = purge_poly(fn)['geometry'][0]
print('{:-^50s}'.format('pre reconstruction'))
print('Object:        ', str(pl)[:50])
print('Length:        ', pl.length)
print('No. of points: ', len(pl.coords))

pl = resample_poly(pl, 300000.0)
itp1 = reconstruct_poly(pl, granularity)
print('{:-^50s}'.format('pre reconstruction (resampled)'))
print('Object:        ', str(itp1)[:50])
print('Length:        ', len(itp1)*granularity)
print('No. of points: ', len(itp1))

pl = resample_poly(pl, 300.0)
print('{:-^50s}'.format('post reconstruction'))
print('Object:        ', str(pl)[:50])
print('Length:        ', pl.length)
print('No. of points: ', len(pl.coords))

itp2 = reconstruct_poly(pl, granularity)
print('{:-^50s}'.format('post reconstruction (resampled)'))
print('Object:        ', str(itp2)[:50])
print('Length:        ', len(itp2)*granularity)
print('No. of points: ', len(itp2))

# plot
pts  = np.array(pl.coords).transpose()
itp1 = itp1.transpose()
itp2 = itp2.transpose()
plt.plot(itp1[0], itp1[1])
plt.plot(itp2[0], itp2[1])
plt.plot(pts[0],pts[1])
plt.legend(['not rebuilt', 'rebuilt', 'poly'])
plt.show()

# # Export shapefile to compare on qgis
# gdf = gpd.GeoDataFrame({
#     'line_type':['poly','not_rebuilt','rebuilt'],
#     'geometry':[LineString(pl),LineString(itp1.transpose()), LineString(itp2.transpose())]
#     })
# gdf.crs = CRS
# gdf.to_file(r'C:\Temp\alignments_test.shp')
