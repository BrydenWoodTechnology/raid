

# ----------------------------------------------------
# 
# 
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# 
# 
# ----------------------------------------------------
# 
# 
#-----------------------------------------------------
# 
# test_grid_creation v.01
# 01.07.18
# Claudio Campanile 
# 
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# 
# Collection of tests, for the grid class
# 
#-----------------------------------------------------
# Usefull Links, References
#-----------------------------------------------------
#
#-----------------------------------------------------



import os
import math
import sys
import imp
import requests
import json
import uuid

import numpy as np
import pandas as pd

import ogr
import gdal
import geopandas as gpd
from shapely.geometry import Point
from pyproj import Proj, transform
from shapely.geometry.linestring import LineString

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from skimage.graph import route_through_array

sys.path.append(os.environ['USERPROFILE']+r'\Documents\ssh')
from keys import Keys

# raidnr imports
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.bwgrid.grid import Grid
from raidnr.core.alignment import Alignment
import raidnr.bwgrid.grid_transformations as gt
from raidnr.config import GRANULARITY, METRICS, URL, PATH_BBOX, PATH_RAILWAY, PATH_SPEED, PATH_CLIP_ALIGNMENT, PATH_CLIP_SPEED
from raidnr.util.ext_methods import filter_files

# data paths
rasterPath=os.path.join(mainPath,"notebooks","results")

# define grid
start = (405439, 412548)
end =   (407179, 413639)
x1, y1, x2, y2 = [404090.6344816111, 411100.1071910278, 408361.4373172173, 414931.40906368574]
bbox = [x1,x2,y1,y2]

# define new path hyper-params
weights = {'Building':10., 'Road':1., 'Woodland':2., 'SurfaceWater_Area':5.} #,'Woodland':3.,'SurfaceWater_Area':1.,
max_dev = 0.

# load rasters into numpy array
a = gt.get_grid_from_directory(rasterPath)

# create grid object
g = Grid(a, bbox, GRANULARITY)


# fitness layer
# fl = g.compute_fitness_layer(weights=weights)
# fig = plt.figure(figsize=(10,10))
# plt.imshow(fl)
# plt.show()

# generate a least cost path
start = g.to_raster(start)
end = g.to_raster(end)
g.compute_new_alignment_(origin=start, destination=end, weights=weights, s=max_dev, plot=True)


# metrics - calc
metrics = ["curvature", "gradient", "angle_deviation_analysis", "distance_deviation_analysis"]
g.evaluate_new_alignment_(metrics=metrics)
g.plot(alignment='self')


g._alignment.plot_metrics()

ras_na = g.rasterize_alignment(g._alignment, metrics=metrics)
js = g._alignment.to_json_str()
j = json.loads(js)
with open(r'C:\Temp\alignment.json','w') as f:
    json.dump(j,f,indent=1)


# # export alignment to json file
# print('{:-^80s}'.format('serializing Alignment into a json-encoded string'))
# js = a.to_json_str()
# j = json.loads(js)
# with open(r'C:\Temp\alignment.json','w') as f:
#     json.dump(j,f,indent=1)

# push to mongo collection
if False:
    print('{:-^80s}'.format('Pushing data to mongo'))
    k = Keys()
    headers = k.mongo_auth_encoded
    headers["Content-Type"] = "application/json"
    r = requests.post(url=URL + r'\Test_Alignment', data=js, headers=headers)
    # extracting response text  
    pastebin_url = r.text 
    print("The pastebin URL is:{}".format(pastebin_url))
else:
    print('n\Metrics NOT sent to Mongo')
