import math 
import sys

import os

cwd=os.getcwd()
src=src=os.path.dirname(cwd)
#append the path to the PYTHONPATH
sys.path.insert(0, src)

import numpy as np
from osgeo import gdal


#insert the modules
import raidnr
from raidnr import *
from raidnr.config import CRS, GRANULARITY, METRICS

import raidnr.core
import raidnr.core.alignment as al

import raidnr.bwgrid.grid_transformations as gt

from raidnr.util.ext_methods import *
import raidnr.util.vertical_alignment as va

from raidnr.core.alignment import Alignment


#load the grid
world_loaded = np.loadtxt('world.txt', dtype=np.float64)
world = world_loaded

#define origin and destination points
origin=(20,700)
destination = (700,100)


#Pre-defined design constants:
#1.0 the maximum allowable circuity coefficient - set arbitrary as 4 
gmax=4




circuity = np.zeros_like(world)

#compute the circuity
for i in range(world.shape[0]):
    for j in range(world.shape[1]):
        calculatedValue=va.airline_dist(origin[0],origin[1],destination[0],destination[1],i,j,w=3)[0]
        circuity[i,j]=calculatedValue



#evaluate circuity
evaluation=np.zeros_like(world)

for i in range(world.shape[0]):
    for j in range(world.shape[1]):
        calculatedValue=va.evaluate_circuity(circuity[i,j],gmax)
        evaluation[i,j]=calculatedValue



# masked grid for visualization
reachable = np.ma.masked_values(evaluation,0)


fig,ax = plt.subplots(figsize=(10,10))

#x and y coords for the color mesh         
ximage=np.arange(0,world.shape[0])
yimage=np.arange(0,world.shape[1])

ax.pcolormesh(yimage,ximage,world)
ax.pcolormesh(yimage,ximage,reachable,cmap="Greys",alpha=0.1)
plt.scatter(origin[0],origin[1])
plt.scatter(destination[0],destination[1])
plt.show()