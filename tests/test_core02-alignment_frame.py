"""
Alignment Object 
----------------

Purge polyline shapefile => interpolate cubic spline
compare the three curves
apply a frenet frame on one of the alignemnt object
Plot results

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
# scientific modules
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import interpolate
# data
from shapely.geometry import LineString
import geopandas as gpd
import pandas as pd

# import rempy root
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from raidnr.core.alignment import Alignment
from raidnr.util.ext_methods import purge_poly, resample_poly, reconstruct_poly
from raidnr.config import CRS, GRANULARITY

# testing controlpoint functionality
d = r'L:\Projects\19\19029 RAID NR\02_BIM\01_WIP\07_QGIS\Data\Route_Manchester_York\Railway\Simplified Railline'
fn = 'Manchester_York_Simple_Left_Line.shp'
fn = os.path.join(d,fn)

# load polyline and build alignment
pl = purge_poly(fn)['geometry'][0]

pl = resample_poly(pl, 300.0)
print('{:-^50s}'.format('post reconstruction'))
print('Object:        ', str(pl)[:50])
print('Length:        ', pl.length)
print('No. of points: ', len(pl.coords))

itp2 = reconstruct_poly(pl, GRANULARITY)
print('{:-^50s}'.format('post reconstruction (resampled)'))
print('Object:        ', str(itp2)[:50])
print('Length:        ', len(itp2)*GRANULARITY)
print('No. of points: ', len(itp2))

a = Alignment(LineString(itp2),'no_id', GRANULARITY)
print('{:-^50s}'.format('init alignment object'), a)

# plot alingment
fig = plt.figure()
ax = fig.gca(projection='3d')
pts  = np.array(pl.coords).transpose()
itp2 = itp2.transpose()
ax.plot(itp2[0], itp2[1])
ax.plot(pts[0],pts[1],':')
ax.legend(['rebuilt', 'poly'])

# plot frenet frame at param u position
u = 0.5
ff = a.frenet_frame_at(u)
ff.display(ax)

# check if the plane is on the curve
ff_o = (ff.o[0], ff.o[1], ff.o[2])
crv_pt = interpolate.splev(u, a.curve)
print('plane origin:   {}'.format(ff_o))
print('point on curve: {}'.format(crv_pt))

# center view to frenet frame
window = 3. #dim of window bbox
ax.set_xlim3d(float(ff.o[0]) - window, float(ff.o[0]) + window)
ax.set_ylim3d(float(ff.o[1]) - window, float(ff.o[1]) + window)
ax.set_zlim3d(float(ff.o[2]) - window, float(ff.o[2]) + window)
plt.show()

