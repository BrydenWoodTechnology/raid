"""
Alignment Test 
--------------

Purge polyline shapefile => interpolate cubic spline
Analyse curvature + apply smoothening
Plot results

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import interpolate
import pandas as pd

# import rempy root
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from raidnr.core.frenet_frame import FrenetFrame
from raidnr.util.ext_methods import purge_poly, resample_poly, reconstruct_poly
from raidnr.config import CRS, GRANULARITY

x=np.array([[0.,1.,2.,3.]]*2+[[0.,1.,4.,9.]])
print(x)
spl,_ = interpolate.splprep(x=x)

ff = FrenetFrame(spl, 0.5)
print(ff)
print(ff.frame_triplet())

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlim((0,10))
ax.set_ylim((0,10))
ax.set_zlim((0,10))
ff.display(ax)

pts = np.array(interpolate.splev(np.arange(0,1.05,.05),spl))
ax.plot(*pts)
plt.show()