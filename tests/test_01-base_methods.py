

# ----------------------------------------------------
# 
# 
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# 
# 
# ----------------------------------------------------
# 
# 
#-----------------------------------------------------
# 
# test_grid_creation v.01
# 01.07.18
# Claudio Campanile 
# 
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# 
# Collection of tests, for the grid class
# 
#-----------------------------------------------------
# Usefull Links, References
#-----------------------------------------------------
#
#-----------------------------------------------------


import os
import math
import sys
import imp
import requests
import json

import numpy as np
import pandas as pd

import ogr
import gdal
import geopandas as gpd
from shapely.geometry import Point
from pyproj import Proj, transform
from shapely.geometry.linestring import LineString

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from skimage.graph import route_through_array

# sys.path.append(os.environ['USERPROFILE']+r'\Documents\ssh')
# from keys import Keys

# raidnr imports
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.bwgrid.grid import Grid
import raidnr.bwgrid.grid_transformations as gt
from raidnr.config import GRANULARITY, METRICS, URL

sys.path.insert(0, os.path.join(os.environ['USERPROFILE'],r'source\repos\RAID_NR\flask_UI'))
import base_methods as bm


# Load area and query
bm.read_bbox()
# bm.preprocess_context_data()
bm.grid()
# bm.query_alignment(push_to_mongo=False, save_plots_to=None, save_json_to=None)

# Design new alignment
start = (408439, 414548)
end =   (413579, 416339)
weights = {'Building':1., 'Road':1., 'Woodland':10., 'SurfaceWater_Area':10.}
bm.gen_path(start, end, weights, max_dev=1000.)

# Analyse the design
bm.analyse_new_path()
bm.query_cost_point(100,100) # To replace with kernel analysis

