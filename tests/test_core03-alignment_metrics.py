"""
Alignment Metrics
-------------------

Build an alignment as in test 01
Analyse metrics (curvature, )
Plot results

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
# matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# scientific
import numpy as np
# geo
from shapely.geometry import LineString
import geopandas as gpd
import pandas as pd
# http requests
import requests
import html
sys.path.append(os.environ['USERPROFILE']+r'\Documents\ssh')
from keys import Keys

# import rempy root
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.core.alignment import Alignment
from raidnr.util.ext_methods import purge_poly, resample_poly, reconstruct_poly, fetch_shp, load_speed_limits, filter_files, snap_bbox
from raidnr.config import CRS, GRANULARITY, URL, PATH_RAILWAY, PATH_SPEED, PATH_OS, PATH_RASTER, PATH_BBOX, PATH_BBOX_OFF, PATH_CLIP_ALIGNMENT, PATH_CLIP_CONTEXT, PATH_CLIP_SPEED
from raidnr.bwgrid import grid_transformations as gt
from raidnr.util import SpatialToolbox as st
from raidnr.bwgrid.grid import Grid


# point to one polyline shapefile
# d = r'L:\Projects\19\19029 RAID NR\02_BIM\01_WIP\07_QGIS\Data\Route_Manchester_York\Railway\Simplified Railline'
#     r'\data\shp\Railway_Lines\BNG'
# fn = 'Manchester_York_Simple_Left_Line.shp'
# fn = os.path.join(d,fn)
# rasterPath=os.path.join(mainPath,"notebooks","results")

# create grid terrain required by gradient analysis
resampling = 300.
# x1, y1, x2, y2 = [404090.6344816111, 411100.1071910278, 408361.4373172173, 414931.40906368574]
bbox = [407912.83459730295, 414116.4445978563, 414391.0912720474, 417003.13814145426]
x1, y1, x2, y2 = bbox
start = ()
end = ()
# snap to w3w
bbox = snap_bbox(bbox)

# retrieve file paths
topo_layers_fn = filter_files(PATH_OS, ext='shp') # context shp
rail_fn = filter_files(PATH_RAILWAY, ext='shp', no_of_groups=1)[0]
speed_fn = filter_files(PATH_SPEED, no_of_groups=1)[0]
rail_clip_fn = os.path.join(PATH_CLIP_ALIGNMENT, 'clipped_rail.shp')
speed_clip_fn = os.path.join(PATH_CLIP_SPEED, 'clipped_speeds.shp')
# save bbox as shp file
st.fromCoordsToBbox(x1, y1, x2, y2, PATH_BBOX, dist=0) 
st.fromCoordsToBbox(x1, y1, x2, y2, PATH_BBOX_OFF, dist=50)

# clip, rasterize and build grid
for fn in topo_layers_fn:
    name=os.path.join(PATH_RASTER, str(os.path.splitext(os.path.split(fn)[1])[0])+'.tif')
    gt.rasterize_shape(fn, bbox, tifName=name)
a = gt.get_grid_from_directory(PATH_RASTER)
g = Grid(a, bbox, GRANULARITY)
# clip and build existing alignment (include speed data)
bbox_fn = os.path.join(PATH_BBOX,'bbox.shp')
st.clipOGR(bbox_fn, rail_fn, rail_clip_fn) # clip alignment
st.clipOGR(bbox_fn, speed_fn, speed_clip_fn) # clip speed data
segments, speeds = load_speed_limits(speed_clip_fn) #load speed data
a = Alignment.from_shp_poly(fn=rail_clip_fn, id=2100, resample_dist=resampling, granularity=GRANULARITY, speed_geometry=segments, speed_values=speeds)

print('{:-^80s}'.format('init alignment object'))
print(a)

# run metrics and plot
metrics = ['curvature', 'gradient', 'distance_deviation_analysis', 'headway','speed_analysis']
a.run_metrics(dtm_grid=g, metrics=metrics)
a.plot_metrics()

# export alignment to json file
print('{:-^80s}'.format('serializing Alignment into a json-encoded string'))
js = a.to_json_str()
j = json.loads(js)
with open(r'C:\Temp\alignment.json','w') as f:
    json.dump(j,f,indent=1)

# push to mongo collection
if False:
    print('{:-^80s}'.format('Pushing data to mongo'))
    k = Keys()
    headers = k.mongo_auth_encoded
    headers["Content-Type"] = "application/json"
    r = requests.post(url=URL + r'\Test_Alignment', data=js, headers=headers)
    # extracting response text  
    pastebin_url = r.text 
    print("The pastebin URL is:{}".format(pastebin_url))
else:
    print('n\Metrics NOT sent to Mongo')