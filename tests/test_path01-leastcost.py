"""
Test description

author: ccampanile
"""

# external modules
import os
import sys
import json
import random
import numpy as np
import matplotlib.pyplot as plt

# import rempy root
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from raidnr.util.least_cost_path import generateWorld, get_least_cost
from raidnr.util.ext_methods import smoothen_poly

# create world anc calc short path
w = generateWorld((1000,1000))
coords, _ = get_least_cost(w,(100,100),(800,800),True)

# simplyfy poly
d = 5.
r = 0.05

poly = np.array(coords)
r1 = smoothen_poly(poly, d)
r2 = smoothen_poly(poly, r, True)

plt.figure(figsize=(10,10))
plt.plot(r1.transpose()[0],r1.transpose()[1], color='orange')
plt.plot(r2.transpose()[0],r2.transpose()[1], color='green')
plt.plot(poly.transpose()[0], poly.transpose()[1], linestyle=':', color='blue')
plt.legend(['dist max = {}'.format(d), 'ratio max = {}'.format(r),'initial'])
plt.xlim((0,1000))
plt.ylim((0,1000))
plt.show()
