

# ----------------------------------------------------
# 
# 
# ██████╗  █████╗ ██╗██████╗     ███╗   ██╗██████╗ 
# ██╔══██╗██╔══██╗██║██╔══██╗    ████╗  ██║██╔══██╗
# ██████╔╝███████║██║██║  ██║    ██╔██╗ ██║██████╔╝
# ██╔══██╗██╔══██║██║██║  ██║    ██║╚██╗██║██╔══██╗
# ██║  ██║██║  ██║██║██████╔╝    ██║ ╚████║██║  ██║
# ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝     ╚═╝  ╚═══╝╚═╝  ╚═╝
# 
# 
# ----------------------------------------------------
# 
# 
#-----------------------------------------------------
# 
# test_grid_creation v.01
# 01.07.18
# Claudio Campanile 
# 
#-----------------------------------------------------
# Description
#-----------------------------------------------------
# 
# Collection of tests, for the grid class
# 
#-----------------------------------------------------
# Usefull Links, References
#-----------------------------------------------------
#
#-----------------------------------------------------


import os
import math
import sys
import imp
import requests
import json

import numpy as np
import pandas as pd

import ogr
import gdal
import geopandas as gpd
from shapely.geometry import Point
from pyproj import Proj, transform
from shapely.geometry.linestring import LineString

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from skimage.graph import route_through_array

# sys.path.append(os.environ['USERPROFILE']+r'\Documents\ssh')
# from keys import Keys

# raidnr imports
mainPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, mainPath)
from raidnr.bwgrid.grid import Grid
import raidnr.bwgrid.grid_transformations as gt
from raidnr.config import GRANULARITY, METRICS, URL

# sys.path.insert(0, os.path.join(os.environ['USERPROFILE'],r'source\repos\RAID_NR\flask_UI'))
import base_methods as bm

# global sPATH_BBOX
# def init_session(ss_name):
#     global sPATH_BBOX 
#     sPATH_BBOX = os.path.join(PATH_BBOX,ss_name)
#     global sPATH_BBOX_OFF          = os.path.join(PATH_BBOX_OFF,         ss_name)
#     global sPATH_CLIP_CONTEXT      = os.path.join(PATH_CLIP_CONTEXT,     ss_name)
#     global sPATH_CLIP_CONTEXT_BUFF = os.path.join(PATH_CLIP_CONTEXT_BUFF,ss_name)
#     global sPATH_CLIP_SPEED        = os.path.join(PATH_CLIP_SPEED,       ss_name)
#     global sPATH_CLIP_ALIGNMENT    = os.path.join(PATH_CLIP_ALIGNMENT,   ss_name)
#     global sPATH_IMG               = os.path.join(PATH_IMG,              ss_name)
#     # create session paths
#     sessionPaths=[sPATH_BBOX,sPATH_BBOX_OFF,sPATH_CLIP_CONTEXT,sPATH_CLIP_CONTEXT_BUFF,sPATH_CLIP_SPEED,sPATH_CLIP_ALIGNMENT,sPATH_IMG]
#     for sspath in sessionPaths:
#         path = os.path.join(sspath,ss_name)
        # if(os.path.exists(path)==False):
        #     os.makedirs(path)


bm.read_bbox()
# bm.preprocess_context_data()
bm.grid()
# bm.query_alignment(push_to_mongo=False, save_to=None,fn_metrics_img=None)
start = (408439, 414548)
end =   (413579, 416339)
weights = {'Building':10., 'Road':1., 'Woodland':2., 'SurfaceWater_Area':5.}
bm.gen_path(start, end, weights, max_dev=1000., save_to=None, fn_metrics_img=None)
bm.analyse_new_path()
bm.query_cost_point(100,100)